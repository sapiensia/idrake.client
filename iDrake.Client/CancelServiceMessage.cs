﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem de cancelamento de serviço.
    ///     Esta mensagem deve ser enviada pelo fornecedor quando o mesmo quiser cancelar um serviço.
    /// </summary>
    
    
    public class CancelServiceMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public CancelServiceMessage()
            : base(MessageType.CancelService)
        {
            MainObjectType = typeof(LogisticNeed).Name;
        }

        /// <summary>
        ///     Motivo do cancelamento.
        /// </summary>
        public string Details { get; set; }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<CancelServiceMessage, ServiceMessage>(this);
        }
    }
}