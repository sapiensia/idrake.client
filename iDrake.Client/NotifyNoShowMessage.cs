﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem de notificação de não utilização do serviço.
    ///     Esta mensagem deve ser enviada pelo fornecedor quando o mesmo quiser informar que um serviço não foi utilizado.
    /// </summary>
    
    
    public class NotifyNoShowMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public NotifyNoShowMessage()
            : base(MessageType.NotifyNoShow)
        {
            MainObjectType = typeof(LogisticNeed).Name;
        }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<NotifyNoShowMessage, ServiceMessage>(this);
        }
    }
}