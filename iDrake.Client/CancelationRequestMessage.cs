﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Solicitação de cancelamento.
    ///     Esta mensagem deve ser enviada pelo cliente quando o mesmo quiser solicitar o cancelamento de um serviço.
    /// </summary>
    
    
    public class CancelationRequestMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public CancelationRequestMessage()
            : base(MessageType.CancelationRequest)
        {
            MainObjectType = typeof(LogisticNeed).Name;
        }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<CancelationRequestMessage, ServiceMessage>(this);
        }
    }
}