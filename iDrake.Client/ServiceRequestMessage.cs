﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem de solicitação de serviço.
    ///     Esta mensagem deve ser enviada pelo cliente quando o mesmo quiser solicitar um serviço.
    /// </summary>
    
    
    public class ServiceRequestMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public ServiceRequestMessage()
            : base(MessageType.ServiceRequest)
        {
            MainObjectType = typeof(LogisticNeed).Name;
        }

        /// <summary>
        ///     Ordem que o cliente gostaria que o serviço fosse realizado.   
        /// </summary>
        
        public IList<ServiceOrder> ServiceOrder { get; set; }

        /// <summary>
        ///     Código do atendimento que cliente gostaria que fornecedor tentasse reusar.
        /// </summary>
        
        public string TreatmentIdToTryReuse { get; set; }

        /// <summary>
        ///     Necessidade logística solicitada.
        /// </summary>
        
        public LogisticNeed LogisticNeed { get; set; }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<ServiceRequestMessage, ServiceMessage>(this);
        }
    }
}