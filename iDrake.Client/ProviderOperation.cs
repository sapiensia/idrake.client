﻿using System.Collections.Generic;
using iDrake.Client.Config;

namespace iDrake.Client
{
    /// <summary>
    ///     Operação do fornecedor Drake realizada no serviço de integração do Drake.
    /// </summary>
    public sealed class ProviderOperation : IProviderOperation
    {

        /// <summary>
        ///     Código padrão do destinatário.
        ///     Quando nenhum código for informado em uma mensagem da operação em questão, esse código será usado.
        /// </summary>
        private readonly int? _defaultRecipientId;

        /// <summary>
        ///     Mensagens que serão enviadas na operação.
        /// </summary>
        private readonly List<Message> _messages = new List<Message>();

        /// <summary>
        ///     Construtor.
        /// </summary>
        public ProviderOperation()
        {
        }

        /// <summary>
        ///     Construtor.
        /// </summary>
        /// <param name="defaultRecipientId">Código do destinatário padrão da operação. Quando um não for informado, este valor será usado.</param>
        public ProviderOperation(int defaultRecipientId)
        {
            _defaultRecipientId = defaultRecipientId;
        }

        /// <summary>
        ///     Confirmar atendimento de serviço.
        /// </summary>
        /// <param name="message">Mensagem de confirmação de atendimento de serviço.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation ConfirmService(ConfirmServiceMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Confirmar atendimento de serviços.
        /// </summary>
        /// <param name="messages">Mensagens de confirmações de atendimentos de serviços.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation ConfirmServices(IEnumerable<ConfirmServiceMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Recusar solicitação de atendimento.
        /// </summary>
        /// <param name="message">Mensagem de recusa de solicitação de atendimento.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation RefuseRequest(RefuseRequestMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Recusar solicitações de atendimentos.
        /// </summary>
        /// <param name="messages">Mensagens de recusa de solicitações de atendimentos.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation RefuseRequests(IEnumerable<RefuseRequestMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Cancelar serviço.
        /// </summary>
        /// <param name="message">Mensagem de cancelamento de serviço.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation CancelService(CancelServiceMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Cancelar serviços.
        /// </summary>
        /// <param name="messages">Mensagens de cancelamento de serviços.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation CancelServices(IEnumerable<CancelServiceMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Recusar solicitação de cancelamento.
        /// </summary>
        /// <param name="message">Mensagem de recusa de solicitação de cancelamento.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation RefuseCancelation(RefuseCancelationMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Recusar solicitações de cancelamentos.
        /// </summary>
        /// <param name="messages">Mensagens de recusa de solicitações de cancelamentos.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation RefuseCancelations(IEnumerable<RefuseCancelationMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Confirmar cancelamento solicitado pelo cliente (sem reembolso).
        /// </summary>
        /// <param name="message">Mensagem de confirmação de cancelamento solicitado pelo cliente.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation CancelConfirmation(CancelationConfirmationMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Confirmar cancelamentos solicitados pelo cliente (sem reembolso).
        /// </summary>
        /// <param name="messages">Mensagens de confirmações de cancelamentos solicitados pelo cliente.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation CancelConfirmations(IEnumerable<CancelationConfirmationMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Confirmar cancelamento solicitado pelo cliente (com reembolso).
        /// </summary>
        /// <param name="message">Mensagem de confirmação de cancelamento solicitado pelo cliente com reembolso.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation CancelConfirmationWithRefund(CancelationConfirmationWithRefundMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Confirmar cancelamento solicitado pelo cliente com taxa de cancelamento.
        /// </summary>
        /// <param name="message">Mensagem de confirmação de cancelamento solicitado pelo cliente com taxa de cancelamento.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation CancelConfirmationWithFee(CancelationConfirmationWithFeeMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Confirmar cancelamentos solicitados pelo cliente (com reembolso).
        /// </summary>
        /// <param name="messages">Mensagens de confirmações de cancelamentos solicitados pelo cliente com reembolso.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation CancelConfirmationsWithRefund(IEnumerable<CancelationConfirmationWithRefundMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Confirmar cancelamentos solicitados pelo cliente com taxa de cancelamento.
        /// </summary>
        /// <param name="messages">Mensagens de confirmações de cancelamentos solicitados pelo cliente com taxa de canelamento.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation CancelConfirmationsWithFee(IEnumerable<CancelationConfirmationWithFeeMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Notificar não utilização de serviço.
        /// </summary>
        /// <param name="message">Mensagem de notificação de não utilização de serviço.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation NotifyNoShow(NotifyNoShowMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Notificar não utilização de serviços.
        /// </summary>
        /// <param name="messages">Mensagens de notificação de não utilização de serviço.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation NotifyNoShows(IEnumerable<NotifyNoShowMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Notificar utilização de serviço.
        /// </summary>
        /// <param name="message">Mensagem de utilização de serviço.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation NotifyUse(NotifyUseMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Notificar utilização de serviços.
        /// </summary>
        /// <param name="messages">Mensagens de utilização de serviços.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation NotifyUses(IEnumerable<NotifyUseMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Solicitar aprovação de fatura.
        /// </summary>
        /// <param name="message">Mensagem contendo a fatura que precisa ser aprovada.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation InvoiceApprovalRequest(InvoiceApprovalRequestMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Solicitar aprovação de faturas. 
        /// </summary>
        /// <param name="messages">Mensagens contendo as faturas que precisam ser aprovadas.</param>
        /// <returns>Operação do fornecedor.</returns>
        public ProviderOperation InvoiceApprovalRequests(IEnumerable<InvoiceApprovalRequestMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        public ProviderOperation ConfirmProcessing(ConfirmProcessingMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Executa a operação.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração que será usada para executar a operação.</param>
        /// <returns>Mensagens enviadas com os códigos carregados.</returns>
        public void Execute(IStrategyConfiguration strategyConfiguration)
        {
            LoadDefaultRecipientId(_messages);
            LoadSenderId(_messages, strategyConfiguration.GetSenderId());
            Validate(_messages);
            Context.Current.SendMessages(strategyConfiguration, _messages);
        }

        /// <summary>
        ///     Executa a operação.
        /// </summary>
        /// <returns>Mensagens enviadas com os códigos carregados.</returns>
        public void Execute()
        {
            Execute(Context.Current.Configuration);
        }

        /// <summary>
        ///     Carrega o código do destinatário padrão nas mensagens em que o código não foi informado.
        /// </summary>
        /// <param name="messages">Mensagens da operação.</param>
        private void LoadDefaultRecipientId(IEnumerable<Message> messages)
        {
            foreach (var message in messages)
            {
                if (message != null && message.RecipientId == null && _defaultRecipientId != null)
                {
                    message.RecipientId = _defaultRecipientId;
                }
            }
        }

        private void LoadSenderId(IEnumerable<Message> messages, int? senderId)
        {
            foreach (var message in messages)
            {
                if (message != null)
                {
                    message.SenderId = senderId;
                }
            }
        }

        private void Validate(IEnumerable<Message> messages)
        {
            if (messages != null)
            {
                foreach (var message in messages)
                {
                    message.Validate();
                }
            }
        }

    }

}