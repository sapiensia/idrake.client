﻿using System.Collections.Generic;
using iDrake.Client.Config;

namespace iDrake.Client
{
    /// <summary>
    ///     Operação do cliente Drake realizada no serviço de integração do Drake.
    /// </summary>
    public sealed class CustomerOperation : ICustomerOperation
    {
        /// <summary>
        ///     Código padrão do destinatário.
        ///     Quando nenhum código for informado em uma mensagem da operação em questão, esse código será usado.
        /// </summary>
        private readonly int? _defaultRecipientId;

        /// <summary>
        ///     Mensagens que serão enviadas na operação.
        /// </summary>
        private readonly List<Message> _messages = new List<Message>();

        /// <summary>
        ///     Construtor.
        /// </summary>
        public CustomerOperation()
        {
        }

        /// <summary>
        ///     Construtor.
        /// </summary>
        /// <param name="defaultRecipientId">Código do destinatário padrão da operação. Quando um não for informado na mensagem, este valor será usado.</param>
        public CustomerOperation(int defaultRecipientId)
        {
            _defaultRecipientId = defaultRecipientId;
        }

        /// <summary>
        ///     Solicitar serviço.
        /// </summary>
        /// <param name="message">Mensagem de solicitação de serviço.</param>
        /// <returns>Operação do cliente.</returns>
        public CustomerOperation ServiceRequest(ServiceRequestMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Solicitar serviços.
        /// </summary>
        /// <param name="messages">Mensagens de solicitação de serviço.</param>
        /// <returns>Operação do cliente.</returns>
        public CustomerOperation ServiceRequests(IEnumerable<ServiceRequestMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Solicitar cancelamento.
        /// </summary>
        /// <param name="message">Mensagem de solicitação de cancelamento.</param>
        /// <returns>Operação do cliente.</returns>
        public CustomerOperation CancelationRequest(CancelationRequestMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Solicitar cancelamentos.
        /// </summary>
        /// <param name="messages">Mensagens de solicitação de cancelamento.</param>
        /// <returns>Operação do cliente.</returns>
        public CustomerOperation CancelationRequests(IEnumerable<CancelationRequestMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Solicitar mudança.
        /// </summary>
        /// <param name="message">Mensagem de solicitação de mudança.</param>
        /// <returns>Operação do cliente.</returns>
        public CustomerOperation ChangeRequest(ChangeRequestMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Solicitar mudanças.
        /// </summary>
        /// <param name="messages">Mensagens de solicitação de mudança.</param>
        /// <returns>Operação do cliente.</returns>
        public CustomerOperation ChangeRequests(IEnumerable<ChangeRequestMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Aprovar fatura.
        /// </summary>
        /// <param name="message">Mensagem de aprovação de fatura.</param>
        /// <returns>Operação do cliente.</returns>
        public CustomerOperation ApproveInvoice(ApproveInvoiceMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        ///     Aprovar faturas.
        /// </summary>
        /// <param name="messages">Mensagens de aprovação de faturas.</param>
        /// <returns>Operação do cliente.</returns>
        public CustomerOperation ApproveInvoices(IEnumerable<ApproveInvoiceMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        public CustomerOperation ConfirmProcessing(ConfirmProcessingMessage message)
        {
            _messages.Add(message);
            return this;
        }
        
        /// <summary>
        ///     Recusar fatura.
        /// </summary>
        /// <param name="message">Mensagem de recusa de fatura.</param>
        /// <returns>Operação do cliente.</returns>
        public CustomerOperation RefuseInvoice(RefuseInvoiceMessage message)
        {
            _messages.Add(message);
            return this;
        }

        /// <summary>
        /// Recusar faturas.
        /// </summary>
        /// <param name="messages">Mensagens de recusa de fatura.</param>
        /// <returns>Operação do cliente.</returns>
        public CustomerOperation RefuseInvoices(IEnumerable<RefuseInvoiceMessage> messages)
        {
            _messages.AddRange(messages);
            return this;
        }

        /// <summary>
        ///     Executa a operação.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração que será usada para executar a operação.</param>
        /// <returns>Mensagens enviadas com os códigos carregados.</returns>
        public void Execute(IStrategyConfiguration strategyConfiguration)
        {
            LoadDefaultRecipientId(_messages);
            LoadSenderId(_messages, strategyConfiguration.GetSenderId());
            Validate(_messages);
            Context.Current.SendMessages(strategyConfiguration, _messages);
        }

        /// <summary>
        ///     Executa a operação.
        /// </summary>
        /// <returns>Mensagens enviadas com os códigos carregados.</returns>
        public void Execute()
        {
            Execute(Context.Current.Configuration);
        }

        /// <summary>
        ///     Carrega o código do destinatário padrão nas mensagens em que o código não foi informado.
        /// </summary>
        /// <param name="messages">Mensagens da operação.</param>
        private void LoadDefaultRecipientId(IEnumerable<Message> messages)
        {
            foreach (Message message in messages)
            {
                if (message != null && message.RecipientId == null && _defaultRecipientId != null)
                {
                    message.RecipientId = _defaultRecipientId;
                }
            }
        }

        private void LoadSenderId(IEnumerable<Message> messages, int? senderId)
        {
            foreach (var message in messages)
            {
                if (message != null)
                {
                    message.SenderId = senderId;
                }
            }
        }

        private void Validate(IEnumerable<Message> messages)
        {
            if (messages != null)
            {
                foreach (var message in messages)
                {
                    message.Validate();                   
                }
            }
        }

    }

}