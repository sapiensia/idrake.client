﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using iDrake.Client.Stubs.Enums;
using iDrake.Client.Utils;
using Newtonsoft.Json;

namespace iDrake.Client
{
    /// <summary>
    ///     Necessidade logística.
    /// </summary>
    
    
    public class LogisticNeed
    {

        /// <summary>
        /// Construtor.
        /// </summary>
        public LogisticNeed()
        {
            Associated = new List<LogisticNeed>();
            Details = new Dictionary<string, string>();
        }

        /// <summary>
        ///     Código da necessidade no DRAKE.
        /// </summary>
        
        public long DrakeId { get; set; }

        /// <summary>
        ///     Colaborador/empregado/funcionário participante da necessidae de logística.
        /// </summary>
        
        public Participant Participant { get; set; }

        /// <summary>
        ///     Data e hora do início da necessidade.
        /// </summary>
        
        public DateTime? Start { get; set; }

        /// <summary>
        ///     Data e hora do fim da necessidade. Normalmente usado apenas para necessidades que tem fim conhecido como hospedagem, locação etc.
        /// </summary>
        
        public DateTime? End { get; set; }

        /// <summary>
        ///     Tipo da necessidade de logística: Transporte aéreo, terrestre, hospedagem ou locação.
        /// </summary>
        
        public LogisticType Type { get; set; }

        /// <summary>
        ///     No caso de passagens aéreas os subtipo é a classe (econômico, primeira classe etc).
        ///     No caso de transporte terrestre o tipo de transporte (carro, ônibus, van etc).
        ///     No caso de hospedagem o tipo de quarto (single, double etc).
        /// </summary>
        
        public string Subtype { get; set; }

        /// <summary>
        ///     Endereço de origem.
        /// </summary>
        
        public Address Origin { get; set; }

        /// <summary>
        ///     Endereço de destino.
        /// </summary>
        
        public Address Destination { get; set; }

        /// <summary>
        ///     Lista de necessidade associadas a esta necessidade. No Drake cada necessidade pode ser atendida separadamente ou ser associada para ser atendida em conjunto.
        ///     Exemplo: Três pessoas precisam de um mesmo carro. Temos três necessidades, mas essas três necessidades serão associadas numa única necessidade de carro contendo os três participantes. Nesse campo podemos acessar os detalhes de cada participante.
        /// </summary>
        
        public IList<LogisticNeed> Associated { get; set; }

        /// <summary>
        ///     Observações.
        /// </summary>
        
        public string Comments { get; set; }

        /// <summary>
        ///     Comentários sobre atendimento anterior.
        /// </summary>
        
        public string CommentsAboutPreviousTreatment { get; set; }

        /// <summary>
        ///     Caso esta necessidade esteja associada com outras quem deve sair primeiro (ordem de origem menor)
        /// </summary>
        
        public int PickupOrder { get; set; }

        
        public IDictionary<string, string> Details
        {
            get; set;
        }

        /// <summary>
        ///     Caso esta necessidade esteja associada com outras quem deve chegar primeiro (ordem de destino menor)
        /// </summary>
        
        public int DeliveryOrder { get; set; }

        /// <summary>
        ///     Código do centro de custo.
        /// </summary>
        [JsonIgnore]
        public string CostCenterCode
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.CostCenterCode);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.CostCenterCode, value);
            }
        }

        /// <summary>
        ///     Nome do centro de custo.
        /// </summary>
        [JsonIgnore]
        public string CostCenterName
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.CostCenterName);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.CostCenterName, value);
            }
        }

        public override string ToString()
        {
            return ToStringBuilder.ReflectionToString(this);
        }

    }

}