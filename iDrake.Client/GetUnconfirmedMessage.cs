﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{

    
    
    public class GetUnconfirmedMessage : Message
    {

        public GetUnconfirmedMessage()
            : base(MessageType.GetUnconfirmedServiceRequest)
        {
        }

        public IEnumerable<long> MessageIdsToRead { get; set; }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<GetUnconfirmedMessage, ServiceMessage>(this);
        }

    }

}