﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Internal;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem de confirmação de serviço.
    ///     Esta mensagem deve ser enviada pelo fornecedor quando o mesmo quiser informar ao cliente que o serviço solicitado foi confirmado.
    /// </summary>
    
    
    public class ConfirmServiceMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public ConfirmServiceMessage()
            : base(MessageType.ConfirmService)
        {
            MainObjectType = typeof(LogisticNeed).Name;
            Provider = new Provider();
        }

        /// <summary>
        ///     Número da reserva.
        /// </summary>
        public string ReservationNumber { get; set; }

        /// <summary>
        ///     Link de rastreamento.
        /// </summary>
        public string TrackingLink { get; set; }

        /// <summary>
        ///     Código único que identifica o atendimento.
        /// </summary>
        public string TreatmentId { get; set; }

        /// <summary>
        ///     Início efetivo do serviço que pode ou não ser diferente do solicitado.
        /// </summary>
        public DateTime? Start { get; set; }

        /// <summary>
        ///     Fim efetivo do serviço que pode ou não ser diferente do solicitado.
        /// </summary>
        public DateTime? End { get; set; }

        /// <summary>
        ///     Endereço de origem efetivo do serviço que pode ou não ser diferente do solicitado.
        /// </summary>
        public Address Origin { get; set; }

        /// <summary>
        ///     Endereço de destino efetivo do serviço que pode ou não ser diferente do solicitado.
        /// </summary>
        public Address Destination { get; set; }

        /// <summary>
        ///     Ordem de atendimento de cada participante que pode ou não ser diferente do solicitado.
        /// </summary>
        public IList<ServiceOrder> ServiceOrder { get; set; }

        /// <summary>
        ///     Provedor de serviço.
        /// </summary>
        public Provider Provider { get; set; }

        /// <summary>
        ///     Valor APENAS do serviço.
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        ///     Taxas 
        /// </summary>
        public decimal Fees { get; set; }

        /// <summary>
        ///     Comissão.
        /// </summary>
        public decimal Commission { get; set; }

        /// <summary>
        ///     Flag que indica se os valores informados nesta confirmação devem ser somados com os valores informados no atendimento anterior.
        ///     Esta flag somente deverá ser usado se for a confirmação de atendimento de uma necessidade já atendida anteriormente e que 
        ///     sofreu alguma alteração por parte do Cliente.
        ///     Caso seu valor seja verdadeiro, o Drake irá obter os valores anteriores e somará com esse, caso esteja falso, o Drake 
        ///     irá substituir os valores do atendimento anterior.
        /// </summary>
        public bool SumWithValuesFromPreviousTreatment { get; set; }

        #region Propriedades usadas quando um atendimento for reusado

        /// <summary>
        ///     Abatimentos por estar reusando outro atendimento que foi cancelado anteriormente.
        /// </summary>
        public decimal RebatesByReuse { get; set; }

        /// <summary>
        ///     Código do atendimento que será reusado
        /// </summary>
        public string ReusedTreatmentId { get; set; }

        public string Details { get; set; }

        #endregion 

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<ConfirmServiceMessage, ServiceMessage>(this);
        }
    }

}