﻿using System;
using System.Configuration;

namespace iDrake.Client.Config
{
    
    /// <summary>
    ///     Estratégia de obtenção das configurações considerando que as mesmas estão em um arquivo de configuração.
    /// </summary>
    public class AppConfigStrategyConfiguration : IStrategyConfiguration
    {

        /// <summary>
        ///     Obtém o código do remetente.
        /// </summary>
        /// <returns>Código do remetente.</returns>
        public int? GetSenderId()
        {
            return Int32.Parse(GetConfig("SenderId", "É preciso informar o código do remetente das mensagens. Utilize 'SenderId'."));
        }

        /// <summary>
        ///     Obtém a chave privada que será usada para assinar a mensagem que será enviada.
        /// </summary>
        /// <returns>Chave privada que será usada para assinar a mensagem que será enviada.</returns>
        public string GetPrivateKey()
        {
            return GetConfig("PrivateKey", "É preciso informar a chave privada usada para assinar as mensagens que serão enviadas para o serviço de integração do DRAKE. Utilize 'PrivateKey'.");
        }

        /// <summary>
        ///      Obtém a URL do serviço de integração.
        /// </summary>
        /// <returns>URL do serviço de integração.</returns>
        public string GetIntegrationServiceUrl()
        {
            return GetConfig("DrakeIntegrationServiceUrl", "É preciso informar a URL do serviço de integração do DRAKE. Utilize 'DrakeIntegrationServiceUrl'.");
        }

        /// <summary>
        ///     Obtém a configuração que define se irá aceitar SSL inválido.
        /// </summary>
        /// <returns>Verdadeiro caso aceite e faslo caso contrário.</returns>
        public bool IsInvalidSslCertificateAccepted()
        {
            var accept = GetConfig("InvalidSslCertificatedAccepted");

            if (string.IsNullOrEmpty(accept))
            {
                return false;
            }

            try
            {
                return bool.Parse(accept);
            }
            catch (Exception)
            {
                throw new ConfigurationErrorsException("É preciso informar 'true' ou 'false' para a configuração 'InvalidSslCertificatedAccepted'.");
            }
        }

        protected string GetConfig(string key, string exceptionMessage = null)
        {
            var config = ConfigurationManager.AppSettings[key];

            if (string.IsNullOrEmpty(config) && exceptionMessage != null)
            {
                throw new ConfigurationErrorsException(exceptionMessage);
            }

            return config;
        }

    }

}
