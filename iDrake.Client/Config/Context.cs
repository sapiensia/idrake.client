﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using AutoMapper;
using iDrake.Client.Internal;
using iDrake.Client.Reports;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;
using iDrake.Client.Utils;
using Newtonsoft.Json.Serialization;

namespace iDrake.Client.Config
{
    /// <summary>
    ///     Contexto entre a API e o serviço.
    /// </summary>
    public sealed class Context
    {

        /// <summary>
        ///     Configuração.
        /// </summary>
        private IStrategyConfiguration _configuration = new AppConfigStrategyConfiguration();

        /// <summary>
        ///     Obtém a configuração usada no contexto atual.
        /// </summary>
        public IStrategyConfiguration Configuration
        {
            get
            {
                return _configuration;
            }

            set
            {
                _configuration = value;
            }
        }       

        /// <summary>
        ///     Singleton do contexto.
        /// </summary>
        private static Context _context;

        /// <summary>
        ///     Construtor.
        /// </summary>
        private Context()
        {
            AutoMapperConfiguration.Configure();
        }

        /// <summary>
        ///     Obtém o contexto atual.
        /// </summary>
        public static Context Current
        {
            get
            {
                if (_context == null)
                {
                    _context = new Context();
                }

                return _context;
            }
        }

        

        /// <summary>
        ///     Enviar mensagens.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração.</param>
        /// <param name="messages">Mensagens para serem enviadas.</param>
        public void SendMessages(IStrategyConfiguration strategyConfiguration, IList<Message> messages)
        {
            List<ServiceMessage> serviceMessages = messages.Select(m => m.GetEquivalentServiceMessage()).ToList();

            string messagesInJsonFormat = JsonHelper.JsonSerializer(serviceMessages, false, new DefaultContractResolver());

            HttpResult httpResult = HttpUtil.Post(
                strategyConfiguration.GetIntegrationServiceUrl() + "/Send",
                GenerateHeader(strategyConfiguration),
                HttpUtil.ApplicationJsonContentType, 
                messagesInJsonFormat,
                strategyConfiguration.GetPrivateKey(),
                strategyConfiguration.IsInvalidSslCertificateAccepted());

            var serviceResult = JsonHelper.JsonDeserialize<IList<Result>>(httpResult.Content);

            LoadGeneratedIds(messages, serviceResult);

            HandleException(httpResult, serviceResult);
        }

        /// <summary>
        ///     Confirmar leitura de mensagens.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração.</param>
        /// <param name="messageIds">Código das mensagens lidas.</param>
        public void ConfirmReadedMessages(IStrategyConfiguration strategyConfiguration, IDictionary<long, string> messageIds)
        {
            IEnumerable<ServiceMessage> messages =
                messageIds.Select(x => new ServiceMessage {Id = x.Key, ExternalId = x.Value, Type = MessageType.ConfirmReceivedMessage });

            string messagesInJsonFormat = JsonHelper.JsonSerializer(messages, false, new DefaultContractResolver());

            HttpResult httpResult = HttpUtil.Post(
                strategyConfiguration.GetIntegrationServiceUrl() + "/Control",
                GenerateHeader(strategyConfiguration),
                HttpUtil.ApplicationJsonContentType, 
                messagesInJsonFormat,
                strategyConfiguration.GetPrivateKey(),
                strategyConfiguration.IsInvalidSslCertificateAccepted());

            var serviceResult = JsonHelper.JsonDeserialize<IList<Result>>(httpResult.Content);

            HandleException(httpResult, serviceResult);
        }

        public BillEntriesReport GetBillEntriesReport(IStrategyConfiguration strategyConfiguration, DateTime endDate)
        {
            var serviceMessage = new ServiceMessage
            {
                SenderId = strategyConfiguration.GetSenderId(),
                End = endDate,
                Type = MessageType.GetBillEntries
            };
            
            var messageInJsonFormat = JsonHelper.JsonSerializer(serviceMessage, false, new DefaultContractResolver());
            
            HttpResult httpResult = HttpUtil.Post(
              strategyConfiguration.GetIntegrationServiceUrl() + "/Report",
              GenerateHeader(strategyConfiguration),
              HttpUtil.ApplicationJsonContentType,
              messageInJsonFormat,
              strategyConfiguration.GetPrivateKey(),
              strategyConfiguration.IsInvalidSslCertificateAccepted());

            var serviceResult = JsonHelper.JsonDeserialize<Result>(httpResult.Content);

            HandleException(httpResult, new[] { serviceResult });

            return JsonHelper.JsonDeserialize<BillEntriesReport>(httpResult.Content);
        }

        /// <summary>
        ///     Ler mensagens.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração.</param>
        /// <param name="messages">Mensagem de consulta.</param>
        /// <returns>Mensagens lidas.</returns>
        public IList<Message> ReadMessages(IStrategyConfiguration strategyConfiguration, IList<Message> messages)
        {
            List<ServiceMessage> serviceMessages = messages.Select(m => m.GetEquivalentServiceMessage()).ToList();

            string messagesInJsonFormat = JsonHelper.JsonSerializer(serviceMessages, false, new DefaultContractResolver());

            HttpResult httpResult = HttpUtil.Post(
                strategyConfiguration.GetIntegrationServiceUrl() + "/Receive",
                GenerateHeader(strategyConfiguration),
                HttpUtil.ApplicationJsonContentType,
                messagesInJsonFormat,
                strategyConfiguration.GetPrivateKey(), 
                strategyConfiguration.IsInvalidSslCertificateAccepted());
            
            var serviceResult = JsonHelper.JsonDeserialize<IList<Result>>(httpResult.Content);

            LoadGeneratedIds(messages, serviceResult);
            HandleException(httpResult, serviceResult);

            var serviceMessagesReturned = JsonHelper.JsonDeserialize<IList<ServiceMessage>>(httpResult.Content);

            List<Message> clientMessages = serviceMessagesReturned.Select(m => m.GetEquivalentClientMessage()).ToList();

            return clientMessages;
        }

        private void LoadGeneratedIds(IList<Message> messages, IList<Result> serviceResult)
        {
            if (serviceResult == null || messages == null)
            {
                return;
            }

            foreach (var message in messages)
            {
                var messageResult = serviceResult.FirstOrDefault(x => x.ClientGuid == message.ClientGuid);

                if (messageResult != null)
                {
                    message.Id = messageResult.GeneratedId;
                }
            }
        }

        /// <summary>
        ///     Gera o cabeçalho da requisição DRAKE.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração.</param>
        /// <returns>Cabeçalho.</returns>
        private IDictionary<string, string> GenerateHeader(IStrategyConfiguration strategyConfiguration)
        {
            var header = new Dictionary<string, string>
                {
                    { SecurityHelper.DrakeSenderIdHeaderName, strategyConfiguration.GetSenderId().ToString() }
                };

            return header;
        }

        /// <summary>
        ///     Trata as exceções, caso existam.
        /// </summary>
        /// <param name="httpResult">Resultado HTTP.</param>
        /// <param name="serviceResult"></param>
        private void HandleException(HttpResult httpResult, IEnumerable<Result> serviceResult)
        {
            if (httpResult.Status == HttpStatusCode.OK)
            {
                return;
            }

            if (httpResult.Status == HttpStatusCode.BadRequest)
            {
                var serviceErrors = serviceResult.Select(x => x.Validation).ToList();

                var clientErrors = Mapper.Map<IList<Stubs.ValidationResult>, IList<ValidationResult>>(serviceErrors);

                throw new OperationException(clientErrors);
            }
                
            if (httpResult.Status == HttpStatusCode.Unauthorized)
            {
                throw new SecurityException(httpResult.Content);
            }

            throw new OperationException(httpResult.Content);
        }
    }
}