﻿namespace iDrake.Client.Config
{
   
    public interface IStrategyConfiguration
    {

        int? GetSenderId();

        string GetPrivateKey();

        string GetIntegrationServiceUrl();

        bool IsInvalidSslCertificateAccepted();

    }

}
