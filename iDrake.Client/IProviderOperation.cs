﻿using System.Collections.Generic;
using iDrake.Client.Config;

namespace iDrake.Client
{
    public interface IProviderOperation
    {
        /// <summary>
        ///     Confirmar atendimento de serviço.
        /// </summary>
        /// <param name="message">Mensagem de confirmação de atendimento de serviço.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation ConfirmService(ConfirmServiceMessage message);

        /// <summary>
        ///     Confirmar atendimento de serviços.
        /// </summary>
        /// <param name="messages">Mensagens de confirmações de atendimentos de serviços.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation ConfirmServices(IEnumerable<ConfirmServiceMessage> messages);

        /// <summary>
        ///     Recusar solicitação de atendimento.
        /// </summary>
        /// <param name="message">Mensagem de recusa de solicitação de atendimento.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation RefuseRequest(RefuseRequestMessage message);

        /// <summary>
        ///     Recusar solicitações de atendimentos.
        /// </summary>
        /// <param name="messages">Mensagens de recusa de solicitações de atendimentos.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation RefuseRequests(IEnumerable<RefuseRequestMessage> messages);

        /// <summary>
        ///     Cancelar serviço.
        /// </summary>
        /// <param name="message">Mensagem de cancelamento de serviço.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation CancelService(CancelServiceMessage message);

        /// <summary>
        ///     Cancelar serviços.
        /// </summary>
        /// <param name="messages">Mensagens de cancelamento de serviços.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation CancelServices(IEnumerable<CancelServiceMessage> messages);

        /// <summary>
        ///     Recusar solicitação de cancelamento.
        /// </summary>
        /// <param name="message">Mensagem de recusa de solicitação de cancelamento.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation RefuseCancelation(RefuseCancelationMessage message);

        /// <summary>
        ///     Recusar solicitações de cancelamentos.
        /// </summary>
        /// <param name="messages">Mensagens de recusa de solicitações de cancelamentos.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation RefuseCancelations(IEnumerable<RefuseCancelationMessage> messages);

        /// <summary>
        ///     Confirmar cancelamento solicitado pelo cliente (sem reembolso).
        /// </summary>
        /// <param name="message">Mensagem de confirmação de cancelamento solicitado pelo cliente.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation CancelConfirmation(CancelationConfirmationMessage message);

        /// <summary>
        ///     Confirmar cancelamentos solicitados pelo cliente (sem reembolso).
        /// </summary>
        /// <param name="messages">Mensagens de confirmações de cancelamentos solicitados pelo cliente.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation CancelConfirmations(IEnumerable<CancelationConfirmationMessage> messages);

        /// <summary>
        ///     Confirmar cancelamento solicitado pelo cliente (com reembolso).
        /// </summary>
        /// <param name="message">Mensagem de confirmação de cancelamento solicitado pelo cliente com reembolso.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation CancelConfirmationWithRefund(CancelationConfirmationWithRefundMessage message);

        /// <summary>
        ///     Confirmar cancelamento solicitado pelo cliente com taxa de cancelamento.
        /// </summary>
        /// <param name="message">Mensagem de confirmação de cancelamento solicitado pelo cliente com taxa de cancelamento.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation CancelConfirmationWithFee(CancelationConfirmationWithFeeMessage message);

        /// <summary>
        ///     Confirmar cancelamentos solicitados pelo cliente (com reembolso).
        /// </summary>
        /// <param name="messages">Mensagens de confirmações de cancelamentos solicitados pelo cliente com reembolso.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation CancelConfirmationsWithRefund(IEnumerable<CancelationConfirmationWithRefundMessage> messages);

        /// <summary>
        ///     Confirmar cancelamentos solicitados pelo cliente com taxa de cancelamento.
        /// </summary>
        /// <param name="messages">Mensagens de confirmações de cancelamentos solicitados pelo cliente com taxa de canelamento.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation CancelConfirmationsWithFee(IEnumerable<CancelationConfirmationWithFeeMessage> messages);

        /// <summary>
        ///     Notificar não utilização de serviço.
        /// </summary>
        /// <param name="message">Mensagem de notificação de não utilização de serviço.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation NotifyNoShow(NotifyNoShowMessage message);

        /// <summary>
        ///     Notificar não utilização de serviços.
        /// </summary>
        /// <param name="messages">Mensagens de notificação de não utilização de serviço.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation NotifyNoShows(IEnumerable<NotifyNoShowMessage> messages);

        /// <summary>
        ///     Notificar utilização de serviço.
        /// </summary>
        /// <param name="message">Mensagem de utilização de serviço.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation NotifyUse(NotifyUseMessage message);

        /// <summary>
        ///     Notificar utilização de serviços.
        /// </summary>
        /// <param name="messages">Mensagens de utilização de serviços.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation NotifyUses(IEnumerable<NotifyUseMessage> messages);

        /// <summary>
        ///     Solicitar aprovação de fatura.
        /// </summary>
        /// <param name="message">Mensagem contendo a fatura que precisa ser aprovada.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation InvoiceApprovalRequest(InvoiceApprovalRequestMessage message);

        /// <summary>
        ///     Solicitar aprovação de faturas. 
        /// </summary>
        /// <param name="messages">Mensagens contendo as faturas que precisam ser aprovadas.</param>
        /// <returns>Operação do fornecedor.</returns>
        ProviderOperation InvoiceApprovalRequests(IEnumerable<InvoiceApprovalRequestMessage> messages);

        /// <summary>
        /// Confirmar que uma mensagem foi processada.
        /// </summary>
        /// <param name="message">Mensagem de confirmação de processamento.</param>
        /// <returns>Operação do cliente.</returns>
        ProviderOperation ConfirmProcessing(ConfirmProcessingMessage message);
        
        /// <summary>
        ///     Executa a operação.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração que será usada para executar a operação.</param>
        /// <returns>Mensagens enviadas com os códigos carregados.</returns>
        void Execute(IStrategyConfiguration strategyConfiguration);

        /// <summary>
        ///     Executa a operação.
        /// </summary>
        /// <returns>Mensagens enviadas com os códigos carregados.</returns>
        void Execute();
    }
}