﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem de confirmação de cancelamento sem reembolso.
    ///     Esta mensagem deve ser enviada pelo fornecedor quando o mesmo quiser confirmar o cancelamento de um serviço sem reembolso para o cliente.
    /// </summary>
    
    
    public class CancelationConfirmationMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public CancelationConfirmationMessage()
            : base(MessageType.CancelationConfirmation)
        {
            MainObjectType = typeof(LogisticNeed).Name;
        }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<CancelationConfirmationMessage, ServiceMessage>(this);
        }
    }
}