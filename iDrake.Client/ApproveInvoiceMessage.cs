﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{

    /// <summary>
    ///     Mensagem de aprovação de fatura.
    ///     Esta mensagem deve ser enviada pelo cliente quando o mesmo quiser aprovar uma fatura enviada por um fornecedor.
    /// </summary>
    
    
    public class ApproveInvoiceMessage : Message
    {

        /// <summary>
        ///     Construtor.
        /// </summary>
        public ApproveInvoiceMessage()
            : base(MessageType.ApproveInvoice)
        {
            MainObjectType = typeof(Invoice).Name;
        }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<ApproveInvoiceMessage, ServiceMessage>(this);
        }

    }

}
