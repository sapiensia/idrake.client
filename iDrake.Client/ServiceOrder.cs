﻿using System;
using System.Runtime.Serialization;

namespace iDrake.Client
{
    /// <summary>
    ///     Ordem em que o serviço será atendido.
    /// </summary>
    
    
    public class ServiceOrder
    {
        /// <summary>
        ///     Código DRAKE do participante da necessidade de logística.
        /// </summary>
        
        public long ParticipantDrakeId { get; set; }

        /// <summary>
        ///     Ordem de origem, números menores saem primeiro.
        /// </summary>
        
        public int PickupOrder { get; set; }

        /// <summary>
        ///     Ordem de destino, números menores chegam primeiro.
        /// </summary>
        
        public int DeliveryOrder { get; set; }
    }
}