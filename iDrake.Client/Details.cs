﻿namespace iDrake.Client
{
    
    public static class Details
    {
        public const string CostCenterCode = "COST_CENTER_CODE";
        public const string CostCenterName = "COST_CENTER_NAME";
        public const string AccountName = "ACCOUNT_NAME";
        public const string AccountNumber = "ACCOUNT_NUMBER";
        public const string PhoneNumbers = "PHONE_NUMBERS";
        public const string Passports = "PASSPORTS";
        public const string Emails = "EMAILS";
        public const string Reason = "REASON";
        public const string Cpf = "CPF";
        public const string Identity = "IDENTITY";
        public const string Comments = "COMMENTS";
        public const string Company = "COMPANY";
        public const string Job = "JOB";
        public const string RequesterEmail = "REQUESTER_EMAIL";
        public const string RequesterPhones = "REQUESTER_PHONES";
        public const string DateOfBirth = "DATE_OF_BIRTH";
    }
}
