﻿namespace iDrake.Client
{

    public enum InvoiceItemExtraValueStatus
    {

        ApprovalRequested = 0,
        Approved = 1,
        Refused = 2

    }

}
