﻿namespace iDrake.Client.Stubs.Enums
{
    public enum MessageType
    {
        /// <summary>
        ///     Esta mensagem é enviada pela Empresa toda vez que necessitar de atendimento logístico pelo fornecedor.
        /// </summary>
        ServiceRequest = 0,

        /// <summary>
        ///     Esta mensagem deverá ser enviada pelo fornecedor em resposta à mensagem de SolicitacaoDeAtendimento caso possa atende-la
        /// </summary>
        ConfirmService = 1,

        /// <summary>
        ///     Esta mensagem deverá ser enviada pelo fornecedor para cada necessidade  que não poderá ser atendida.
        /// </summary>
        RefuseRequest = 2,

        /// <summary>
        ///     Esta mensagem deverá ser enviada  pela empresa cliente para solicitar o cancelamento de uma necessidade previamente atendida.
        /// </summary>
        CancelationRequest = 3,

        /// <summary>
        ///     Esta mensagem deverá ser enviada  pelo fornecedor,para cada mensagem de solicitação de cancelamento de necessidade recebida, desde que o cancelamento seja possível sem custo.
        /// </summary>
        CancelationConfirmation = 4,

        /// <summary>
        ///     Esta mensagem deverá ser enviada  pelo fornecedor,para cada mensagem de solicitação de cancelamento de necessidade recebida, desde que o cancelamento seja possível mas através de reembolso.
        /// </summary>
        CancelationConfirmationWithRefund = 5,

        /// <summary>
        ///     Esta mensagem deverá ser enviada  pelo fornecedor,para cada mensagem de solicitação de cancelamento de necessidade recebida que não puder ser cancelada.
        /// </summary>
        RefuseCancelation = 6,

        /// <summary>
        ///     Esta mensagem deve ser enviada pelo cliente para solicitar a alteração do atendimento especificado
        /// </summary>
        ChangeRequest = 7,

        /// <summary>
        ///     Esta mensagem pode ser enviada pelo fornecedor para informar que um atendimento confirmado não poderá mais ser fornecido.
        /// </summary>
        CancelService = 9,

        /// <summary>
        ///     Esta mensagem deve ser enviada pelo fornecedor para informar que a necessidade foi efetivamente utilizada
        /// </summary>
        NotifyUse = 10,

        /// <summary>
        ///     Esta mensagem deve ser enviada pelo fornecedor para informar que a necessidade não foi utilizada devido a ausencia da pessoa que deveria utiliza-la
        /// </summary>
        NotifyNoShow = 11,

        // Esta mensagem deve ser enviada pelo fornecedor quando ele precisar da lista de mensagens que ele ainda não confirmou o recebimento.
        GetUnconfirmedServiceRequest = 12,

        /// <summary>
        ///     Esta mensagem deve ser enviada pelo cliente Drake ou Fornecedor para confirmar o recebimento de uma mensagem.
        /// </summary>
        ConfirmReceivedMessage = 13,

        /// <summary>
        ///     Esta mensagem deve ser enviada pelo fornecedor quando ele precisar solicitar uma aprovação de uma nota para faturamento.
        /// </summary>
        InvoiceApprovalRequest = 14,

        /// <summary>
        ///     Esta mensagem deve ser enviada pelo cliente quando o mesmo quiser aceitar uma nota fiscal enviada pelo fornecedor.
        /// </summary>
        ApproveInvoice = 15,

        /// <summary>
        ///     Esta mensagem deve ser enviada pelo cliente quando o mesmo quiser recusar uma nota fiscal enviada pelo fornecedor.
        /// </summary>
        RefuseInvoice = 16,

        /// <summary>
        /// Esta mensagem é gerada pelo serviço quando o remetente de uma mensagem original solicitou receber uma notificação quando o destinatário informar que recebeu a mensagem.
        /// </summary>
        Acknowledgment = 17,

        GetBillEntries = 18,

        /// <summary>
        /// Esta mensagem deve ser enviada pelo fornecedor quando o mesmo quiser confirmar o cancelamento de um serviço porém cobrando uma taxa (cancelado com pagamento).
        /// </summary>
        CancelationConfirmationWithFee = 19,
        
        
        /// <summary>
        /// Esta mensagem indica que uma outra mensagem foi processada com sucesso
        /// </summary>
        ConfirmProcessing = 20

    }

}