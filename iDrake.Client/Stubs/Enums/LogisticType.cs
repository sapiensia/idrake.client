﻿namespace iDrake.Client.Stubs.Enums
{
    public enum LogisticType
    {
        RoadTravel = 0,
        FlightTravel = 1,
        Hotel = 2,
        CarRental = 3
    }
}