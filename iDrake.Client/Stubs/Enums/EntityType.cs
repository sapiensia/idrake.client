﻿namespace iDrake.Client.Stubs.Enums
{
    public enum EntityType
    {
        PrivateIndividual = 0,

        LegalEntity = 1
    }
}