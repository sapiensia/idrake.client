﻿using System.Collections.Generic;

namespace iDrake.Client.Stubs
{

    public class Invoice
    {
        public long? DrakeId { get; set; }

        public string ExternalId { get; set; }

        public IList<InvoiceItem> Items { get; set; }

        public virtual ICollection<Detail> Details { get; set; } 
    }

}
