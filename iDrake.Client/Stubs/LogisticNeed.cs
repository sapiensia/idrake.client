﻿using System;
using System.Collections.Generic;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client.Stubs
{
    /// <summary>
    ///     Representa uma necessidade logística.
    /// </summary>
    public class LogisticNeed
    {
        public virtual long Id { get; set; }

        /// <summary>
        ///     Código da necessidade no Drake.
        /// </summary>
        public virtual long DrakeId { get; set; }

        public virtual Participant Participant { get; set; }

        /// <summary>
        ///     Data e Hora do início da necessidade.
        /// </summary>
        public virtual DateTime? Start { get; set; }

        /// <summary>
        ///     Data e Hora do Fim da necessidade. Normalmente usado apenas para necessidades que tem fim conhecido como Hospedagem.
        /// </summary>
        public virtual DateTime? End { get; set; }

        /// <summary>
        ///     Um dos tipos padrões de Logisitca: Transporte Aereo, terrestre, hospedagem ou locação
        /// </summary>
        public virtual LogisticType Type { get; set; }

        /// <summary>
        ///     No caso de passagens aéreas os subtipo é a classe.
        ///     No caso de transporte terrestre o tipo de transporte : Carro, Onibus, Van, etc...
        ///     No caso de Hospedagem o tipo de quarto : Single, Double, etc...
        /// </summary>
        public virtual string Subtype { get; set; }

        /// <summary>
        ///     Endereço de origem
        /// </summary>
        public virtual Address Origin { get; set; }

        /// <summary>
        ///     Endereço de destino
        /// </summary>
        public virtual Address Destination { get; set; }

        /// <summary>
        ///     Lista de necessidade associadas a esta necessidade. No Drake cada necessidade pode ser atendida separadamente ou ser associada para ser atendida em conjunto.
        ///     ex: 3 pessoas precisam de carro , temos 3 necessidades mas essas 3 necessidades serão associadas numa necessidade de carro contendo os 3 participantes
        ///     nesse campo podemos acessar os detalhes de cada participante
        /// </summary>
        public virtual IList<LogisticNeed> Associated { get; set; }

        /// <summary>
        ///     Observação colocada pelo solicitante
        /// </summary>
        public virtual string Comments { get; set; }

        /// <summary>
        ///     Comentários sobre atendimento anterior.
        /// </summary>
        public virtual string CommentsAboutPreviousTreatment { get; set; }

        /// <summary>
        ///     Caso esta necessidade esteja associada com outras quem deve sair primeiro (ordem de origem menor)
        /// </summary>
        public virtual int PickupOrder { get; set; }

        /// <summary>
        ///     Caso esta necessidade esteja associada com outras quem deve chegar primeiro (ordem de destino menor)
        /// </summary>
        public virtual int DeliveryOrder { get; set; }

        public virtual ICollection<Detail> Details { get; set; } 

    }
}