﻿namespace iDrake.Client.Stubs
{

    public class Detail
    {
        
        public Detail()
        {
            
        }

        public Detail(string key, string value)
        {
            Key = key;
            Value = value;
        }

        /// <summary>
        /// Nome do detalhe.
        /// </summary>
        public virtual string Key { get; set; }

        /// <summary>
        /// Descrição do detalhe.
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Valor do detalhe.
        /// </summary>
        public virtual string Value { get; set; }

    }

}
