﻿using System;
using System.Collections.Generic;
using AutoMapper;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client.Stubs
{
    public class ServiceMessage
    {

        public long? HttpMessageId { get; set; }

        /// <summary>
        ///     Identificador do objeto no sistema, esse campo não deve ser preenchido pelo remetente e estará diponível após a recepção da mensagem pelo Serviço de integração do drake
        /// </summary>
        public long? Id { get; set; }

        public Guid? ClientGuid { get; set; }

        /// <summary>
        ///     Data que a mensagem foi enviada
        /// </summary>
        public DateTime? Sent { get; set; }

        /// <summary>
        ///     Data que a mensagem foi recebida
        /// </summary>
        public DateTime? Received { get; set; }

        /// <summary>
        ///     Entidade que enviou a mensagem
        /// </summary>
        public int? SenderId { get; set; }

        /// <summary>
        ///     Entidade que deve receber a mensagem
        /// </summary>
        public int? RecipientId { get; set; }

        public string SenderUserName { get; set; }

        /// <summary>
        ///     Flag que indica se a mensagem é uma confirmação de recebimento ou não.
        /// </summary>
        public bool? Acknowledgment { get; set; }

        /// <summary>
        ///     Código da mensagem ao qua a confirmação de recebimento se relaciona.
        /// </summary>
        public long? ReceivedMessageId { get; set; }

        /// <summary>
        ///     Tipo de mensagem
        /// </summary>
        public MessageType? Type { get; set; }

        /// <summary>
        ///     Codigo da Necessidade no drake, sendo tratada pela mensagem
        /// </summary>
        public long? DrakeId { get; set; }

        /// <summary>
        ///     Codigo da necessidade no sistema externo ao drake
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        ///     Dados da necessidade, apenas para mensagens do tipo SolicitacaoDeAtendimento,SolicitacaoDeAlteracao
        /// </summary>
        public LogisticNeed LogisticNeed { get; set; }

        /// <summary>
        ///     Dados da fatura, apenas para mensagens do tipo SolicitacaoAprovacaoFatura.
        /// </summary>
        public Invoice Invoice { get; set; }

        /// <summary>
        ///     Código do atendimento que cliente gostaria que fosse reusado.
        ///     Apenas mensagens de Solicitação de Serviço e Solicitação de Alteração de Serviço preenchem esse valor.
        /// </summary>
        public string TreatmentIdToTryReuse { get; set; }
        
        #region Propriedades usadas quando um atendimento for reusado
    
        /// <summary>
        ///     Código do atendimento que fornecedor reusou.
        ///     Apenas mensagens de Confirmação de Atendimento.
        /// </summary>
        public string ReusedTreatmentId { get; set; }

        /// <summary>
        ///     Abatimentos por estar reusando outro atendimento que foi cancelado anteriormente.
        /// </summary>
        public decimal? RebatesByReuse { get; set; }

        #endregion 

        /// <summary>
        ///     Detalhes da mensagem o contexto desse campo varia com o tipo de mensagem
        ///     Sugestão de utilização:
        ///     SolicitacaoDeAtendimento: Não deve conter nada.
        ///     ConfirmacaoDeAtendimento: Qualquer detalhe textual referente ao atendimento
        ///     RecusaDeAtendimento: Motivo pelo qual a necessidade não pode ser atendida
        /// </summary>
        public string Details { get; set; }

        #region Campos A Serem preenchidos em mensagens do tipo ConfirmacaoDeAtendimento e ConfirmacaoDeAlteracao

        /// <summary>
        ///     Numero da reserva, Apenas para mensagens do tipo ConfirmacaoDeAtendimento
        /// </summary>
        public string ReservationNumber { get; set; }

        /// <summary>
        /// Link de rastreamento, Apenas para mensagens do tipo ConfirmacaoDeAtendimento
        /// </summary>
        public string TrackingLink { get; set; }

        /// <summary>
        ///     Flag que indica se os valores informados nesta confirmação devem ser somados com os valores informados no atendimento anterior.
        ///     Esta flag somente deverá ser usado se for a confirmação de atendimento de uma necessidade já atendida anteriormente e que 
        ///     sofreu alguma alteração por parte do Cliente.
        ///     Caso seu valor seja verdadeiro, o Drake irá obter os valores anteriores e somará com esse, caso esteja falso, o Drake 
        ///     irá substituir os valores do atendimento anterior.
        /// </summary>
        public bool? SumWithValuesFromPreviousTreatment { get; set; }

        /// <summary>
        ///     Código único que identifica o atendimento. Apenas para mensagens do tipo ConfirmacaoDeAtendimento.
        /// </summary>
        public string TreatmentId { get; set; }

        /// <summary>
        ///     Valor a ser cobrado pelo serviço, Apenas para mensagens do tipo ConfirmacaoDeAtendimento
        /// </summary>
        public decimal? Price { get; set; }
        /// <summary>
        ///     Provedor de serviço.
        /// </summary>
        public string ProviderName { get; set; }

        public string ProviderId { get; set; }

        /// <summary>
        ///     Valor APENAS do serviço.
        /// </summary>
        public decimal? Value { get; set; }

        /// <summary>
        ///     Taxas 
        /// </summary>
        public decimal? Fees { get; set; }

        /// <summary>
        ///     Comissão.
        /// </summary>
        public decimal? Commission { get; set; }

        /// <summary>
        ///     Inicio efetivo do serviço que pode ou não ser diferente do solicitado, Apenas para mensagens do tipo ConfirmacaoDeAtendimento
        /// </summary>
        public DateTime? Start { get; set; }

        /// <summary>
        ///     Fim efetivo do serviço que pode ou não ser diferente do solicitado , Apenas para mensagens do tipo ConfirmacaoDeAtendimento
        /// </summary>
        public DateTime? End { get; set; }

        /// <summary>
        ///     Endereço de origem efetivo do serviço que pode ou não ser diferente do solicitado, Apenas para mensagens do tipo ConfirmacaoDeAtendimento
        /// </summary>
        public Address Origin { get; set; }

        /// <summary>
        ///     Endereço de destino efetivo do serviço que pode ou não ser diferente do solicitado , Apenas para mensagens do tipo ConfirmacaoDeAtendimento
        /// </summary>
        public Address Destination { get; set; }

        /// <summary>
        ///     Ordem de Atendimento de cada participante, Apenas para mensagens do tipo ConfirmacaoDeAtendimento
        /// </summary>
        public IList<ServiceOrder> ServiceOrder { get; set; }

        #endregion
        
        #region Propriedade específica para mensagem de leitura 
        
        public IEnumerable<long> MessageIdsToRead { get; set; }

        #endregion

        internal Message GetEquivalentClientMessage()
        {
            switch (Type)
            {
                case MessageType.ServiceRequest:
                    return Mapper.Map<ServiceMessage, ServiceRequestMessage>(this);

                case MessageType.ConfirmService:
                    return Mapper.Map<ServiceMessage, ConfirmServiceMessage>(this);

                case MessageType.RefuseRequest:
                    return Mapper.Map<ServiceMessage, RefuseRequestMessage>(this);

                case MessageType.ChangeRequest:
                    return Mapper.Map<ServiceMessage, ChangeRequestMessage>(this);

                case MessageType.CancelService:
                    return Mapper.Map<ServiceMessage, CancelServiceMessage>(this);

                case MessageType.NotifyNoShow:
                    return Mapper.Map<ServiceMessage, NotifyNoShowMessage>(this);

                case MessageType.NotifyUse:
                    return Mapper.Map<ServiceMessage, NotifyUseMessage>(this);

                case MessageType.CancelationRequest:
                    return Mapper.Map<ServiceMessage, CancelationRequestMessage>(this);

                case MessageType.CancelationConfirmationWithRefund:
                    return Mapper.Map<ServiceMessage, CancelationConfirmationWithRefundMessage>(this);

                case MessageType.CancelationConfirmationWithFee:
                    return Mapper.Map<ServiceMessage, CancelationConfirmationWithFeeMessage>(this);

                case MessageType.CancelationConfirmation:
                    return Mapper.Map<ServiceMessage, CancelationConfirmationMessage>(this);

                case MessageType.RefuseCancelation:
                    return Mapper.Map<ServiceMessage, RefuseCancelationMessage>(this);

                case MessageType.InvoiceApprovalRequest:
                    return Mapper.Map<ServiceMessage, InvoiceApprovalRequestMessage>(this);

                case MessageType.ApproveInvoice:
                    return Mapper.Map<ServiceMessage, ApproveInvoiceMessage>(this);

                case MessageType.RefuseInvoice:
                    return Mapper.Map<ServiceMessage, RefuseInvoiceMessage>(this);

                case MessageType.Acknowledgment:
                    return Mapper.Map<ServiceMessage, AcknowledgmentMessage>(this);
                
                case MessageType.ConfirmProcessing:
                    return Mapper.Map<ServiceMessage, ConfirmProcessingMessage>(this);

                case MessageType.ConfirmReceivedMessage:
                    return Mapper.Map<ServiceMessage, ConfirmServiceMessage>(this);

                default:
                    throw new OperationException(
                        string.Format("Tipo de mensagem recebida do serviço não suportada. " + Type));
            }
        }
    
    }

}