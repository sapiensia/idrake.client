﻿using System.Collections.Generic;

namespace iDrake.Client.Stubs
{

    public class InvoiceItemExtraValue
    {

        public string ExternalId { get; set; }

        public Participant Participant { get; set; }

        public decimal Value { get; set; }

        public InvoiceItem InvoiceItem { get; set; }

        public InvoiceItemExtraValueStatus Status { get; set; }

        public ICollection<Detail> Details { get; set; } 

    }

}
