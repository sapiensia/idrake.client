﻿using System.Collections.Generic;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client.Stubs
{
    public class Participant
    {
        /// <summary>
        ///     Codigo interno e imutavel da pessoa no drake
        /// </summary>
        public virtual long Id { get; set; }

        /// <summary>
        /// Código DRAKe do participante.
        /// </summary>
        public virtual long DrakeId { get; set; }

        /// <summary>
        ///     Nome Completo da pessoa
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        ///     Sexo
        /// </summary>
        public virtual Gender Gender { get; set; }

        /// <summary>
        ///     Numero da Identidade
        /// </summary>
        public virtual string Identity { get; set; }

        /// <summary>
        ///     Numero do Cpf
        /// </summary>
        public virtual string CPF { get; set; }

        /// <summary>
        ///     Numero do Passaporte Atual
        /// </summary>
        public virtual string Passport { get; set; }

        /// <summary>
        ///     Contatos telefonicos
        /// </summary>
        public virtual string Tels { get; set; }

        /// <summary>
        ///     Numero e nome do centro de custo
        /// </summary>
        public virtual string CostCenter { get; set; }

        /// <summary>
        ///     Endereço de email
        /// </summary>
        public virtual string Email { get; set; }

        public virtual string Hash { get; set; }

        public virtual ICollection<Detail> Details { get; set; } 

    }

}