﻿using System;

namespace iDrake.Client.Stubs
{

    public class Result
    {
        public long GeneratedId { get; set; }
        public Guid? ClientGuid { get; set; }

        public ValidationResult Validation { get; set; }

        public bool Success { get; set; }
    }

}
