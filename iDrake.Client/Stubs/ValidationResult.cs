﻿using System.Collections.Generic;

namespace iDrake.Client.Stubs
{
    /// <summary>
    ///     Resultado da validação.
    /// </summary>
    public class ValidationResult
    {
        /// <summary>
        ///     Mensagem enviada.
        /// </summary>
        public ServiceMessage Message { get; set; }

        /// <summary>
        ///     Erros encontrados durante a validação.
        /// </summary>
        public IList<ValidationFailure> Errors { get; set; }
    }
}