﻿namespace iDrake.Client.Stubs
{
    public class Attachment
    {
        /// <summary>
        ///     Nome do arquivo anexo
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Extensão do arquivo
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        ///     Conteudo do arquivo
        /// </summary>
        public byte[] Content { get; set; }
    }
}