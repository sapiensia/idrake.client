﻿namespace iDrake.Client.Stubs
{
    public class ServiceOrder
    {

        public ServiceOrder()
        {
            Participant = new Participant();
        }

        /// <summary>
        ///     Codigo da pessoa no drake
        /// </summary>
        public virtual Participant Participant { get; set; }

        /// <summary>
        ///     Ordem de origem, numeros menores saem primeiro
        /// </summary>
        public virtual int PickupOrder { get; set; }

        /// <summary>
        ///     Ordem de Destino, numeros menores chegam primeiro
        /// </summary>
        public virtual int DeliveryOrder { get; set; }
    }
}