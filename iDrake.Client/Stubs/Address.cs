﻿namespace iDrake.Client.Stubs
{
    /// <summary>
    ///     Endereço.
    /// </summary>
    public class Address
    {
        #region Propriedades

        public virtual long Id { get; set; }

        /// <summary>
        ///     Nome (necessário para casos em que o endereço é um Hotel, Aeroporto, Rodoviária etc).
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Rua.
        /// </summary>
        public virtual string Street { get; set; }

        /// <summary>
        ///     Número.
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        ///     Complemento.
        /// </summary>
        public virtual string Complement { get; set; }

        /// <summary>
        ///     Nome do bairro
        /// </summary>
        public virtual string District { get; set; }

        /// <summary>
        ///     Nome da cidade
        /// </summary>
        public virtual string City { get; set; }

        /// <summary>
        ///     Nome ou sigla do estado
        /// </summary>
        public virtual string State { get; set; }

        /// <summary>
        ///     Nome do País
        /// </summary>
        public virtual string Country { get; set; }

        /// <summary>
        ///     CEP
        /// </summary>
        public virtual string Zip { get; set; }

        /// <summary>
        ///     Latitude especificada de acordo com o sistema WGS84
        /// </summary>
        public virtual double? Latitude { get; set; }

        /// <summary>
        ///     Longitude especificada de acordo com o sistema WGS84
        /// </summary>
        public virtual double? Longitude { get; set; }

        /// <summary>
        ///     Codigo IATA apenas para aeroportos
        /// </summary>
        public virtual string IATA { get; set; }

        #endregion

    }

}