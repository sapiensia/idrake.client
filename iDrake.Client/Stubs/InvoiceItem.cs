﻿using System.Collections.Generic;

namespace iDrake.Client.Stubs
{

    public class InvoiceItem
    {

        /// <summary>
        /// Código da necessidade logística no Drake.
        /// </summary>
        public long DrakeId { get; set; }

        /// <summary>
        ///     Código externo (ex.: Número da reserva).
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        ///     Tarifa
        /// </summary>
        public decimal? Total { get; set; }

        public IList<InvoiceItemExtraValue> ExtraValues { get; set; }

        /// <summary>
        ///     Repasse (desconto).
        /// </summary>
        public decimal? Rebate { get; set; }

        /// <summary>
        ///     Comentários.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        ///     Status.
        /// </summary>
        public InvoiceItemStatus Status { get; set; }

        /// <summary>
        ///     Avaliação do cliente Drake.
        /// </summary>
        public virtual string Appraisal { get; set; }

        public virtual ICollection<Detail> Details { get; set; } 

    }

}
