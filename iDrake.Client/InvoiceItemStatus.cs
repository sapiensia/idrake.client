﻿namespace iDrake.Client
{

    public enum InvoiceItemStatus
    {

        ApprovalRequested = 0,
        Approved = 1,
        Refused = 2

    }

}
