﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using iDrake.Client.Utils;
using Newtonsoft.Json;

namespace iDrake.Client
{

    
    
    public class InvoiceItemExtraValue
    {

        public InvoiceItemExtraValue()
        {
            Details = new Dictionary<string, string>();       
        }

        #region Propriedades serializadas

        /// <summary>
        /// Valor.
        /// </summary>
        
        public decimal Value { get; set; }

        /// <summary>
        /// Trabalhador responsável pelo valor adicional.
        /// </summary>
        
        public Participant Participant { get; set; }

        /// <summary>
        ///     Status. Preenchido apenas pelo cliente Drake.
        /// </summary>
        
        public InvoiceItemExtraValueStatus Status { get; set; }

        /// <summary>
        /// Detalhes.
        /// </summary>
        
        public IDictionary<string, string> Details
        {
            get;
            set;
        }

        #endregion

        #region Wrappers dos detalhes

        /// <summary>
        /// Comentários e observações.
        /// </summary>
        [JsonIgnore]
        public string Comments
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.Comments);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.Comments, value);
            }
        }

        #endregion

    }

}
