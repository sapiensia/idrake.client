using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    public class ConfirmProcessingMessage : Message
    {
        public ConfirmProcessingMessage() : base(MessageType.ConfirmProcessing)
        {
            MainObjectType = null;
            ClientGuid = null;
        }
        
        public long ReceivedMessageId { get; set; }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<ConfirmProcessingMessage, ServiceMessage>(this);
        }
    }
}