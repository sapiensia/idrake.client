﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;
using Newtonsoft.Json;

namespace iDrake.Client
{

    /// <summary>
    ///     Mensagem contendo uma nota para faturamento.
    ///     Esta mensagem deve ser enviada pelo fornecedor quando o mesmo quiser informar ao cliente que existe uma nota para ser fatura.
    /// </summary>
    
     
    public class InvoiceApprovalRequestMessage : Message
    {

        public InvoiceApprovalRequestMessage()
            : base(MessageType.InvoiceApprovalRequest)
        {
            Invoice = new Invoice();
            MainObjectType = typeof(Invoice).Name;
        }

        public void AddItems(IList<InvoiceItem> items)
        {
            foreach (var item in items)
            {
                AddItem(item);
            }
        }
                
        public void AddItem(InvoiceItem item)
        {
            Invoice.Items.Add(item);
        }

        public void SetCostCenter(string code, string name)
        {
            Invoice.CostCenterCode = code;
            Invoice.CostCenterName = name;
        }
        [JsonIgnore]
        public IList<InvoiceItem> Items
        {
            get
            {
                return Invoice != null ? Invoice.Items : null;
            }
        }

        public string GetCostCenterCode()
        {
            return Invoice != null ? Invoice.CostCenterCode : null;
        }

        public string GetCostCenterName()
        {
            return Invoice != null ? Invoice.CostCenterName : null;
        }
            
        
        public Invoice Invoice { get; set; }  

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<InvoiceApprovalRequestMessage, ServiceMessage>(this);
        }

    }

}
