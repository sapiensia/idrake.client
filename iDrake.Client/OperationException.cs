﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iDrake.Client
{
    /// <summary>
    ///     Exceção lançada durante a execução de uma operação.
    /// </summary>
    public sealed class OperationException : Exception
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public OperationException()
        {
        }

        /// <summary>
        /// Construtor.
        /// </summary>
        /// <param name="errors">Erros.</param>
        public OperationException(IList<ValidationResult> errors)
            : base(GetMessage(errors))
        {     
            Errors = errors;    
        }

        /// <summary>
        ///     Construtor.
        /// </summary>
        /// <param name="message">Mensagem de erro.</param>
        public OperationException(string message)
            : base(message)
        {
        }

        /// <summary>
        ///     Construtor.
        /// </summary>
        /// <param name="message">Mensagem de erro.</param>
        /// <param name="innerException">Exceção raiz.</param>
        public OperationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        ///     Erros.
        /// </summary>
        public IList<ValidationResult> Errors { get; set; }

        private static string GetMessage(IList<ValidationResult> errors)
        {
            var message = new StringBuilder();

            if (errors != null && errors.Count > 0)
            {
                foreach (var validationResult in errors)
                {
                    if (validationResult != null && validationResult.Errors != null && validationResult.Errors.Count > 0)
                    {
                        foreach (var error in validationResult.Errors)
                        {
                            message.AppendLine(string.Format("{0} : {1}", error.PropertyName, error.ErrorMessage));
                        }
                    }
                }
            }

            return message.ToString();
        }

    }

}