﻿using System.Runtime.Serialization;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{

    public class AcknowledgmentMessage : Message
    {

        public AcknowledgmentMessage() : base(MessageType.Acknowledgment)
        {
            MainObjectType = null;
            ClientGuid = null;
        }

        
        public long ReceivedMessageId { get; set; }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return new ServiceMessage
            {
                SenderId = SenderId,
                RecipientId = RecipientId,
                DrakeId = DrakeId,
                Id = Id,
                ClientGuid = ClientGuid,
                Sent = Sent,
                ReceivedMessageId = ReceivedMessageId
            };
        }
    }
}
