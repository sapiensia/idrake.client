﻿namespace iDrake.Client
{
    /// <summary>
    ///     Validação da mensagem.
    /// </summary>
    public sealed class ValidationFailure
    {
        /// <summary>
        ///     Mensagem informativa sobre o erro encontrado.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        ///     Propriedade onde o erro foi encontrado.
        /// </summary>
        public string PropertyName { get; set; }
    }
}