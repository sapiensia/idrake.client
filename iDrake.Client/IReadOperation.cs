using System.Collections.Generic;
using iDrake.Client.Config;

namespace iDrake.Client
{
    public interface IReadOperation
    {
        IList<Message> ReadMessages(IStrategyConfiguration strategyConfiguration, IEnumerable<long> messageIdsToRead);
        IList<Message> ReadMessages(IStrategyConfiguration strategyConfiguration);
        IList<Message> ReadMessages(IEnumerable<long> idsMessageToRead);
        IList<Message> ReadMessages();

        void ConfirmReadedMessages(IList<long> readedMessageIds);
        void ConfirmReadedMessages(IStrategyConfiguration strategyConfiguration, IList<long> readedMessageIds);
        void ConfirmReadedMessages(IStrategyConfiguration strategyConfiguration);
        void ConfirmReadedMessages();
    }
}