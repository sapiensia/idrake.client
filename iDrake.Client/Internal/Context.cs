﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using AutoMapper;
using Drake.Cloud.Integration.Client.Stubs;
using Drake.Cloud.Integration.Client.Stubs.Enums;
using Drake.Cloud.Integration.Utilities.Helpers;
using Drake.Cloud.Integration.Utilities.Util;

namespace Drake.Cloud.Integration.Client.Internal
{
    /// <summary>
    ///     Contexto entre a API e o serviço.
    /// </summary>
    internal sealed class Context
    {
        /// <summary>
        ///     Singleton do contexto.
        /// </summary>
        private static Context _context;

        /// <summary>
        ///     Construtor.
        /// </summary>
        private Context()
        {
            AutoMapperConfiguration.Configure();
        }

        /// <summary>
        ///     Obtém o contexto atual.
        /// </summary>
        public static Context Current
        {
            get
            {
                if (_context == null)
                {
                    _context = new Context();
                }

                return _context;
            }
        }

        /// <summary>
        ///     Enviar mensagens.
        /// </summary>
        /// <param name="messages">Mensagens para serem enviadas.</param>
        public void SendMessages(IList<Message> messages)
        {
            List<ServiceMessage> serviceMessages = messages.Select(m => m.GetEquivalentServiceMessage()).ToList();

            string messagesInJsonFormat = JsonHelper.JsonSerializer(serviceMessages);

            HttpResult httpResult = HttpUtil.Post(Configuration.IntegrationServiceUrl + "/Send", GenerateHeader(),
                                                  HttpUtil.ApplicationJsonContentType, messagesInJsonFormat,
                                                  Configuration.PrivateKey);

            HandleException(httpResult);
        }

        /// <summary>
        ///     Confirmar leitura de mensagens.
        /// </summary>
        /// <param name="messageIds">Código das mensagens lidas.</param>
        public void ConfirmReadedMessages(IList<long> messageIds)
        {
            IEnumerable<ServiceMessage> messages =
                messageIds.Select(id => new ServiceMessage {Id = id, Type = MessageType.ConfirmReceivedMessage});

            string messagesInJsonFormat = JsonHelper.JsonSerializer(messages);

            HttpResult httpResult = HttpUtil.Post(Configuration.IntegrationServiceUrl + "/Control", GenerateHeader(),
                                                  HttpUtil.ApplicationJsonContentType, messagesInJsonFormat,
                                                  Configuration.PrivateKey);

            HandleException(httpResult);
        }

        /// <summary>
        ///     Ler mensagens.
        /// </summary>
        /// <param name="messages">Mensagem de consulta.</param>
        /// <returns>Mensagens lidas.</returns>
        public IList<Message> ReadMessages(IList<Message> messages)
        {
            List<ServiceMessage> serviceMessages = messages.Select(m => m.GetEquivalentServiceMessage()).ToList();

            string messagesInJsonFormat = JsonHelper.JsonSerializer(serviceMessages);

            HttpResult httpResult = HttpUtil.Post(Configuration.IntegrationServiceUrl + "/Receive", GenerateHeader(),
                                                  HttpUtil.ApplicationJsonContentType, messagesInJsonFormat,
                                                  Configuration.PrivateKey);

            HandleException(httpResult);

            var serviceMessagesReturned = JsonHelper.JsonDeserialize<IList<ServiceMessage>>(httpResult.Content);

            List<Message> clientMessages = serviceMessagesReturned.Select(m => m.GetEquivalentClientMessage()).ToList();

            return clientMessages;
        }

        /// <summary>
        ///     Gera o cabeçalho da requisição DRAKE.
        /// </summary>
        /// <returns>Cabeçalho.</returns>
        private IDictionary<string, string> GenerateHeader()
        {
            var header = new Dictionary<string, string>
                {
                    {SecurityHelper.DrakeSenderIdHeaderName, Configuration.SenderId.ToString()}
                };

            return header;
        }

        /// <summary>
        ///     Trata as exceções, caso existam.
        /// </summary>
        /// <param name="httpResult">Resultado HTTP.</param>
        private void HandleException(HttpResult httpResult)
        {
            if (httpResult.Status != HttpStatusCode.OK)
            {
                if (httpResult.Status == HttpStatusCode.BadRequest)
                {
                    var serviceErrors = JsonHelper.JsonDeserialize<IList<Stubs.ValidationResult>>(httpResult.Content);

                    IList<ValidationResult> clientErrors =
                        Mapper.Map<IList<Stubs.ValidationResult>, IList<ValidationResult>>(serviceErrors);

                    throw new OperationException {Errors = clientErrors};
                }
                else if (httpResult.Status == HttpStatusCode.Unauthorized)
                {
                    throw new SecurityException(httpResult.Content);
                }

                throw new OperationException(httpResult.Content);
            }
        }
    }
}