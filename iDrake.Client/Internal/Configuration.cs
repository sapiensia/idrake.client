﻿using System;
using System.Configuration;

namespace Drake.Cloud.Integration.Client.Internal
{
    /// <summary>
    ///     Configuração da API.
    /// </summary>
    internal class Configuration
    {
      
        /// <summary>
        ///     Obtém a URL do serviço de integração.
        /// </summary>
        public static string IntegrationServiceUrl
        {
            get 
            {
                return GetConfig("DrakeIntegrationServiceUrl", "É preciso informar a URL do serviço de integração do DRAKE. Utilize 'DrakeIntegrationServiceUrl'.");
            }
        }

        /// <summary>
        ///     Obtém o código do remetente.
        /// </summary>
        public static int SenderId
        {
            get 
            {
                return Int32.Parse(GetConfig("SenderId", "É preciso informar o código do remetente das mensagens. Utilize 'SenderId'."));
            }
        }

        /// <summary>
        ///     Obtém a chave privada que será usada para
        /// </summary>
        public static string PrivateKey
        {
            get 
            {
                return GetConfig("PrivateKey", "É preciso informar a chave privada usada para assinar as mensagens que serão enviadas para o serviço de integração do DRAKE. Utilize 'PrivateKey'.");
            }
        }

        private static string GetConfig(string key, string exceptionMessage)
        {
            var config = ConfigurationManager.AppSettings[key];

            if (string.IsNullOrEmpty(config))
            {
                throw new ConfigurationException(exceptionMessage);
            }

            return config;
        }
    }
}