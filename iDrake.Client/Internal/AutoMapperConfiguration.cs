﻿using System.Collections.Generic;
using AutoMapper;
using iDrake.Client.Stubs;

namespace iDrake.Client.Internal
{
    /// <summary>
    ///     Configuração do AutoMapper.
    /// </summary>
    internal class AutoMapperConfiguration
    {
        /// <summary>
        ///     Configurar.
        /// </summary>
        internal static void Configure()
        {
            
            #region Mensagens enviadas pelo cliente.

            Mapper.CreateMap<ServiceRequestMessage, ServiceMessage>()
                  .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                  .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                  .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                  .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                  .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                  .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                  .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                  .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                  .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                  .ForMember(dstn => dstn.LogisticNeed, opt => opt.MapFrom(orgn => Mapper.Map<LogisticNeed, Stubs.LogisticNeed>(orgn.LogisticNeed)))
                  .ForMember(dstn => dstn.TreatmentIdToTryReuse, opt => opt.MapFrom(orgn => orgn.TreatmentIdToTryReuse))
                  .ForMember(dstn => dstn.ServiceOrder, opt => opt.MapFrom(orgn => Mapper.Map<IList<ServiceOrder>, IList<Stubs.ServiceOrder>>(orgn.ServiceOrder)))
                  .ReverseMap();
            
            Mapper.CreateMap<ChangeRequestMessage, ServiceMessage>()
                  .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                  .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))    
                  .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                  .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                  .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                  .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                  .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                  .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                  .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                  .ForMember(dstn => dstn.LogisticNeed, opt => opt.MapFrom(orgn => Mapper.Map<LogisticNeed, Stubs.LogisticNeed>(orgn.LogisticNeed)))
                  .ForMember(dstn => dstn.TreatmentIdToTryReuse, opt => opt.MapFrom(orgn => orgn.TreatmentIdToTryReuse))
                  .ForMember(dstn => dstn.ServiceOrder, opt => opt.MapFrom(orgn => Mapper.Map<IList<ServiceOrder>, IList<Stubs.ServiceOrder>>(orgn.ServiceOrder)))
                  .ReverseMap();

            Mapper.CreateMap<CancelationRequestMessage, ServiceMessage>()
                  .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                  .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                  .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                  .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                  .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                  .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                  .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                  .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                  .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                  .ReverseMap();

            Mapper.CreateMap<ApproveInvoiceMessage, ServiceMessage>()
                  .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                  .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                  .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                  .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                  .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                  .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                  .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                  .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                  .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                  .ReverseMap();

            Mapper.CreateMap<RefuseInvoiceMessage, ServiceMessage>().ReverseMap();

            #endregion

            #region Mensagens enviadas por fornecedores.

            Mapper.CreateMap<ConfirmServiceMessage, ServiceMessage>()
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                .ForMember(dstn => dstn.ExternalId, opt => opt.MapFrom(orgn => orgn.ExternalId))
                .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                .ForMember(dstn => dstn.Value, opt => opt.MapFrom(orgn => orgn.Value))
                .ForMember(dstn => dstn.Fees, opt => opt.MapFrom(orgn => orgn.Fees))
                .ForMember(dstn => dstn.Commission, opt => opt.MapFrom(orgn => orgn.Commission))
                .ForMember(dstn => dstn.ReservationNumber, opt => opt.MapFrom(orgn => orgn.ReservationNumber))
                .ForMember(dstn => dstn.TreatmentId, opt => opt.MapFrom(orgn => orgn.TreatmentId))
                .ForMember(dstn => dstn.RebatesByReuse, opt => opt.MapFrom(orgn => orgn.RebatesByReuse))
                .ForMember(dstn => dstn.ReusedTreatmentId, opt => opt.MapFrom(orgn => orgn.ReusedTreatmentId))
                .ForMember(dstn => dstn.ProviderId, opt => opt.MapFrom(orgn => orgn.Provider.Id))
                .ForMember(dstn => dstn.ProviderName, opt => opt.MapFrom(orgn => orgn.Provider.Name))
                .ForMember(dstn => dstn.SumWithValuesFromPreviousTreatment, opt => opt.MapFrom(orgn => orgn.SumWithValuesFromPreviousTreatment))
                .ForMember(dstn => dstn.Details, opt => opt.MapFrom(orgn => orgn.Details));

            Mapper.CreateMap<ServiceMessage, ConfirmServiceMessage>()
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                .ForMember(dstn => dstn.ExternalId, opt => opt.MapFrom(orgn => orgn.ExternalId))
                .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                .ForMember(dstn => dstn.Value, opt => opt.MapFrom(orgn => orgn.Value))
                .ForMember(dstn => dstn.Fees, opt => opt.MapFrom(orgn => orgn.Fees))
                .ForMember(dstn => dstn.Commission, opt => opt.MapFrom(orgn => orgn.Commission))
                .ForMember(dstn => dstn.ReservationNumber, opt => opt.MapFrom(orgn => orgn.ReservationNumber))
                .ForMember(dstn => dstn.TreatmentId, opt => opt.MapFrom(orgn => orgn.TreatmentId))
                .ForMember(dstn => dstn.RebatesByReuse, opt => opt.MapFrom(orgn => orgn.RebatesByReuse))
                .ForMember(dstn => dstn.ReusedTreatmentId, opt => opt.MapFrom(orgn => orgn.ReusedTreatmentId))
                .ForMember(dstn => dstn.Details, opt => opt.MapFrom(orgn => orgn.Details))
                .AfterMap((orgn, dstn) => dstn.Provider = new Provider { Id = orgn.ProviderId, Name = orgn.ProviderName});
                
            Mapper.CreateMap<RefuseRequestMessage, ServiceMessage>()
                  .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                  .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                  .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                  .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                  .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                  .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                  .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                  .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                  .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                  .ForMember(dstn => dstn.Details, opt => opt.MapFrom(orgn => orgn.Details)).ReverseMap();

            Mapper.CreateMap<CancelServiceMessage, ServiceMessage>()
                  .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                  .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                  .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                  .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                  .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                  .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                  .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                  .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                  .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                  .ForMember(dstn => dstn.Details, opt => opt.MapFrom(orgn => orgn.Details))
                  .ReverseMap();

            Mapper.CreateMap<RefuseCancelationMessage, ServiceMessage>()
                  .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                  .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                  .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                  .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                  .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                  .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                  .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                  .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                  .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                  .ReverseMap();

            Mapper.CreateMap<CancelationConfirmationMessage, ServiceMessage>()
                  .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                  .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                  .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                  .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                  .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                  .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                  .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                  .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                  .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                  .ReverseMap();

            Mapper.CreateMap<CancelationConfirmationWithRefundMessage, ServiceMessage>()
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                .ForMember(dstn => dstn.Value, opt => opt.MapFrom(orgn => orgn.RefundValue));

            Mapper.CreateMap<ServiceMessage, CancelationConfirmationWithRefundMessage>()
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                .ForMember(dstn => dstn.RefundValue, opt => opt.MapFrom(orgn => orgn.Value));


            Mapper.CreateMap<CancelationConfirmationWithFeeMessage, ServiceMessage>()
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                .ForMember(dstn => dstn.Value, opt => opt.MapFrom(orgn => orgn.FeeValue));
            
            
            Mapper.CreateMap<ServiceMessage, CancelationConfirmationWithFeeMessage>()
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                .ForMember(dstn => dstn.FeeValue, opt => opt.MapFrom(orgn => orgn.Value));

            Mapper.CreateMap<NotifyUseMessage, ServiceMessage>()
                  .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                  .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                  .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                  .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                  .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                  .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                  .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                  .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                  .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                  .ReverseMap();

            Mapper.CreateMap<NotifyNoShowMessage, ServiceMessage>()
                  .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                  .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                  .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                  .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                  .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                  .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                  .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                  .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                  .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
                  .ReverseMap();
            
            Mapper.CreateMap<InvoiceApprovalRequestMessage, ServiceMessage>()
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                .ForMember(dstn => dstn.Invoice, opt => opt.MapFrom(orgn => Mapper.Map<Invoice, Stubs.Invoice>(orgn.Invoice)));

            Mapper.CreateMap<ServiceMessage, InvoiceApprovalRequestMessage>()
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                .ForMember(dstn => dstn.Invoice, opt => opt.MapFrom(orgn => Mapper.Map<Stubs.Invoice, Invoice>(orgn.Invoice)));
            
            Mapper.CreateMap<RefuseInvoiceMessage, ServiceMessage>()
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                .ForMember(dstn => dstn.Invoice, opt => opt.MapFrom(orgn => Mapper.Map<Invoice, Stubs.Invoice>(orgn.Invoice)));

            Mapper.CreateMap<ServiceMessage, RefuseInvoiceMessage>()
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Acknowledgment, opt => opt.MapFrom(orgn => orgn.Acknowledgment))
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.DrakeId, opt => opt.MapFrom(orgn => orgn.DrakeId))
                .ForMember(dstn => dstn.Invoice, opt => opt.MapFrom(orgn => Mapper.Map<Stubs.Invoice, Invoice>(orgn.Invoice)));

            Mapper.CreateMap<ServiceMessage, AcknowledgmentMessage>()
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.ReceivedMessageId, opt => opt.MapFrom(orgn => orgn.ReceivedMessageId));
                
            #endregion

            #region Mensagens enviadas por todos

            Mapper.CreateMap<GetUnconfirmedMessage, ServiceMessage>()
               .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
               .ForMember(dstn => dstn.ClientGuid, opt => opt.MapFrom(orgn => orgn.ClientGuid))
               .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
               .ForMember(dstn => dstn.Type, opt => opt.MapFrom(orgn => orgn.Type))
               .ForMember(dstn => dstn.MessageIdsToRead, opt => opt.MapFrom(orgn => orgn.MessageIdsToRead))
               .ReverseMap();
            
            
            Mapper.CreateMap<ServiceMessage, ConfirmProcessingMessage>()
                .ForMember(dstn => dstn.Sent, opt => opt.MapFrom(orgn => orgn.Sent))
                .ForMember(dstn => dstn.Id, opt => opt.MapFrom(orgn => orgn.Id))
                .ForMember(dstn => dstn.SenderId, opt => opt.MapFrom(orgn => orgn.SenderId))
                .ForMember(dstn => dstn.RecipientId, opt => opt.MapFrom(orgn => orgn.RecipientId))
                .ForMember(dstn => dstn.SenderUserName, opt => opt.MapFrom(orgn => orgn.SenderUserName))
                .ForMember(dstn => dstn.ReceivedMessageId, opt => opt.MapFrom(orgn => orgn.ReceivedMessageId))
                .ReverseMap();
            
            #endregion

            #region Entidades base

            Mapper.CreateMap<LogisticNeed, Stubs.LogisticNeed>()
                .ReverseMap();

            Mapper.CreateMap<Invoice, Stubs.Invoice>()
                .ForMember(dstn => dstn.Items, opt => opt.MapFrom(orgn => Mapper.Map<IList<InvoiceItem>, IList<Stubs.InvoiceItem>>(orgn.Items)));

            Mapper.CreateMap<Stubs.Invoice, Invoice>()
                .ForMember(dstn => dstn.Items, opt => opt.MapFrom(orgn => Mapper.Map<IList<Stubs.InvoiceItem>, IList<InvoiceItem>>(orgn.Items)));
            
            Mapper.CreateMap<InvoiceItem, Stubs.InvoiceItem>()
                .ForMember(dstn => dstn.ExtraValues, opt => opt.MapFrom(orgn => Mapper.Map<IList<InvoiceItemExtraValue>, IList<Stubs.InvoiceItemExtraValue>>(orgn.ExtraValues)));

            Mapper.CreateMap<Stubs.InvoiceItem, InvoiceItem>()
                .ForMember(dstn => dstn.ExtraValues, opt => opt.MapFrom(orgn => Mapper.Map<IList<Stubs.InvoiceItemExtraValue>, IList<InvoiceItemExtraValue>>(orgn.ExtraValues)));
                
            Mapper.CreateMap<InvoiceItemExtraValue, Stubs.InvoiceItemExtraValue>()
                .ReverseMap();

            Mapper.CreateMap<Address, Stubs.Address>()
                .ReverseMap();
            
            Mapper.CreateMap<ServiceOrder, Stubs.ServiceOrder>()
                .ReverseMap().AfterMap((dstn, orgn) => dstn.Participant = new Stubs.Participant { DrakeId = orgn.ParticipantDrakeId });
                     
            Mapper.CreateMap<Participant, Stubs.Participant>().ReverseMap();

            Mapper.CreateMap<Stubs.ValidationResult, ValidationResult>()
                  .ForMember(dstn => dstn.MessageSent, opt => opt.MapFrom(orgn => orgn.Message.GetEquivalentClientMessage()))
                  .ForMember(dstn => dstn.Errors, opt => opt.MapFrom(orgn => orgn.Errors));

            Mapper.CreateMap<Detail, KeyValuePair<string, string>>()
                 .ConstructUsing(x => new KeyValuePair<string, string>(x.Key, x.Value));

            Mapper.CreateMap<KeyValuePair<string, string>, Detail>()
                 .ConstructUsing(x => new Detail(x.Key, x.Value));

            #endregion

        }

    }

}