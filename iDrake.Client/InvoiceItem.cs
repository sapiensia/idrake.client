﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using iDrake.Client.Utils;
using Newtonsoft.Json;

namespace iDrake.Client
{

    
    
    public class InvoiceItem
    {

        /// <summary>
        /// Construtor.
        /// </summary>
        public InvoiceItem()
        {
            ExtraValues = new List<InvoiceItemExtraValue>();
            Details = new Dictionary<string, string>();
        }

        /// <summary>
        /// Código da necessidade logística no Drake.
        /// </summary>
        
        public long DrakeId { get; set; }

        /// <summary>
        ///     Total final.
        /// </summary>
        
        public decimal? Total { get; set; }

        /// <summary>
        ///     Repasse.
        /// </summary> 
        
        public decimal? Rebate { get; set; }

        /// <summary>
        ///     Valores adicionais.
        /// </summary>
        
        public IList<InvoiceItemExtraValue> ExtraValues { get; set; }

        /// <summary>
        ///     Status. Preenchido apenas pelo cliente Drake.
        /// </summary>
        
        public InvoiceItemStatus Status { get; set; }

        /// <summary>
        ///     Avaliação do cliente Drake. Preenchido apenas pelo cliente Drake.
        /// </summary>
        
        public virtual string Appraisal { get; set; }

        
        public IDictionary<string, string> Details { get; set; }
       
        #region Wrappers dos detalhes
       
        /// <summary>
        /// Comentários e observações.
        /// </summary>
        [JsonIgnore]
        public string Comments
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.Comments);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.Comments, value);
            }
        }

        #endregion
    }

}
