﻿using System.Collections.Generic;

namespace iDrake.Client
{
    /// <summary>
    ///     Resultado da validação.
    /// </summary>
    public sealed class ValidationResult
    {
        /// <summary>
        ///     Mensagem enviada.
        /// </summary>
        public Message MessageSent { get; set; }

        /// <summary>
        ///     Erros encontrados durante a validação.
        /// </summary>
        public IList<ValidationFailure> Errors { get; set; }
    }
}