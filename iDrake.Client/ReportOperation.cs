﻿using System;
using iDrake.Client.Config;
using iDrake.Client.Reports;

namespace iDrake.Client
{
    public class ReportOperation
    {

        /// <summary>
        /// Obtém o relatório de entradas de faturamento de um período.
        /// </summary>
        /// <param name="endDate">Fim do período.</param>
        /// <returns>Relatório de entradas de faturamento.</returns>
        public virtual BillEntriesReport GetBillEntriesReport(DateTime endDate)
        {
            return GetBillEntriesReport(Context.Current.Configuration, endDate);
        }

        /// <summary>
        /// Obtém o relatório de entradas de faturamento do período atual.
        /// </summary>
        /// <returns>Relatório de entradas de faturamento.</returns>
        public virtual BillEntriesReport GetCurrentBillEntriesReport()
        {
            return GetCurrentBillEntriesReport(Context.Current.Configuration);
        }

        /// <summary>
        /// Obtém o relatório de entradas de faturamento do período atual.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração</param>
        /// <returns>Relatório de entradas de faturamento.</returns>
        public virtual BillEntriesReport GetCurrentBillEntriesReport(IStrategyConfiguration strategyConfiguration)
        {
            var endDate = DateTime.Now.Date;
            return Context.Current.GetBillEntriesReport(strategyConfiguration, endDate);
        }

        /// <summary>
        /// Obtém o relatório de entradas de faturamento de um período atual.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração</param>
        /// <param name="endDate">Fim do período.</param>
        /// <returns>Relatório de entradas de faturamento.</returns>
        public virtual BillEntriesReport GetBillEntriesReport(IStrategyConfiguration strategyConfiguration, DateTime endDate)
        {
            return Context.Current.GetBillEntriesReport(strategyConfiguration, endDate);
        }

    }
}