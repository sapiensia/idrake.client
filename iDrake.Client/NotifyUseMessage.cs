﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem de notificação de utilização do serviço.
    ///     Esta mensagem deve ser enviada pelo fornecedor quando o mesmo quiser informar que um serviço foi utilizado.
    /// </summary>
    
    
    public class NotifyUseMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public NotifyUseMessage()
            : base(MessageType.NotifyUse)
        {
            MainObjectType = typeof(LogisticNeed).Name;
        }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<NotifyUseMessage, ServiceMessage>(this);
        }
    }
}