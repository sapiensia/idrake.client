﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct("iDrake.Client")] 

[assembly: ComVisible(false)]

[assembly: Guid("2bb73633-d201-40ee-a073-fb1dfe8d2b31")]

[assembly: InternalsVisibleTo("iDrake.Client.Test.UnitTest")]