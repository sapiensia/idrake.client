﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem de solicitação de mudança em um serviço.
    ///     Esta mensagem deve ser enviada pelo cliente quando o mesmo quiser informar ao fornecedor a necessidade de mudança em um serviço solicitado anteriormente.
    /// </summary>
    
    
    public class ChangeRequestMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public ChangeRequestMessage()
            : base(MessageType.ChangeRequest)
        {
            MainObjectType = typeof(LogisticNeed).Name;
        }

        /// <summary>
        ///     Ordem que o cliente gostaria que o serviço fosse realizado.   
        /// </summary>
        
        public IList<ServiceOrder> ServiceOrder { get; set; }

        /// <summary>
        ///     Necessidade de logística.
        /// </summary>
        
        public LogisticNeed LogisticNeed { get; set; }

        /// <summary>
        ///     Código do atendimento que cliente gostaria que fornecedor tentasse reusar.
        /// </summary>
        
        public string TreatmentIdToTryReuse { get; set; }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<ChangeRequestMessage, ServiceMessage>(this);
        }
    }
}