﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem de recusa de serviço.
    ///     Esta mensagem deve ser enviada pelo fornecedor quando o mesmo quiser recusar uma solicitação de serviço feita pelo cliente.
    /// </summary>
    
    
    public class RefuseRequestMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public RefuseRequestMessage()
            : base(MessageType.RefuseRequest)
        {
            MainObjectType = typeof(LogisticNeed).Name;
        }

        /// <summary>
        ///     Motivo pelo qual a recusa foi feita.
        /// </summary>
        public string Details { get; set; }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<RefuseRequestMessage, ServiceMessage>(this);
        }
    }
}