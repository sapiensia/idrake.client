﻿using System;
using System.Runtime.Serialization;
using iDrake.Client.Internal;
using iDrake.Client.Utils;

namespace iDrake.Client
{

    /// <summary>
    ///     Endereço.
    /// </summary>
    
    
    public class Address
    {

        /// <summary>
        ///     Nome (necessário para casos em que o endereço é um Hotel, Aeroporto, Rodoviária etc).
        /// </summary>
        
        [FieldLength(MaxLength = 255, Message = "Nome deve ter no máximo 255 caracteres.")]
        public string Name { get; set; }

        /// <summary>
        ///     Rua.
        /// </summary>
        
        [FieldLength(MaxLength = 255, Message = "Rua deve ter no máximo 255 caracteres.")]
        public string Street { get; set; }

        /// <summary>
        ///     Número.
        /// </summary>
        
        [FieldLength(MaxLength = 25, Message = "Número deve ter no máximo 25 caracteres.")]
        public string Number { get; set; }

        /// <summary>
        ///     Complemento.
        /// </summary>
        
        [FieldLength(MaxLength = 50, Message = "Complemento deve ter no máximo 50 caracteres.")]
        public string Complement { get; set; }

        /// <summary>
        ///     Nome do bairro.
        /// </summary>
        
        [FieldLength(MaxLength = 100, Message = "Bairro deve ter no máximo 100 caracteres.")]
        public string District { get; set; }

        /// <summary>
        ///     Nome da cidade.
        /// </summary>
        
        [FieldLength(MaxLength = 100, Message = "Cidade deve ter no máximo 100 caracteres.")]
        public string City { get; set; }

        /// <summary>
        ///     Nome ou sigla do estado.
        /// </summary>
        
        [FieldLength(MaxLength = 2, Message = "Estado deve ter no máximo 2 caracteres.")]
        public string State { get; set; }

        /// <summary>
        ///     Nome do país.
        /// </summary>
        
        [FieldLength(MaxLength = 40, Message = "País deve ter no máximo 40 caracteres.")]
        public string Country { get; set; }

        /// <summary>
        ///     CEP.
        /// </summary>
        
        [FieldLength(MaxLength = 8, Message = "CEP deve ter no máximo 8 caracteres.")]
        public string Zip { get; set; }

        /// <summary>
        ///     Latitude especificada de acordo com o sistema WGS84.
        /// </summary>
        
        public double? Latitude { get; set; }

        /// <summary>
        ///     Longitude especificada de acordo com o sistema WGS84.
        /// </summary>
        
        public double? Longitude { get; set; }

        /// <summary>
        ///     Codigo IATA (apenas para aeroportos).
        /// </summary>
        
        [FieldLength(MaxLength = 4, Message = "Código IATA deve ter no máximo 4 caracteres.")]
        public string IATA { get; set; }

        public override string ToString()
        {
            return ToStringBuilder.ReflectionToString(this);
        }

    }

}