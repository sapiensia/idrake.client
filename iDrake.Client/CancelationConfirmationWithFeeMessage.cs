﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem de confirmação de cancelamento com taxa.
    ///     Esta mensagem deve ser enviada pelo fornecedor quando o mesmo quiser confirmar o cancelamento de um serviço porém cobrando uma taxa.
    /// </summary>
    
    
    public class CancelationConfirmationWithFeeMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public CancelationConfirmationWithFeeMessage()
            : base(MessageType.CancelationConfirmationWithFee)
        {
            MainObjectType = typeof(LogisticNeed).Name;
        }

        /// <summary>
        ///     Valor da taxa de cancelamento.
        /// </summary>
        
        public decimal FeeValue { get; set; }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<CancelationConfirmationWithFeeMessage, ServiceMessage>(this);
        }
    }
}