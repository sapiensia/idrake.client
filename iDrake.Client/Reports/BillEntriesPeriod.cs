﻿using System;
using System.Runtime.Serialization;

namespace iDrake.Client.Reports
{
    
    public class BillEntriesPeriod
    {
        
        public DateTime Start { get; set; }

        
        public DateTime End { get; set; }
    }
}