﻿using System;
using System.Runtime.Serialization;

namespace iDrake.Client.Reports
{
    
    public class BillEntriesReportItem
    {
        
        public DateTime Date { get; set; }

        
        public int TotalByDay { get; set; }

        
        public int TotalSentByDay { get; set; }

        
        public int TotalReceivedByDay { get; set; }

        
        public decimal TotalCostByDay { get; set; }

        
        public decimal TotalCostReceivedMessagesByDay { get; set; }

        
        public decimal TotalCostSentMessagesByDay { get; set; }
    }
}