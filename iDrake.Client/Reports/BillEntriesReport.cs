﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace iDrake.Client.Reports
{
    
    public class BillEntriesReport
    {
        
        public decimal TotalCost { get; set; }

        
        public decimal TotalCostWithDiscount { get; set; }

        
        public decimal Discount { get; set; }

        
        public IList<BillEntriesReportItem> Items { get; set; }

        
        public DateTime Start { get; set; }

        
        public DateTime End { get; set; }

        
        public IList<BillEntriesPeriod> Periods { get; set; }

    }
}