﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using iDrake.Client.Utils;
using Newtonsoft.Json;

namespace iDrake.Client
{
    
     
    public class Invoice
    {

        public Invoice()
        {
            Items = new List<InvoiceItem>();
            Details = new Dictionary<string, string>();
        }

        
        public IList<InvoiceItem> Items { get; set; }

        
        public virtual string ExternalId { get; set; }

        
        public virtual long? DrakeId { get; set; }

        
        public IDictionary<string, string> Details
        {
            get;
            set;
        }

        /// <summary>
        ///     Código do centro de custo.
        /// </summary>
        [JsonIgnore]
        public string CostCenterCode
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.CostCenterCode);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.CostCenterCode, value);
            }
        }

        /// <summary>
        ///     Nome do centro de custo.
        /// </summary>
        [JsonIgnore]
        public string CostCenterName
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.CostCenterName);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.CostCenterName, value);
            }
        }
        
    }

}
