﻿using System;
using System.Collections.Generic;
using System.Linq;
using iDrake.Client.Config;

namespace iDrake.Client
{
    /// <summary>
    ///     Operação de leitura das mensagens enviadas para os clientes e fornecedores.
    /// </summary>
    public class ReadOperation : IReadOperation
    {

        /// <summary>
        ///     Códigos das mensagens lidas nesta operação.
        /// </summary>
        private IDictionary<long, string> _readedMessages = new Dictionary<long, string>();

        /// <summary>
        ///     Ler mensagens não confirmadas.
        /// </summary>
        /// <returns>Mensagens não confirmadas.</returns>
        public virtual IList<Message> ReadMessages()
        {
            return ReadMessages(Context.Current.Configuration);
        }

        /// <summary>
        ///     Ler mensagens não confirmadas com os códigos informados.
        /// </summary>
        /// <param name="idsMessageToRead">Códigos das mensagens não confirmadas que devem ser lidas novamente.</param>
        /// <returns>Mensagens não confirmadas</returns>
        public virtual IList<Message> ReadMessages(IEnumerable<long> idsMessageToRead)
        {
            return ReadMessages(Context.Current.Configuration, idsMessageToRead);
        }

        /// <summary>
        ///     Ler mensagens não confirmadas usando a estratégia de configuração informada.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração que será usada para ler as mensagens não confirmadas.</param>
        /// <returns>Mensagens não confirmadas.</returns>
        public virtual IList<Message> ReadMessages(IStrategyConfiguration strategyConfiguration)
        {
            var messages = new List<Message>();

            messages.Add(new GetUnconfirmedMessage { RecipientId = strategyConfiguration.GetSenderId(), SenderId = null });

            IList<Message> returnedMessages = Context.Current.ReadMessages(strategyConfiguration, messages);

            if (returnedMessages != null && returnedMessages.Count > 0)
            {
                // Lê as mensagens e guarda temporariamente.
                _readedMessages = returnedMessages.ToDictionary(x => x.Id, x => (string)null);
            }

            return returnedMessages;
        }

        /// <summary>
        /// Define um ID externo que foi gerado para a mensagem recebida.
        /// </summary>
        /// <param name="messageId">ID da mensagem</param>
        /// <param name="externalId">ID externo gerado</param>
        /// <exception cref="InvalidOperationException">Se não existir mensagem com o ID informado.</exception>
        public virtual void SetExternalId(long messageId, string externalId)
        {
            if (!_readedMessages.ContainsKey(messageId))
            {
                throw new InvalidOperationException("Não existe uma mensagem de ID " + messageId + " na lista de mensagens lidas.");
            }

            _readedMessages[messageId] = externalId;
        }

        /// <summary>
        ///     Ler mensagens não confirmadas usando a estratégia de configuração informada.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração que será usada para ler as mensagens não confirmadas.</param>
        /// <param name="messageIdsToRead">Códigos das mensagens que devem ser lidas.</param>
        /// <returns>Mensagens não confirmadas.</returns>
        public virtual IList<Message> ReadMessages(IStrategyConfiguration strategyConfiguration, IEnumerable<long> messageIdsToRead)
        {
            var messages = new List<Message>();

            messages.Add(new GetUnconfirmedMessage { RecipientId = strategyConfiguration.GetSenderId(), SenderId = null, MessageIdsToRead = messageIdsToRead });

            IList<Message> returnedMessages = Context.Current.ReadMessages(strategyConfiguration, messages);

            if (returnedMessages != null && returnedMessages.Count > 0)
            {
                // Lê as mensagens e guarda temporariamente.
                _readedMessages = returnedMessages.ToDictionary(x => x.Id, x => (string)null);
            }

            return returnedMessages;
        }

        /// <summary>
        ///     Confirma a leitura das mensagens.
        /// </summary>
        public virtual void ConfirmReadedMessages()
        {
            ConfirmReadedMessages(Context.Current.Configuration);
        }

        /// <summary>
        ///     Confirma a leitura das mensagens.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração que será usada para ler as mensagens não confirmadas.</param>
        public virtual void ConfirmReadedMessages(IStrategyConfiguration strategyConfiguration)
        {
           ConfirmReadedMessages(strategyConfiguration, _readedMessages);
        }

        /// <summary>
        ///     Confirma a leitura das mensagens.
        /// </summary>
        /// <param name="strategyConfiguration">Configuração.</param>
        /// <param name="readedMessageIds">Código das mensagens lidas.</param>
        public virtual void ConfirmReadedMessages(IStrategyConfiguration strategyConfiguration, IList<long> readedMessageIds)
        {
            Context.Current.ConfirmReadedMessages(strategyConfiguration, readedMessageIds.ToDictionary(x => x, x => (string)null));
        }

        /// <summary>
        ///     Confirma a leitura das mensagens.
        /// </summary>
        /// <param name="readedMessageIds">Código das mensagens lidas.</param>
        public virtual void ConfirmReadedMessages(IList<long> readedMessageIds)
        {
            Context.Current.ConfirmReadedMessages(Context.Current.Configuration, readedMessageIds.ToDictionary(x => x, x => (string)null));
        }
        
        /// <summary>
        ///     Confirma a leitura das mensagens.
        /// </summary>
        /// <param name="strategyConfiguration">Configuração.</param>
        /// <param name="readMessages">Código das mensagens lidas.</param>
        public virtual void ConfirmReadedMessages(IStrategyConfiguration strategyConfiguration, IDictionary<long, string> readMessages)
        {
            Context.Current.ConfirmReadedMessages(strategyConfiguration, readMessages);
        }

        /// <summary>
        ///     Confirma a leitura das mensagens.
        /// </summary>
        /// <param name="readMessages">Código das mensagens lidas.</param>
        public virtual void ConfirmReadedMessages(IDictionary<long, string> readMessages)
        {
            Context.Current.ConfirmReadedMessages(Context.Current.Configuration, readMessages);
        }

    }
}