﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;

namespace iDrake.Client.Utils
{
    public class HttpUtil
    {
        public const string ApplicationJsonContentType = "application/json";

        public static string EncodeToUrl(string value)
        {
            return HttpUtility.UrlEncode(value);
        }

        public static string DecodeFromUrl(string value)
        {
            return HttpUtility.UrlDecode(value);
        }

        private static void AcceptInvalidSsl()
        {
            ServicePointManager.ServerCertificateValidationCallback += delegate
            {
                return true; // **** Always accept
            };
        }

        public static HttpResult Post(string url, IDictionary<string, string> header, string contentType, string postData, bool acceptInvalidSsl = false, string userAgent = null, bool expected100Continue = false)
        {
            return Post(url, header, contentType, postData, null, acceptInvalidSsl, userAgent);
        }

        public static HttpResult Post(string url, IDictionary<string, string> header, string contentType, string postData, string privateKey, bool acceptInvalidSsl = false, string userAgent = null, bool expected100Continue = false)
        {
            if (acceptInvalidSsl)
            {
                AcceptInvalidSsl();
            }

            var request = WebRequest.Create(url) as HttpWebRequest;

            if (header != null)
            {
                foreach (var pair in header)
                {
                    request.Headers.Add(pair.Key, pair.Value);
                }
            }

            // Set the Method property of the request to POST.
            request.Method = "POST";
            request.Accept = "*/*";
            request.UserAgent = userAgent ?? string.Format("{0} {1}", Assembly.GetCallingAssembly().GetName().Name, Assembly.GetCallingAssembly().GetName().Version);

            // Para resolver problemas com proxy SQUID.
            request.ServicePoint.Expect100Continue = expected100Continue;

            //Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4

            request.Date = DateTime.Now;

            // Create POST data and convert it to a byte array.
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.
            request.ContentType = contentType;

            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;

            if (string.IsNullOrEmpty(privateKey) == false)
            {
                SecurityHelper.SignHttpWebRequest(request, postData, privateKey);
            }

            // Get the request stream.
            Stream dataStream = request.GetRequestStream();

            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);

            // Close the Stream object.
            dataStream.Close();

            var httpResult = new HttpResult();

            try
            {
                // Get the response.
                WebResponse response = request.GetResponse();

                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();

                // Open the stream using a StreamReader for easy access.
                var reader = new StreamReader(dataStream);

                // Read the content.
                string responseFromServer = reader.ReadToEnd();

                // Clean up the streams.
                reader.Close();
                dataStream.Close();
                response.Close();

                httpResult.Status = HttpStatusCode.OK;
                httpResult.Content = responseFromServer;
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    HttpStatusCode statusCode = ((HttpWebResponse)we.Response).StatusCode;
                    httpResult.Status = statusCode;

                    using (Stream stream = we.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        httpResult.Content = reader.ReadToEnd();
                    }
                }
                else
                {
                    httpResult.Content = we.Message;
                }
            }

            return httpResult;
        }
    }

    public class HttpResult
    {
        public HttpStatusCode Status { get; set; }
        public string Content { get; set; }
    }
}
