﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace iDrake.Client.Utils
{
    public class JsonHelper
    {
        public static string JsonSerializer(object instance, Type type)
        {
            return JsonConvert.SerializeObject(instance, type, Formatting.Indented,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
        }

        public static string JsonSerializer<T>(T t)
        {
            return JsonConvert.SerializeObject(t, Formatting.Indented,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
        }

        /// <summary>
        ///     JSON Deserialization
        /// </summary>
        public static T JsonDeserialize<T>(string jsonString)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString);
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public static object JsonDeserialize(string jsonString, Type type)
        {
            try
            {
                return JsonConvert.DeserializeObject(jsonString, type);
            }
            catch (Exception)
            {
                if (type.IsValueType)
                {
                    return Activator.CreateInstance(type);
                }

                return null;
            }
        }

        public static string JsonSerializer<T>(T t, bool jsonIndented, IContractResolver contractResolver)
        {
            var formatting = Formatting.None;

            if (jsonIndented)
            {
                formatting = Formatting.Indented;
            }

            return JsonConvert.SerializeObject(t, formatting,
                new JsonSerializerSettings
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                    NullValueHandling = NullValueHandling.Ignore,
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = contractResolver
                });
        }

        public static string SerializeJsonContentList(IList<string> jsonContentList)
        {
            var output = new StringBuilder();

            output.Append('[');

            for (int i = 0; i < jsonContentList.Count; i++)
            {
                output.Append(jsonContentList[i]);

                if (i < jsonContentList.Count - 1)
                {
                    output.Append(',');
                }
            }

            output.Append(']');

            return output.ToString();
        }
    }
}