﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace iDrake.Client.Utils
{
    // http://social.msdn.microsoft.com/Forums/en-US/csharplanguage/thread/67b50472-b23f-4191-b8f4-e4a696fca5e4/
    public class SecurityHelper
    {
        public const string DrakeSignatureHeaderName = "x-Drake-Signature";
        public const string DrakeSenderIdHeaderName = "x-Drake-SenderId";

        public static readonly string[] HeadersToSign = { DrakeSenderIdHeaderName, "User-Agent", "Date", "Content-Type" };

        public static string GetDataToSign(IDictionary<string, string> headers, string method, string content)
        {
            var dataToSign = new StringBuilder();

            dataToSign.AppendFormat("{0}\n", method);

            foreach (string headerKey in HeadersToSign.OrderBy(h => h).ToList())
            {
                dataToSign.AppendFormat("{0}:{1}\n", headerKey.ToLower(), headers[headerKey]);
            }

            dataToSign.Append("\n");

            dataToSign.Append(content);

            return dataToSign.ToString();
        }

        public static void SignHttpWebRequest(WebRequest request, string content, string privateKey)
        {
            IDictionary<string, string> headers = ConvertWebHeaderCollectionToDictionary(request.Headers);

            var signature = SecurityHelper.SignData(GetDataToSign(headers, request.Method, content), privateKey);

            request.Headers.Add(DrakeSignatureHeaderName, signature);
        }

        public static void SignHttpRequest(HttpRequest request, string content, string privateKey)
        {
            IDictionary<string, string> headers = ConvertNameValueCollectionToDictionary(request.Headers);

            var signature = SecurityHelper.SignData(GetDataToSign(headers, request.HttpMethod, content), privateKey);

            request.Headers.Add(DrakeSignatureHeaderName, signature);
        }

        private static IDictionary<string, string> ConvertWebHeaderCollectionToDictionary(
            WebHeaderCollection webHeaderCollection)
        {
            var headerDictionary = new Dictionary<string, string>();

            foreach (string webHeaderKey in webHeaderCollection.AllKeys)
            {
                headerDictionary.Add(webHeaderKey, webHeaderCollection[webHeaderKey]);
            }

            return headerDictionary;
        }

        public static IDictionary<string, string> ConvertNameValueCollectionToDictionary(
            NameValueCollection nameValueCollection)
        {
            var headerDictionary = new Dictionary<string, string>();

            foreach (string nameValueKey in nameValueCollection.AllKeys)
            {
                headerDictionary.Add(nameValueKey, nameValueCollection[nameValueKey]);
            }

            return headerDictionary;
        }

        public static string GenerateSaltedHash(string input, string salt)
        {
            return GenerateSHA1Hash(input + salt);
        }

        public static string GenerateSHA1Hash(string input)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            byte[] data = Encoding.ASCII.GetBytes(input);
            byte[] hash = sha.ComputeHash(data);

            var sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        public static string RSAEncrypt(string publicKey, string plainData)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(publicKey);
                byte[] plainbytes = Encoding.UTF8.GetBytes(plainData);
                byte[] cipherbytes = rsa.Encrypt(plainbytes, false);

                return Convert.ToBase64String(cipherbytes);
            }
        }

        public static string RSADecrypt(string privateKey, string dataEncrypted)
        {

            using (var rsa = new RSACryptoServiceProvider())
            {
                byte[] cipherData = Convert.FromBase64String(dataEncrypted);

                rsa.FromXmlString(privateKey);

                byte[] plain = rsa.Decrypt(cipherData, false);
                return Encoding.UTF8.GetString(plain);
            }

        }



        public static string SignData(string message, string privateKey)
        {
            //// The array to store the signed message in bytes
            byte[] signedBytes;
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportCspBlob(Convert.FromBase64String(privateKey));
                //// Write the message to a byte array using UTF8 as the encoding.
                var encoder = new UTF8Encoding();
                byte[] originalData = encoder.GetBytes(message);

                try
                {
                    //// Sign the data, using SHA512 as the hashing algorithm 
                    signedBytes = rsa.SignData(originalData, CryptoConfig.MapNameToOID("SHA512"));
                }
                catch (CryptographicException e)
                {
                    Console.WriteLine(e.Message);
                    return null;
                }
                finally
                {
                    //// Set the keycontainer to be cleared when rsa is garbage collected.
                    rsa.PersistKeyInCsp = false;
                }
            }
            //// Convert the a base64 string before returning
            return Convert.ToBase64String(signedBytes);
        }

        public static bool VerifyHttpRequestSignature(HttpRequest request, string content, string signature,
                                                      string publicKey)
        {
            IDictionary<string, string> headers = ConvertNameValueCollectionToDictionary(request.Headers);

            string dataToSign = GetDataToSign(headers, request.HttpMethod, content);

            return VerifyData(dataToSign, signature, publicKey);
        }



        public static bool VerifyData(string message, string signature, string publicKey)
        {
            bool success = false;
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportCspBlob(Convert.FromBase64String(publicKey));
                var encoder = new UTF8Encoding();
                byte[] bytesToVerify = encoder.GetBytes(message);
                byte[] signedBytes = Convert.FromBase64String(signature);
                try
                {
                    success = rsa.VerifyData(bytesToVerify, CryptoConfig.MapNameToOID("SHA512"), signedBytes);
                }
                catch (CryptographicException e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
            return success;
        }

        public static KeyPairs CreateKeyPairs()
        {
            var rsa = new RSACryptoServiceProvider();

            var privateKey = Convert.ToBase64String(rsa.ExportCspBlob(true));
            var publicKey = Convert.ToBase64String(rsa.ExportCspBlob(false));

            return new KeyPairs { PrivateKey = privateKey, PublicKey = publicKey };

        }


    }

    public class KeyPairs
    {
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }
    }
}
