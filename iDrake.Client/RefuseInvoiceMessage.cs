﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    
    
    public class RefuseInvoiceMessage : Message
    {

        public RefuseInvoiceMessage()
            : base(MessageType.RefuseInvoice)
        {
            Invoice = new Invoice(); 
            MainObjectType = typeof(Invoice).Name;
        }

        public void AddItems(IList<InvoiceItem> items)
        {
            foreach (var item in items)
            {
                AddItem(item);
            }
        }
                
        public void AddItem(InvoiceItem item)
        {
            Invoice.Items.Add(item);
        }

        public IList<InvoiceItem> Items
        {
            get
            {
                return Invoice.Items;
            }
        }
            
        
        internal Invoice Invoice { get; set; }  

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<RefuseInvoiceMessage, ServiceMessage>(this);
        }

    }

}
