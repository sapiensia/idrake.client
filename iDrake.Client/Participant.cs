﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using iDrake.Client.Stubs.Enums;
using iDrake.Client.Utils;
using Newtonsoft.Json;

namespace iDrake.Client
{

    /// <summary>
    ///     Participante.
    /// </summary>
    
    
    public class Participant
    {

        /// <summary>
        /// Construtor.
        /// </summary>
        public Participant()
        {
            Details = new Dictionary<string, string>();    
        }

        /// <summary>
        /// Código DRAKE do participante.
        /// </summary>
        
        public long DrakeId { get; set; }

        /// <summary>
        ///     Nome completo da pessoa.
        /// </summary>
        
        public string Name { get; set; }

        /// <summary>
        ///     Sexo.
        /// </summary>
        
        public Gender Gender { get; set; }

        /// <summary>
        ///     Número da identidade.
        /// </summary>
        [JsonIgnore]
        public string Identity
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.Identity); 
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.Identity, value);    
            }
        }

        /// <summary>
        ///     Número do Cpf.
        /// </summary>
        [JsonIgnore]
        public string Cpf 
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.Cpf);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.Cpf, value);    
            }
        }

        /// <summary>
        ///     Número do passaporte atual.
        /// </summary>
        [JsonIgnore]
        public string Passports
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.Passports);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.Passports, value);
            }
        }

        /// <summary>
        ///     Contatos telefônicos.
        /// </summary>
        [JsonIgnore]
        public string PhoneNumbers
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.PhoneNumbers);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.PhoneNumbers, value);
            }
        }

        /// <summary>
        ///     Código do centro de custo.
        /// </summary>
        [JsonIgnore]
        public string CostCenterCode
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.CostCenterCode);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.CostCenterCode, value);
            }
        }

        /// <summary>
        ///     Nome do centro de custo.
        /// </summary>
        public string CostCenterName
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.CostCenterName);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.CostCenterName, value);
            }
        }

        /// <summary>
        ///     Endereços de e-mail.
        /// </summary>
        [JsonIgnore]
        public string Emails
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.Emails);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.Emails, value);
            }
        }        
        /// <summary>
        ///     Empresa do Trabalhador.
        /// </summary>
        [JsonIgnore]
        public string Company
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.Company);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.Company, value);
            }
        }        
        
        /// <summary>
        ///     Função do trabalhador.
        /// </summary>
        [JsonIgnore]
        public string Job
        {
            get
            {
                return DictionaryHelper.Get(Details, Client.Details.Job);
            }

            set
            {
                DictionaryHelper.Add(Details, Client.Details.Job, value);
            }
        }

        
        public IDictionary<string, string> Details
        {
            get;
            set;
        }

        public override string ToString()
        {
            return ToStringBuilder.ReflectionToString(this);
        }

    }

}