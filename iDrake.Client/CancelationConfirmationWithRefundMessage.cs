﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem de confirmação de cancelamento com reembolso.
    ///     Esta mensagem deve ser enviada pelo fornecedor quando o mesmo quiser confirmar o cancelamento de um serviço com reembolso para o cliente.
    /// </summary>
    
    
    public class CancelationConfirmationWithRefundMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public CancelationConfirmationWithRefundMessage()
            : base(MessageType.CancelationConfirmationWithRefund)
        {
            MainObjectType = typeof(LogisticNeed).Name;
        }

        /// <summary>
        ///     Valor do reembolso.
        /// </summary>
        
        public decimal RefundValue { get; set; }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<CancelationConfirmationWithRefundMessage, ServiceMessage>(this);
        }
    }
}