﻿using System;
using System.Runtime.Serialization;
using AutoMapper;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem de recusa de cancelamento.
    ///     Esta mensagem deve ser enviada pelo fornecedor quando o mesmo quiser recusar uma solicitação de cancelamento de serviço feita pelo cliente.
    /// </summary>
    
    
    public class RefuseCancelationMessage : Message
    {
        /// <summary>
        ///     Construtor.
        /// </summary>
        public RefuseCancelationMessage()
            : base(MessageType.RefuseCancelation)
        {
            MainObjectType = typeof(LogisticNeed).Name;
        }

        internal override ServiceMessage GetEquivalentServiceMessage()
        {
            return Mapper.Map<RefuseCancelationMessage, ServiceMessage>(this);
        }
    }
}