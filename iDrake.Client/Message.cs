﻿using System;
using System.Runtime.Serialization;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;
using iDrake.Client.Utils;

namespace iDrake.Client
{
    /// <summary>
    ///     Mensagem.
    /// </summary>
    public class Message
    {
        /// <summary>
        ///     Código do remetente da mensagem.
        /// </summary>
        private int? _senderId;

        /// <summary>
        ///     Construtor.
        /// </summary>
        /// <param name="type">Tipo da mensagem.</param>
        protected Message(MessageType type)
        {
            ClientGuid = Guid.NewGuid();
            Type = type;
        }

        /// <summary>
        ///     Código da mensagem.
        /// </summary>
        
        public long Id { get; set; }

        
        internal Guid? ClientGuid { get; set; }

        // Data em que a mensagem foi enviada pelo remetente.
        
        public DateTime? Sent { get; internal set; }

        /// <summary>
        ///     Flag que indica se o recebimento da mensagem deve ser notificado ao remetente ou não.
        /// </summary>
        
        public bool? Acknowledgment { get; set; }

        /// <summary>
        ///     Tipo da mensagem.
        /// </summary>
        
        public MessageType Type { get; private set; }

        /// <summary>
        ///     Tipo do objeto manipulado na mensagem.
        /// </summary>
        
        public string MainObjectType { get; protected set; }

        /// <summary>
        ///     Código do DRAKE.
        /// </summary>
        
        public long? DrakeId { get; set; }

        /// <summary>
        ///     Código do sistema externo ao DRAKE.
        /// </summary>
        
        public string ExternalId { get; set; }

        /// <summary>
        ///     Nome do usuário logado no sistema que enviou a mensagem.
        /// </summary>
        
        public string SenderUserName { get; set; }

        /// <summary>
        ///     Código do remetente.
        /// </summary>
        
        public int? SenderId
        {
            get { return _senderId; }
            set { _senderId = value; }
        }

        public Role SenderRole
        {
            get
            {
                switch (Type)
                {
                    case MessageType.CancelationRequest:
                    case MessageType.ServiceRequest:
                    case MessageType.ChangeRequest:
                    case MessageType.RefuseInvoice:
                    case MessageType.ApproveInvoice:
                        return Role.Customer;

                    default:
                        return Role.Provider;
                }
            }
        }

        /// <summary>
        ///     Código do destinatário.
        /// </summary>
        
        public int? RecipientId { get; set; }

        internal virtual ServiceMessage GetEquivalentServiceMessage()
        {
            return new ServiceMessage
            {
                SenderId = SenderId,
                RecipientId = RecipientId,
                DrakeId = DrakeId,
                Id = Id,
                ClientGuid = ClientGuid,
                Sent = Sent
            };
        }

        public virtual void Validate()
        {
        }

        public override string ToString()
        {
            return ToStringBuilder.ReflectionToString(this);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var message = obj as Message;

            return message != null && ClientGuid == message.ClientGuid;
        }

        public override int GetHashCode()
        {
            return ClientGuid.GetHashCode();
        }
    }
}