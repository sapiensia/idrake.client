﻿using System.Collections.Generic;
using iDrake.Client.Config;

namespace iDrake.Client
{
    public interface ICustomerOperation
    {
        /// <summary>
        ///     Solicitar serviço.
        /// </summary>
        /// <param name="message">Mensagem de solicitação de serviço.</param>
        /// <returns>Operação do cliente.</returns>
        CustomerOperation ServiceRequest(ServiceRequestMessage message);

        /// <summary>
        ///     Solicitar serviços.
        /// </summary>
        /// <param name="messages">Mensagens de solicitação de serviço.</param>
        /// <returns>Operação do cliente.</returns>
        CustomerOperation ServiceRequests(IEnumerable<ServiceRequestMessage> messages);

        /// <summary>
        ///     Solicitar cancelamento.
        /// </summary>
        /// <param name="message">Mensagem de solicitação de cancelamento.</param>
        /// <returns>Operação do cliente.</returns>
        CustomerOperation CancelationRequest(CancelationRequestMessage message);

        /// <summary>
        ///     Solicitar cancelamentos.
        /// </summary>
        /// <param name="messages">Mensagens de solicitação de cancelamento.</param>
        /// <returns>Operação do cliente.</returns>
        CustomerOperation CancelationRequests(IEnumerable<CancelationRequestMessage> messages);

        /// <summary>
        ///     Solicitar mudança.
        /// </summary>
        /// <param name="message">Mensagem de solicitação de mudança.</param>
        /// <returns>Operação do cliente.</returns>
        CustomerOperation ChangeRequest(ChangeRequestMessage message);

        /// <summary>
        ///     Solicitar mudanças.
        /// </summary>
        /// <param name="messages">Mensagens de solicitação de mudança.</param>
        /// <returns>Operação do cliente.</returns>
        CustomerOperation ChangeRequests(IEnumerable<ChangeRequestMessage> messages);

        /// <summary>
        ///     Aprovar fatura.
        /// </summary>
        /// <param name="message">Mensagem de aprovação de fatura.</param>
        /// <returns>Operação do cliente.</returns>
        CustomerOperation ApproveInvoice(ApproveInvoiceMessage message);

        /// <summary>
        ///     Aprovar faturas.
        /// </summary>
        /// <param name="messages">Mensagens de aprovação de faturas.</param>
        /// <returns>Operação do cliente.</returns>
        CustomerOperation ApproveInvoices(IEnumerable<ApproveInvoiceMessage> messages);

        /// <summary>
        ///     Recusar fatura.
        /// </summary>
        /// <param name="message">Mensagem de recusa de fatura.</param>
        /// <returns>Operação do cliente.</returns>
        CustomerOperation RefuseInvoice(RefuseInvoiceMessage message);

        /// <summary>
        /// Recusar faturas.
        /// </summary>
        /// <param name="messages">Mensagens de recusa de fatura.</param>
        /// <returns>Operação do cliente.</returns>
        CustomerOperation RefuseInvoices(IEnumerable<RefuseInvoiceMessage> messages);

        /// <summary>
        /// Confirmar que uma mensagem foi processada.
        /// </summary>
        /// <param name="message">Mensagem de confirmação de processamento.</param>
        /// <returns>Operação do cliente.</returns>
        CustomerOperation ConfirmProcessing(ConfirmProcessingMessage message);
        
        /// <summary>
        ///     Executa a operação.
        /// </summary>
        /// <param name="strategyConfiguration">Estratégia de configuração que será usada para executar a operação.</param>
        /// <returns>Mensagens enviadas com os códigos carregados.</returns>
        void Execute(IStrategyConfiguration strategyConfiguration);

        /// <summary>
        ///     Executa a operação.
        /// </summary>
        /// <returns>Mensagens enviadas com os códigos carregados.</returns>
        void Execute();
    }
}