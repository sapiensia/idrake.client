﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using iDrake.Client;
using iDrake.Client.Config;

namespace Gerador_mensagens_de_teste_iDrake
{
    public partial class FormOperacoesFornecedor : Form
    {
        public FormOperacoesFornecedor()
        {
            InitializeComponent();
        }

        private void btnConfirmarSolicitacao_Click(object sender, EventArgs e)
        {
            try
            {
                var operacao = new ProviderOperation(GetCodigoCliente());

                var mensagem = new ConfirmServiceMessage();

                mensagem.TreatmentId = txtCodigoAtendimento.Text;

                mensagem.Origin = new Address
                {
                    City = txtCidadeOrigem.Text,
                    Complement = txtComplementoOrigem.Text,
                    Country = txtPaisOrigem.Text,
                    District = txtBairroOrigem.Text,
                    Street = txtRuaOrigem.Text,
                    State = txtEstadoOrigem.Text,
                    Zip = txtCEPOrigem.Text,
                    Number = txtNumeroOrigem.Text,
                    IATA = txtIATAOrigem.Text
                };

                if (string.IsNullOrEmpty(txtLatitudeOrigem.Text) == false)
                {
                    mensagem.Origin.Latitude = double.Parse(txtLatitudeOrigem.Text);
                }

                if (string.IsNullOrEmpty(txtLongitudeOrigem.Text) == false)
                {
                    mensagem.Origin.Longitude = double.Parse(txtLongitudeOrigem.Text);
                }

                mensagem.Destination = new Address
                {
                    City = txtCidadeDestino.Text,
                    Complement = txtComplementoDestino.Text,
                    Country = txtPaisDestino.Text,
                    District = txtBairroDestino.Text,
                    Street = txtRuaDestino.Text,
                    State = txtEstadoDestino.Text,
                    Zip = txtCEPDestino.Text,
                    Number = txtNumeroDestino.Text,
                    IATA = txtIATADestino.Text
                };

                if (string.IsNullOrEmpty(txtLatitudeDestino.Text) == false)
                {
                    mensagem.Destination.Latitude = double.Parse(txtLatitudeDestino.Text);
                }

                if (string.IsNullOrEmpty(txtLongitudeDestino.Text) == false)
                {
                    mensagem.Destination.Longitude = double.Parse(txtLongitudeDestino.Text);
                }

                mensagem.Start = dtpInicio.Value;
                mensagem.End = dtpTermino.Value;              

                mensagem.Value = decimal.Parse(txtPreco.Text);
                mensagem.Fees = decimal.Parse(txtTaxas.Text);
                mensagem.Commission = decimal.Parse(txtComissao.Text);
                mensagem.ReservationNumber = txtReserva.Text;

                var provedor = new Provider
                {
                    Name = txtNomeProvedor.Text,
                    Id = txtCodigoProvedor.Text
                };

                mensagem.Provider = provedor;

                if (cboxReusarAtendimento.Checked)
                {
                    mensagem.ReusedTreatmentId = txtCodigoAtendimentoReusado.Text;
                    mensagem.RebatesByReuse = decimal.Parse(txtAbatimentos.Text);
                }

                var serviceOrders = new List<ServiceOrder>();

                serviceOrders.Add(new ServiceOrder()
                {
                    ParticipantDrakeId = int.Parse(txtDrakeIDParticipante1.Text),
                    PickupOrder = int.Parse(txtOrdemSaidaParticipante1.Text),
                    DeliveryOrder = int.Parse(txtOrdemChegadaParticipante1.Text)
                });

                if (string.IsNullOrEmpty(txtDrakeIDParticipante2.Text) == false && 
                    string.IsNullOrEmpty(txtOrdemSaidaParticipante2.Text) == false && 
                    string.IsNullOrEmpty(txtOrdemChegadaParticipante2.Text) == false)
                {
                    serviceOrders.Add(new ServiceOrder()
                    {
                        ParticipantDrakeId = int.Parse(txtDrakeIDParticipante2.Text),
                        PickupOrder = int.Parse(txtOrdemSaidaParticipante2.Text),
                        DeliveryOrder = int.Parse(txtOrdemChegadaParticipante2.Text)
                    });
                }

                mensagem.ServiceOrder = serviceOrders;

                mensagem.DrakeId = GetDrakeId();

                mensagem.ExternalId = GetExternoId();

                mensagem.SumWithValuesFromPreviousTreatment = cboxSomarValores.Checked;

                operacao.ConfirmService(mensagem);

                operacao.Execute(GetConfig());
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        private IStrategyConfiguration GetConfig()
        {
            return new ConfigStrategy(GetCodigoFornecedor(), txtChavePrivada.Text);
        }

        private int GetCodigoCliente()
        {
            return int.Parse(txtCodigoCliente.Text);
        }

        private int GetCodigoFornecedor()
        {
            return int.Parse(txtCodigoFornecedor.Text);
        }

        private int GetDrakeId()
        {
            return int.Parse(txtDrakeId.Text);
        }

        private string GetExternoId()
        {
            return txtExternoId.Text;
        }

        private void btnCancelarServico_Click(object sender, EventArgs e)
        {
            try
            {
                var operacao = new ProviderOperation(GetCodigoCliente());

                var mensagem = new CancelServiceMessage
                {
                    Details = txtDetalhes.Text,
                    DrakeId = GetDrakeId(),
                    ExternalId = GetExternoId()
                };

                operacao.CancelService(mensagem);

                operacao.Execute(GetConfig());
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }      
        }

        private void btnInformarUsoServico_Click(object sender, EventArgs e)
        {
            try
            {
                var operacao = new ProviderOperation(GetCodigoCliente());

                var mensagem = new NotifyUseMessage
                {
                    ExternalId = GetExternoId()
                };

                operacao.NotifyUse(mensagem);

                operacao.Execute(GetConfig());
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        private void btnInformarNoShow_Click(object sender, EventArgs e)
        {
            try
            {
                var operacao = new ProviderOperation(GetCodigoCliente());

                var mensagem = new NotifyNoShowMessage
                {
                    ExternalId = GetExternoId()
                };

                operacao.NotifyNoShow(mensagem);

                operacao.Execute(GetConfig());
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        private void btnRecusarSolicitacao_Click(object sender, EventArgs e)
        {
            try
            {
                var operacao = new ProviderOperation(GetCodigoCliente());

                var mensagem = new RefuseRequestMessage
                {
                    DrakeId = GetDrakeId(),
                    Details = txtMotivo.Text
                };

                operacao.RefuseRequest(mensagem);

                operacao.Execute(GetConfig());
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        private void btnConfirmarSolicitacaoCancelamento_Click(object sender, EventArgs e)
        {
            try
            {
                var operacao = new ProviderOperation(GetCodigoCliente());

                var mensagem = new CancelationConfirmationMessage
                {
                    ExternalId = GetExternoId()
                };

                operacao.CancelConfirmation(mensagem);

                operacao.Execute(GetConfig());
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        private void btnConfirmarSolicitacaoCancelamentoComReembolso_Click(object sender, EventArgs e)
        {
            try
            {
                var operacao = new ProviderOperation(GetCodigoCliente());

                var mensagem = new CancelationConfirmationWithRefundMessage
                {
                    ExternalId = GetExternoId(),
                    RefundValue = decimal.Parse(txtReembolso.Text)
                };

                operacao.CancelConfirmationWithRefund(mensagem);

                operacao.Execute(GetConfig());
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        private void btnCaixaEntrada_Click(object sender, EventArgs e)
        {
            try
            {
                var caixaSaida = new FormCaixaEntrada();

                caixaSaida.SenderId = GetCodigoFornecedor();
                caixaSaida.PrivateKey = txtChavePrivada.Text;

                caixaSaida.Show(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }   
        
        }

        private void btnSolicitarAprovacaoParaFaturamento_Click(object sender, EventArgs e)
        {
            var form = new FormSolicitarAprovacaoFaturamento
            {
                ConfigStrategy = GetConfig(),
                CodigoCliente = GetCodigoCliente(),
                CodigoFornecedor = GetCodigoFornecedor(),
                ChavePrivadaFornecedor = txtChavePrivada.Text
            };

            form.ShowDialog(this);
        }

        private void FormOperacoesFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void btnConfirmarRecebimento_Click(object sender, EventArgs e)
        {
            try
            {
                var operacao = new ReadOperation();                             

                if (string.IsNullOrEmpty(txtCodigoExterno.Text))
                {
                    var lista = new List<long>();
                    lista.Add(Int32.Parse(txtCodigoMensagem.Text));
                    operacao.ConfirmReadedMessages(new ConfigStrategy(GetCodigoFornecedor(), txtChavePrivada.Text), lista);
                }
                else
                {
                    var dic = new Dictionary<long, string>();
                    dic.Add(Int32.Parse(txtCodigoMensagem.Text), txtCodigoExterno.Text);
                    operacao.ConfirmReadedMessages(new ConfigStrategy(GetCodigoFornecedor(), txtChavePrivada.Text), dic);
                }

                MessageBox.Show("Recebimento confirmado!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cboxReusarAtendimento_CheckedChanged(object sender, EventArgs e)
        {
            txtCodigoAtendimentoReusado.Enabled = cboxReusarAtendimento.Checked;
            txtAbatimentos.Enabled = cboxReusarAtendimento.Checked;
        }

        private void label50_Click(object sender, EventArgs e)
        {

        }
    }

}
