﻿using System.Configuration;
using iDrake.Client.Config;

namespace Gerador_mensagens_de_teste_iDrake
{
    
    public class ConfigStrategy: IStrategyConfiguration
    {

        private readonly int _senderId;
        private readonly string _privateKey;
        private string IntegrationUrl = ConfigurationManager.AppSettings.Get("iDrakeUrl");


        public ConfigStrategy(int senderId, string privateKey)
        {
            _senderId = senderId;
            _privateKey = privateKey;
        }
        public int? GetSenderId()
        {
            return _senderId;
        }

        public string GetPrivateKey()
        {
            return _privateKey;
        }

        public string GetIntegrationServiceUrl()
        {
            return IntegrationUrl;
        }

        public bool IsInvalidSslCertificateAccepted()
        {
            return true;
        }

    }
}
