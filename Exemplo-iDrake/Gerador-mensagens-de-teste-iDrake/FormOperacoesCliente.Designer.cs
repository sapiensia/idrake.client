﻿namespace Gerador_mensagens_de_teste_iDrake
{
    partial class FormOperacoesCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOperacoesCliente));
            this.btnCaixaEntrada = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCancelarServico = new System.Windows.Forms.Button();
            this.txtDetalhes = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboxReceberConfirmacaoRecebimento = new System.Windows.Forms.CheckBox();
            this.txtChavePrivada = new System.Windows.Forms.TextBox();
            this.txtCodigoFornecedor = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtCodigoCliente = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDrakeId = new System.Windows.Forms.TextBox();
            this.txtExternoId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.btnAprovarFatura = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label82 = new System.Windows.Forms.Label();
            this.btnSolicitarAlterarNecessidade = new System.Windows.Forms.Button();
            this.btnReprovarUmaFatura = new System.Windows.Forms.Button();
            this.btnConfirmarRecebimento = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtCodigoMensagem = new System.Windows.Forms.TextBox();
            this.groupBox9.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCaixaEntrada
            // 
            this.btnCaixaEntrada.Location = new System.Drawing.Point(671, 162);
            this.btnCaixaEntrada.Name = "btnCaixaEntrada";
            this.btnCaixaEntrada.Size = new System.Drawing.Size(248, 33);
            this.btnCaixaEntrada.TabIndex = 40;
            this.btnCaixaEntrada.Text = "Ver caixa de entrada";
            this.btnCaixaEntrada.UseVisualStyleBackColor = true;
            this.btnCaixaEntrada.Click += new System.EventHandler(this.btnCaixaEntrada_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label6);
            this.groupBox9.Controls.Add(this.btnCancelarServico);
            this.groupBox9.Controls.Add(this.txtDetalhes);
            this.groupBox9.Location = new System.Drawing.Point(14, 162);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(240, 108);
            this.groupBox9.TabIndex = 39;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Solicitar cancelamento de serviço";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Detalhes";
            // 
            // btnCancelarServico
            // 
            this.btnCancelarServico.Location = new System.Drawing.Point(152, 79);
            this.btnCancelarServico.Name = "btnCancelarServico";
            this.btnCancelarServico.Size = new System.Drawing.Size(75, 23);
            this.btnCancelarServico.TabIndex = 19;
            this.btnCancelarServico.Text = "Enviar";
            this.btnCancelarServico.Click += new System.EventHandler(this.btnCancelarServico_Click_1);
            // 
            // txtDetalhes
            // 
            this.txtDetalhes.Location = new System.Drawing.Point(6, 32);
            this.txtDetalhes.Multiline = true;
            this.txtDetalhes.Name = "txtDetalhes";
            this.txtDetalhes.Size = new System.Drawing.Size(221, 41);
            this.txtDetalhes.TabIndex = 17;
            this.txtDetalhes.Text = "Trabalhador não precisará mais ir ao evento";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboxReceberConfirmacaoRecebimento);
            this.groupBox1.Controls.Add(this.txtChavePrivada);
            this.groupBox1.Controls.Add(this.txtCodigoFornecedor);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.txtCodigoCliente);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtDrakeId);
            this.groupBox1.Controls.Add(this.txtExternoId);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(14, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(905, 108);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados básicos";
            // 
            // cboxReceberConfirmacaoRecebimento
            // 
            this.cboxReceberConfirmacaoRecebimento.AutoSize = true;
            this.cboxReceberConfirmacaoRecebimento.Location = new System.Drawing.Point(684, 83);
            this.cboxReceberConfirmacaoRecebimento.Name = "cboxReceberConfirmacaoRecebimento";
            this.cboxReceberConfirmacaoRecebimento.Size = new System.Drawing.Size(204, 17);
            this.cboxReceberConfirmacaoRecebimento.TabIndex = 39;
            this.cboxReceberConfirmacaoRecebimento.Text = "Receber confirmação de recebimento";
            this.cboxReceberConfirmacaoRecebimento.UseVisualStyleBackColor = true;
            // 
            // txtChavePrivada
            // 
            this.txtChavePrivada.Location = new System.Drawing.Point(169, 57);
            this.txtChavePrivada.Name = "txtChavePrivada";
            this.txtChavePrivada.Size = new System.Drawing.Size(718, 20);
            this.txtChavePrivada.TabIndex = 38;
            this.txtChavePrivada.Text = resources.GetString("txtChavePrivada.Text");
            // 
            // txtCodigoFornecedor
            // 
            this.txtCodigoFornecedor.Location = new System.Drawing.Point(788, 19);
            this.txtCodigoFornecedor.Name = "txtCodigoFornecedor";
            this.txtCodigoFornecedor.Size = new System.Drawing.Size(100, 20);
            this.txtCodigoFornecedor.TabIndex = 37;
            this.txtCodigoFornecedor.Text = "6";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(625, 22);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(160, 13);
            this.label42.TabIndex = 36;
            this.label42.Text = "Código Fornecedor (destinatário)";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(14, 60);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(127, 13);
            this.label33.TabIndex = 35;
            this.label33.Text = "Chave Privada do Cliente";
            // 
            // txtCodigoCliente
            // 
            this.txtCodigoCliente.Location = new System.Drawing.Point(514, 19);
            this.txtCodigoCliente.Name = "txtCodigoCliente";
            this.txtCodigoCliente.Size = new System.Drawing.Size(100, 20);
            this.txtCodigoCliente.TabIndex = 34;
            this.txtCodigoCliente.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Drake ID";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(356, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(146, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Código do Cliente (remetente)";
            // 
            // txtDrakeId
            // 
            this.txtDrakeId.Location = new System.Drawing.Point(70, 19);
            this.txtDrakeId.Name = "txtDrakeId";
            this.txtDrakeId.Size = new System.Drawing.Size(100, 20);
            this.txtDrakeId.TabIndex = 23;
            // 
            // txtExternoId
            // 
            this.txtExternoId.Location = new System.Drawing.Point(240, 18);
            this.txtExternoId.Name = "txtExternoId";
            this.txtExternoId.Size = new System.Drawing.Size(100, 20);
            this.txtExternoId.TabIndex = 32;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(181, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Externo ID";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(295, 14);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(406, 24);
            this.label43.TabIndex = 38;
            this.label43.Text = "Este formulário é para ser usado por um cliente";
            // 
            // btnAprovarFatura
            // 
            this.btnAprovarFatura.Location = new System.Drawing.Point(160, 29);
            this.btnAprovarFatura.Name = "btnAprovarFatura";
            this.btnAprovarFatura.Size = new System.Drawing.Size(75, 23);
            this.btnAprovarFatura.TabIndex = 44;
            this.btnAprovarFatura.Text = "Enviar";
            this.btnAprovarFatura.Click += new System.EventHandler(this.btnAprovarFatura_Click_1);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label82);
            this.groupBox3.Controls.Add(this.btnAprovarFatura);
            this.groupBox3.Location = new System.Drawing.Point(260, 162);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(248, 60);
            this.groupBox3.TabIndex = 42;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Aprovar fatura";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(6, 15);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(140, 13);
            this.label82.TabIndex = 43;
            this.label82.Text = "Código da fatura no DRAKE";
            // 
            // btnSolicitarAlterarNecessidade
            // 
            this.btnSolicitarAlterarNecessidade.Location = new System.Drawing.Point(671, 197);
            this.btnSolicitarAlterarNecessidade.Name = "btnSolicitarAlterarNecessidade";
            this.btnSolicitarAlterarNecessidade.Size = new System.Drawing.Size(248, 33);
            this.btnSolicitarAlterarNecessidade.TabIndex = 43;
            this.btnSolicitarAlterarNecessidade.Text = "Solicitar/alterar necessidade";
            this.btnSolicitarAlterarNecessidade.UseVisualStyleBackColor = true;
            this.btnSolicitarAlterarNecessidade.Click += new System.EventHandler(this.btnSolicitarAlterarNecessidade_Click);
            // 
            // btnReprovarUmaFatura
            // 
            this.btnReprovarUmaFatura.Location = new System.Drawing.Point(671, 236);
            this.btnReprovarUmaFatura.Name = "btnReprovarUmaFatura";
            this.btnReprovarUmaFatura.Size = new System.Drawing.Size(247, 28);
            this.btnReprovarUmaFatura.TabIndex = 44;
            this.btnReprovarUmaFatura.Text = "Reprovar uma fatura";
            this.btnReprovarUmaFatura.UseVisualStyleBackColor = true;
            this.btnReprovarUmaFatura.Click += new System.EventHandler(this.btnReprovarUmaFatura_Click);
            // 
            // btnConfirmarRecebimento
            // 
            this.btnConfirmarRecebimento.Location = new System.Drawing.Point(152, 45);
            this.btnConfirmarRecebimento.Name = "btnConfirmarRecebimento";
            this.btnConfirmarRecebimento.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmarRecebimento.TabIndex = 45;
            this.btnConfirmarRecebimento.Text = "Enviar";
            this.btnConfirmarRecebimento.UseVisualStyleBackColor = true;
            this.btnConfirmarRecebimento.Click += new System.EventHandler(this.btnConfirmarRecebimento_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCodigoMensagem);
            this.groupBox2.Controls.Add(this.btnConfirmarRecebimento);
            this.groupBox2.Location = new System.Drawing.Point(14, 288);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(238, 86);
            this.groupBox2.TabIndex = 46;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Confirmar recebimento";
            // 
            // txtCodigoMensagem
            // 
            this.txtCodigoMensagem.Location = new System.Drawing.Point(6, 19);
            this.txtCodigoMensagem.Name = "txtCodigoMensagem";
            this.txtCodigoMensagem.Size = new System.Drawing.Size(221, 20);
            this.txtCodigoMensagem.TabIndex = 46;
            // 
            // FormOperacoesCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(930, 439);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnReprovarUmaFatura);
            this.Controls.Add(this.btnSolicitarAlterarNecessidade);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.btnCaixaEntrada);
            this.Name = "FormOperacoesCliente";
            this.Text = "Operações Realizadas por Clientes";
            this.Load += new System.EventHandler(this.FormSolicitarAlterarNecessidade_Load);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtChavePrivada;
        private System.Windows.Forms.TextBox txtCodigoFornecedor;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtCodigoCliente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDrakeId;
        private System.Windows.Forms.TextBox txtExternoId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCancelarServico;
        private System.Windows.Forms.TextBox txtDetalhes;
        private System.Windows.Forms.Button btnCaixaEntrada;
        private System.Windows.Forms.Button btnAprovarFatura;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Button btnSolicitarAlterarNecessidade;
        private System.Windows.Forms.Button btnReprovarUmaFatura;
        private System.Windows.Forms.CheckBox cboxReceberConfirmacaoRecebimento;
        private System.Windows.Forms.Button btnConfirmarRecebimento;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtCodigoMensagem;
    }
}