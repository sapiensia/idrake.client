﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using iDrake.Client;
using iDrake.Client.Config;
using iDrake.Client.Stubs.Enums;

namespace Gerador_mensagens_de_teste_iDrake
{
    public partial class FormSolicitarAlterarNecessidade : Form
    {
        public FormSolicitarAlterarNecessidade()
        {
            InitializeComponent();
        }

        public IStrategyConfiguration ConfigStrategy { get; set; }

        public long CodigoFornecedor { get; set; }

        public string ChavePrivadaCliente { get; set; }

        public long CodigoCliente { get; set; }
        public bool ReceberConfirmacao { get; set; }


        private int GetCodigoCliente()
        {
            return int.Parse(txtCodigoCliente.Text);
        }

        private int GetCodigoFornecedor()
        {
            return int.Parse(txtCodigoFornecedor.Text);
        }

        private int GetDrakeId()
        {
            return int.Parse(txtDrakeId.Text);
        }

        private string GetExternoId()
        {
            return txtExternoId.Text;
        }

        private LogisticType GetTipo()
        {
            switch (comboBoxTipo.SelectedItem.ToString())
            {
                case "Aluguel de carro":
                    return LogisticType.CarRental;

                case "Transporte aéreo":
                    return LogisticType.FlightTravel;

                case "Hospedagem":
                    return LogisticType.Hotel;
                   
                case "Transporte rodoviário":
                    return LogisticType.RoadTravel;

                default:
                    return LogisticType.RoadTravel;
            }
        }

        private Address GetOrigemParticipante2()
        {
            var endereco = new Address
            {
                City = txtCidadeOrigem2.Text,
                Complement = txtComplementoOrigem2.Text,
                Country = txtPaisOrigem2.Text,
                District = txtBairroOrigem2.Text,
                IATA = txtIATAOrigem2.Text,
                Number = txtNumeroOrigem2.Text,
                State = txtEstadoOrigem2.Text,
                Street = txtRuaOrigem2.Text,
                Zip = txtCEPOrigem2.Text
            };

            try
            {
                endereco.Latitude = double.Parse(txtLatitudeOrigem2.Text);
            }
            catch (Exception)
            {
            }

            try
            {
                endereco.Longitude = double.Parse(txtLongitudeOrigem2.Text);
            }
            catch (Exception)
            {
            }

            return endereco;
        }

        private Address GetOrigemParticipante1()
        {
            var endereco = new Address
            {
                City = txtCidadeOrigem.Text,
                Complement = txtComplementoOrigem.Text,
                Country = txtPaisOrigem.Text,
                District = txtBairroOrigem.Text,
                IATA = txtIATAOrigem.Text,
                Number = txtNumeroOrigem.Text,
                State = txtEstadoOrigem.Text,
                Street = txtRuaOrigem.Text,
                Zip = txtCEPOrigem.Text
            };

            try
            {
                endereco.Latitude = double.Parse(txtLatitudeOrigem.Text);
            }
            catch (Exception)
            {
            }

            try
            {
                endereco.Longitude = double.Parse(txtLongitudeOrigem.Text);
            }
            catch (Exception)
            {
            }

            return endereco;
        }

        private Address GetDestinoParticipante2()
        {
            var endereco = new Address
            {
                City = txtCidadeDestino2.Text,
                Complement = txtComplementoDestino2.Text,
                Country = txtPaisDestino2.Text,
                District = txtBairroDestino2.Text,
                IATA = txtIATADestino2.Text,
                Number = txtNumeroDestino2.Text,
                State = txtEstadoDestino2.Text,
                Street = txtRuaDestino2.Text,
                Zip = txtCEPDestino2.Text
            };

            try
            {
                endereco.Latitude = double.Parse(txtLatitudeDestino2.Text);
            }
            catch (Exception)
            {
            }

            try
            {
                endereco.Longitude = double.Parse(txtLongitudeDestino2.Text);
            }
            catch (Exception)
            {
            }

            return endereco;
        }

        private Address GetDestinoParticipante1()
        {
            var endereco = new Address
            {
                City = txtCidadeDestino1.Text,
                Complement = txtComplementoDestino1.Text,
                Country = txtPaisDestino1.Text,
                District = txtBairroDestino1.Text,
                IATA = txtIATADestino1.Text,
                Number = txtNumeroDestino1.Text,
                State = txtEstadoDestino1.Text,
                Street = txtRuaDestino1.Text,
                Zip = txtCEPDestino1.Text
            };

            try
            {
                endereco.Latitude = double.Parse(txtLatitudeDestino1.Text);
            }
            catch (Exception)
            {
            }

            try
            {
                endereco.Longitude = double.Parse(txtLongitudeDestino1.Text);
            }
            catch (Exception)
            {
            }

            return endereco;
        }

        private Participant GetParticipante2()
        {
            var participante = new Participant
            {
                Cpf = txtCPFParticipante2.Text,
                CostCenterCode = txtCodigoCentroCustoParticipante2.Text,
                DrakeId = long.Parse(txtDrakeIDParticipante2.Text),
                Emails = txtEmailParticipante2.Text,
                Identity = txtIdentidadeParticipante2.Text,
                Name = txtNomeParticipante2.Text,
                Passports = txtPassaporteParticipante2.Text,
                PhoneNumbers = txtTelefonesParticipante2.Text
            };

            participante.Details.Add("COST_CENTER_ID", txtCodigoCentroCustoParticipante2.Text);
            participante.Details.Add("COST_CENTER_NAME", txtNomeCentroCustoParticipante2.Text);

            if (comboBoxSexoParticipante2.SelectedText == "Feminino")
            {
                participante.Gender = Gender.Female; 
            }
            else
            {
                participante.Gender = Gender.Male;
            }             

            return participante;
        }

        private Participant GetParticipante1()
        {
            var participante = new Participant
            {
                Cpf = txtCPFParticipante1.Text,
                CostCenterCode = txtCodigoCentroCustoParticipante1.Text,
                DrakeId = long.Parse(txtDrakeIDParticipante1.Text),
                Emails = txtEmailParticipante1.Text,
                Identity = txtIdentidadeParticipante1.Text,
                Name = txtNomeParticipante1.Text,
                Passports = txtPassaporteParticipante1.Text,
                PhoneNumbers = txtTelefonesParticipante1.Text
            };

            participante.Details.Add("COST_CENTER_ID", txtCodigoCentroCustoParticipante1.Text);
            participante.Details.Add("COST_CENTER_NAME", txtNomeCentroCustoParticipante1.Text);

            if (comboBoxSexoParticipante1.SelectedText == "Feminino")
            {
                participante.Gender = Gender.Female;
            }
            else
            {
                participante.Gender = Gender.Male;
            }

            return participante;
        }

        private void btnEnviarSolicitacao_Click(object sender, EventArgs e)
        {
            try
            {
                var operacao = new CustomerOperation();

                var necessidadeLogistica = new LogisticNeed();
                necessidadeLogistica.Type = GetTipo();
                necessidadeLogistica.Subtype = txtSubtipo.Text;
                necessidadeLogistica.Start = dtpInicio.Value;
                necessidadeLogistica.End = dtpTermino.Value;
                necessidadeLogistica.Origin = GetOrigemParticipante1();

                if (checkBoxAssociada.Checked)
                {
                    necessidadeLogistica.DrakeId = long.Parse(txtDrakeId.Text);

                    var necessidade1 = new LogisticNeed();
                    necessidade1.DrakeId = long.Parse(txtDrakeIDNecessidade1.Text);
                    necessidade1.Comments = txtComentarioNecessidade1.Text;
                    necessidade1.Origin = GetOrigemParticipante1();
                    necessidade1.Destination = GetDestinoParticipante1();
                    necessidade1.Type = GetTipo();
                    necessidade1.Subtype = txtSubtipo.Text;
                    necessidade1.PickupOrder = int.Parse(txtOrdemSaidaParticipante1.Text);
                    necessidade1.DeliveryOrder = int.Parse(txtOrdemChegadaParticipante1.Text);
                    necessidade1.Participant = GetParticipante1();
                    necessidade1.Details.Add("COST_CENTER_ID", txtCodigoCentroCustoNecessidade1.Text);
                    necessidade1.Details.Add("COST_CENTER_NAME", txtCodigoCentroCustoNecessidade1.Text);
                                                                
                    necessidadeLogistica.Associated.Add(necessidade1);

                    var necessidade2 = new LogisticNeed();
                    necessidade2.DrakeId = long.Parse(txtDrakeIDNecessidade2.Text);
                    necessidade2.Comments = txtComentarioNecessidade2.Text;
                    necessidade2.Origin = GetOrigemParticipante2();
                    necessidade2.Destination = GetDestinoParticipante2();
                    necessidade2.Type = GetTipo();
                    necessidade2.Subtype = txtSubtipo.Text;
                    necessidade2.PickupOrder = int.Parse(txtOrdemSaidaParticipante2.Text);
                    necessidade2.DeliveryOrder = int.Parse(txtOrdemChegadaParticipante2.Text);
                    necessidade2.Participant = GetParticipante2();
                    necessidade2.Details.Add("COST_CENTER_ID", txtCodigoCentroCustoNecessidade2.Text);
                    necessidade2.Details.Add("COST_CENTER_NAME", txtCodigoCentroCustoNecessidade2.Text);
                    
                    necessidadeLogistica.Associated.Add(necessidade2);

                }
                else
                {
                    necessidadeLogistica.DrakeId = long.Parse(txtDrakeId.Text);
                    necessidadeLogistica.Comments = txtComentarioNecessidade1.Text;
                    necessidadeLogistica.Origin = GetOrigemParticipante1();
                    necessidadeLogistica.Destination = GetDestinoParticipante1();
                    necessidadeLogistica.Type = GetTipo();
                    necessidadeLogistica.Subtype = txtSubtipo.Text;
                    necessidadeLogistica.PickupOrder = int.Parse(txtOrdemSaidaParticipante1.Text);
                    necessidadeLogistica.DeliveryOrder = int.Parse(txtOrdemChegadaParticipante1.Text);
                    necessidadeLogistica.Participant = GetParticipante1();
                    necessidadeLogistica.Details.Add("COST_CENTER_ID", txtCodigoCentroCustoNecessidade.Text);
                    necessidadeLogistica.Details.Add("COST_CENTER_NAME", txtNomeCentroCustoNecessidade.Text);
                }

                if (checkboxAlteracao.Checked)
                {
                    var mensagem = new ChangeRequestMessage
                    {
                        DrakeId = necessidadeLogistica.DrakeId,
                        LogisticNeed = necessidadeLogistica,
                        SenderId = GetCodigoCliente(),
                        RecipientId = GetCodigoFornecedor(),
                        Acknowledgment = ReceberConfirmacao
                    };


                    if (cboxReusoAtendimento.Checked)
                    {
                        mensagem.TreatmentIdToTryReuse = txtCodigoAtendimentoReusado.Text;
                    }

                    operacao.ChangeRequest(mensagem);
                }
                else
                {
                    var mensagem = new ServiceRequestMessage()
                    {
                        DrakeId = necessidadeLogistica.DrakeId,
                        LogisticNeed = necessidadeLogistica,
                        SenderId = GetCodigoCliente(),
                        RecipientId = GetCodigoFornecedor(),
                        Acknowledgment = ReceberConfirmacao
                    };

                    if (cboxReusoAtendimento.Checked)
                    {
                        mensagem.TreatmentIdToTryReuse = txtCodigoAtendimentoReusado.Text;
                    }

                    operacao.ServiceRequest(mensagem);
                }

                

                operacao.Execute(new ConfigStrategy(GetCodigoCliente(), txtChavePrivada.Text));
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        private void btnCancelarServico_Click(object sender, EventArgs e)
        {
            try
            {     
                var operacao = new CustomerOperation();

                var mensagem = new CancelationRequestMessage
                {
                    DrakeId = long.Parse(txtDrakeId.Text),
                    SenderId = GetCodigoCliente(),
                    RecipientId = GetCodigoFornecedor(),
                    Acknowledgment = ReceberConfirmacao
                };

                operacao.CancelationRequest(mensagem);

                operacao.Execute(new ConfigStrategy(GetCodigoCliente(), txtChavePrivada.Text));
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        private void btnCaixaEntrada_Click(object sender, EventArgs e)
        {
            try
            {
                var caixaSaida = new FormCaixaEntrada();

                caixaSaida.SenderId = GetCodigoCliente();
                caixaSaida.PrivateKey = txtChavePrivada.Text;

                caixaSaida.Show(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }   
        }

        private void checkBoxAssociada_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxAssociada.Checked)
            {
                txtDrakeIDNecessidade1.Enabled = true;
                groupBoxParticipante2.Enabled = true;
            }
            else
            {
                txtDrakeIDNecessidade1.Enabled = false;
                groupBoxParticipante2.Enabled = false;
            }
        }

        private void FormSolicitarAlterarNecessidade_Load(object sender, EventArgs e)
        {
            txtCodigoFornecedor.Text = CodigoFornecedor.ToString();
            txtCodigoCliente.Text = CodigoCliente.ToString();
            txtChavePrivada.Text = ChavePrivadaCliente;
        }

        private void cboxReusoAtendimento_CheckedChanged(object sender, EventArgs e)
        {
            txtCodigoAtendimentoReusado.Enabled = cboxReusoAtendimento.Checked;
        }

    }
}
