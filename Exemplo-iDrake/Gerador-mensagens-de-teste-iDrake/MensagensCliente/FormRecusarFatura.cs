﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using iDrake.Client;
using iDrake.Client.Config;
using iDrake.Client.Stubs.Enums;

namespace Gerador_mensagens_de_teste_iDrake.MensagensCliente
{
    public partial class FormRecusarFatura : Form
    {
        public FormRecusarFatura()
        {
            InitializeComponent();
        }

        public IStrategyConfiguration ConfigStrategy { get; set; }

        public long CodigoFornecedor { get; set; }

        public string ChavePrivadaCliente { get; set; }

        public int CodigoCliente { get; set; }

        public bool ReceberConfirmacao { get; set; }

        private void FormRecusarFatura_Load(object sender, EventArgs e)
        {
            txtCodigoFornecedor.Text = CodigoFornecedor.ToString();
            txtCodigoCliente.Text = CodigoCliente.ToString();
            txtChavePrivadaFornecedor.Text = ChavePrivadaCliente;

            CarregarFaturas();
        }

        private Dictionary<string, InvoiceApprovalRequestMessage> faturas = new Dictionary<string, InvoiceApprovalRequestMessage>();

        private void CarregarFaturas()
        {
            var operacao = new ReadOperation();
            var mensagens = operacao.ReadMessages(new ConfigStrategy(CodigoCliente, ChavePrivadaCliente));

            foreach (var mensagem in mensagens)
            {                                            
                if (mensagem is InvoiceApprovalRequestMessage)
                {
                    var mensagemCast = mensagem as InvoiceApprovalRequestMessage;

                    if (mensagemCast.ExternalId != null)
                    {
                        comboBoxFaturas.Items.Add(mensagemCast.ExternalId);

                        if (faturas.ContainsKey(mensagemCast.ExternalId) == false)
                        {
                            faturas.Add(mensagemCast.ExternalId, mensagemCast);
                        }
                        else
                        {
                            faturas[mensagemCast.ExternalId] = mensagemCast;
                        }
                    }
                }                                        
            }   
        }

        private void comboBoxFaturas_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarFatura(comboBoxFaturas.SelectedItem.ToString());
        }

        private void CarregarFatura(string faturaID)
        {
            var mensagem = faturas[faturaID];

            var table = new DataTable();

            table.Columns.Add("DrakeID");
            table.Columns.Add("ValorTotal");
            table.Columns.Add("ValorAdicional");
            table.Columns.Add("Desconto");
            table.Columns.Add("ObservacaoFornecedor");
            table.Columns.Add("ObservacaoCliente");
            table.Columns.Add(new DataColumn("Recusado", typeof(bool)));

            if (mensagem.Items != null)
            {
                foreach (var item in mensagem.Items)
                {
                    //table.Rows.Add(item.DrakeId, item.Fare, item.Extra, item.Rebate, item.Comments, null, false);
                }
            }

            dataGridView.DataSource = table;
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            var tabela = dataGridView.DataSource as DataTable;

            if (tabela == null)
            {
                return;
            }

            var mensagem = new RefuseInvoiceMessage();

            foreach (DataRow linha in tabela.Rows)
            {
                var drakeId = long.Parse(linha["DrakeID"].ToString());
                var observacaoCliente = linha["ObservacaoCliente"].ToString();
                var recusado = bool.Parse(linha["Recusado"].ToString());

                var status = InvoiceItemStatus.ApprovalRequested;

                if (recusado)
                {
                    status = InvoiceItemStatus.Refused;    
                }
                else
                {
                    status = InvoiceItemStatus.Approved;
                }

                var item = new InvoiceItem
                {
                    DrakeId = drakeId,
                    Status = status,
                    Appraisal = observacaoCliente
                };

                mensagem.AddItem(item);
            }

            mensagem.DrakeId = long.Parse(txtCodigoFaturaDrake.Text);
            mensagem.ExternalId = comboBoxFaturas.SelectedItem.ToString();
            mensagem.Acknowledgment = ReceberConfirmacao;

            try
            {
                var operacao = new CustomerOperation(int.Parse(txtCodigoFornecedor.Text));

                operacao.RefuseInvoice(mensagem);
                operacao.Execute(new ConfigStrategy(int.Parse(txtCodigoCliente.Text), txtChavePrivadaFornecedor.Text));
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

    }

}
