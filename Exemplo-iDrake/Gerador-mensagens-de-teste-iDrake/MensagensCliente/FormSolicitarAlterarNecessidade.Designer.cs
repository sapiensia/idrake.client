﻿namespace Gerador_mensagens_de_teste_iDrake
{
    partial class FormSolicitarAlterarNecessidade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label83 = new System.Windows.Forms.Label();
            this.txtNomeCentroCustoNecessidade = new System.Windows.Forms.TextBox();
            this.txtCodigoCentroCustoNecessidade = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.txtSubtipo = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxTipo = new System.Windows.Forms.ComboBox();
            this.checkBoxAssociada = new System.Windows.Forms.CheckBox();
            this.checkboxAlteracao = new System.Windows.Forms.CheckBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label85 = new System.Windows.Forms.Label();
            this.txtNomeCentroCustoNecessidade1 = new System.Windows.Forms.TextBox();
            this.txtCodigoCentroCustoNecessidade1 = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNomeCentroCustoParticipante1 = new System.Windows.Forms.TextBox();
            this.comboBoxSexoParticipante1 = new System.Windows.Forms.ComboBox();
            this.txtEmailParticipante1 = new System.Windows.Forms.TextBox();
            this.txtCodigoCentroCustoParticipante1 = new System.Windows.Forms.TextBox();
            this.txtTelefonesParticipante1 = new System.Windows.Forms.TextBox();
            this.txtPassaporteParticipante1 = new System.Windows.Forms.TextBox();
            this.txtCPFParticipante1 = new System.Windows.Forms.TextBox();
            this.txtIdentidadeParticipante1 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txtComentarioNecessidade1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNomeParticipante1 = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.txtDrakeIDNecessidade1 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtIATADestino1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNumeroDestino1 = new System.Windows.Forms.TextBox();
            this.txtCEPDestino1 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.txtLongitudeDestino1 = new System.Windows.Forms.TextBox();
            this.txtLatitudeDestino1 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.txtPaisDestino1 = new System.Windows.Forms.TextBox();
            this.txtEstadoDestino1 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.txtCidadeDestino1 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.txtBairroDestino1 = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.txtComplementoDestino1 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.txtRuaDestino1 = new System.Windows.Forms.TextBox();
            this.txtOrdemChegadaParticipante1 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtDrakeIDParticipante1 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtIATAOrigem = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtNumeroOrigem = new System.Windows.Forms.TextBox();
            this.txtCEPOrigem = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtLongitudeOrigem = new System.Windows.Forms.TextBox();
            this.txtLatitudeOrigem = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtPaisOrigem = new System.Windows.Forms.TextBox();
            this.txtEstadoOrigem = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCidadeOrigem = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtBairroOrigem = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtComplementoOrigem = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtRuaOrigem = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtOrdemSaidaParticipante1 = new System.Windows.Forms.TextBox();
            this.groupBoxParticipante2 = new System.Windows.Forms.GroupBox();
            this.label87 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.txtNomeCentroCustoNecessidade2 = new System.Windows.Forms.TextBox();
            this.txtNomeCentroCustoParticipante2 = new System.Windows.Forms.TextBox();
            this.txtCodigoCentroCustoNecessidade2 = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.txtComentarioNecessidade2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxSexoParticipante2 = new System.Windows.Forms.ComboBox();
            this.txtEmailParticipante2 = new System.Windows.Forms.TextBox();
            this.txtCodigoCentroCustoParticipante2 = new System.Windows.Forms.TextBox();
            this.txtTelefonesParticipante2 = new System.Windows.Forms.TextBox();
            this.txtPassaporteParticipante2 = new System.Windows.Forms.TextBox();
            this.txtCPFParticipante2 = new System.Windows.Forms.TextBox();
            this.txtIdentidadeParticipante2 = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.txtNomeParticipante2 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtIATADestino2 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.txtNumeroDestino2 = new System.Windows.Forms.TextBox();
            this.txtCEPDestino2 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.txtLongitudeDestino2 = new System.Windows.Forms.TextBox();
            this.txtLatitudeDestino2 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.txtPaisDestino2 = new System.Windows.Forms.TextBox();
            this.txtEstadoDestino2 = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.txtCidadeDestino2 = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.txtBairroDestino2 = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.txtComplementoDestino2 = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.txtRuaDestino2 = new System.Windows.Forms.TextBox();
            this.txtDrakeIDNecessidade2 = new System.Windows.Forms.TextBox();
            this.txtOrdemChegadaParticipante2 = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtDrakeIDParticipante2 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtOrdemSaidaParticipante2 = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.txtIATAOrigem2 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtCEPOrigem2 = new System.Windows.Forms.TextBox();
            this.txtNumeroOrigem2 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtLongitudeOrigem2 = new System.Windows.Forms.TextBox();
            this.txtPaisOrigem2 = new System.Windows.Forms.TextBox();
            this.txtLatitudeOrigem2 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtEstadoOrigem2 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtCidadeOrigem2 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtBairroOrigem2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtComplementoOrigem2 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtRuaOrigem2 = new System.Windows.Forms.TextBox();
            this.dtpTermino = new System.Windows.Forms.DateTimePicker();
            this.dtpInicio = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEnviarSolicitacao = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtChavePrivada = new System.Windows.Forms.TextBox();
            this.txtCodigoFornecedor = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtCodigoCliente = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDrakeId = new System.Windows.Forms.TextBox();
            this.txtExternoId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.cboxReusoAtendimento = new System.Windows.Forms.CheckBox();
            this.txtCodigoAtendimentoReusado = new System.Windows.Forms.TextBox();
            this.groupBox4.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBoxParticipante2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtCodigoAtendimentoReusado);
            this.groupBox4.Controls.Add(this.cboxReusoAtendimento);
            this.groupBox4.Controls.Add(this.label89);
            this.groupBox4.Controls.Add(this.label83);
            this.groupBox4.Controls.Add(this.txtNomeCentroCustoNecessidade);
            this.groupBox4.Controls.Add(this.txtCodigoCentroCustoNecessidade);
            this.groupBox4.Controls.Add(this.label84);
            this.groupBox4.Controls.Add(this.txtSubtipo);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.comboBoxTipo);
            this.groupBox4.Controls.Add(this.checkBoxAssociada);
            this.groupBox4.Controls.Add(this.checkboxAlteracao);
            this.groupBox4.Controls.Add(this.groupBox11);
            this.groupBox4.Controls.Add(this.dtpTermino);
            this.groupBox4.Controls.Add(this.dtpInicio);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.btnEnviarSolicitacao);
            this.groupBox4.Location = new System.Drawing.Point(14, 162);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1048, 848);
            this.groupBox4.TabIndex = 36;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Solicitar necessidade";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(869, 29);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(35, 13);
            this.label83.TabIndex = 64;
            this.label83.Text = "Nome";
            // 
            // txtNomeCentroCustoNecessidade
            // 
            this.txtNomeCentroCustoNecessidade.Location = new System.Drawing.Point(910, 25);
            this.txtNomeCentroCustoNecessidade.Name = "txtNomeCentroCustoNecessidade";
            this.txtNomeCentroCustoNecessidade.Size = new System.Drawing.Size(109, 20);
            this.txtNomeCentroCustoNecessidade.TabIndex = 63;
            this.txtNomeCentroCustoNecessidade.Text = "BASE";
            // 
            // txtCodigoCentroCustoNecessidade
            // 
            this.txtCodigoCentroCustoNecessidade.Location = new System.Drawing.Point(678, 25);
            this.txtCodigoCentroCustoNecessidade.Name = "txtCodigoCentroCustoNecessidade";
            this.txtCodigoCentroCustoNecessidade.Size = new System.Drawing.Size(124, 20);
            this.txtCodigoCentroCustoNecessidade.TabIndex = 62;
            this.txtCodigoCentroCustoNecessidade.Text = "100";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(588, 29);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(83, 13);
            this.label84.TabIndex = 61;
            this.label84.Text = "Centro de Custo";
            // 
            // txtSubtipo
            // 
            this.txtSubtipo.Location = new System.Drawing.Point(542, 56);
            this.txtSubtipo.Name = "txtSubtipo";
            this.txtSubtipo.Size = new System.Drawing.Size(193, 20);
            this.txtSubtipo.TabIndex = 36;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(494, 59);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(43, 13);
            this.label28.TabIndex = 43;
            this.label28.Text = "Subtipo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(303, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 42;
            this.label9.Text = "Tipo";
            // 
            // comboBoxTipo
            // 
            this.comboBoxTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTipo.FormattingEnabled = true;
            this.comboBoxTipo.Items.AddRange(new object[] {
            "Transporte rodoviário",
            "Transporte aéreo",
            "Aluguel de carro",
            "Hospedagem"});
            this.comboBoxTipo.Location = new System.Drawing.Point(337, 56);
            this.comboBoxTipo.Name = "comboBoxTipo";
            this.comboBoxTipo.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTipo.TabIndex = 41;
            // 
            // checkBoxAssociada
            // 
            this.checkBoxAssociada.AutoSize = true;
            this.checkBoxAssociada.Location = new System.Drawing.Point(306, 31);
            this.checkBoxAssociada.Name = "checkBoxAssociada";
            this.checkBoxAssociada.Size = new System.Drawing.Size(219, 17);
            this.checkBoxAssociada.TabIndex = 18;
            this.checkBoxAssociada.Text = "É uma necessidade logística associada?";
            this.checkBoxAssociada.UseVisualStyleBackColor = true;
            this.checkBoxAssociada.CheckedChanged += new System.EventHandler(this.checkBoxAssociada_CheckedChanged);
            // 
            // checkboxAlteracao
            // 
            this.checkboxAlteracao.AutoSize = true;
            this.checkboxAlteracao.Location = new System.Drawing.Point(17, 825);
            this.checkboxAlteracao.Name = "checkboxAlteracao";
            this.checkboxAlteracao.Size = new System.Drawing.Size(109, 17);
            this.checkboxAlteracao.TabIndex = 17;
            this.checkboxAlteracao.Text = "É uma alteração?";
            this.checkboxAlteracao.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.groupBox13);
            this.groupBox11.Controls.Add(this.groupBoxParticipante2);
            this.groupBox11.Location = new System.Drawing.Point(22, 118);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(1009, 695);
            this.groupBox11.TabIndex = 15;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Endereços";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label85);
            this.groupBox13.Controls.Add(this.txtNomeCentroCustoNecessidade1);
            this.groupBox13.Controls.Add(this.txtCodigoCentroCustoNecessidade1);
            this.groupBox13.Controls.Add(this.label86);
            this.groupBox13.Controls.Add(this.label6);
            this.groupBox13.Controls.Add(this.txtNomeCentroCustoParticipante1);
            this.groupBox13.Controls.Add(this.comboBoxSexoParticipante1);
            this.groupBox13.Controls.Add(this.txtEmailParticipante1);
            this.groupBox13.Controls.Add(this.txtCodigoCentroCustoParticipante1);
            this.groupBox13.Controls.Add(this.txtTelefonesParticipante1);
            this.groupBox13.Controls.Add(this.txtPassaporteParticipante1);
            this.groupBox13.Controls.Add(this.txtCPFParticipante1);
            this.groupBox13.Controls.Add(this.txtIdentidadeParticipante1);
            this.groupBox13.Controls.Add(this.label50);
            this.groupBox13.Controls.Add(this.label49);
            this.groupBox13.Controls.Add(this.label48);
            this.groupBox13.Controls.Add(this.label47);
            this.groupBox13.Controls.Add(this.label46);
            this.groupBox13.Controls.Add(this.label45);
            this.groupBox13.Controls.Add(this.label44);
            this.groupBox13.Controls.Add(this.txtComentarioNecessidade1);
            this.groupBox13.Controls.Add(this.label4);
            this.groupBox13.Controls.Add(this.txtNomeParticipante1);
            this.groupBox13.Controls.Add(this.label61);
            this.groupBox13.Controls.Add(this.txtDrakeIDNecessidade1);
            this.groupBox13.Controls.Add(this.label51);
            this.groupBox13.Controls.Add(this.groupBox5);
            this.groupBox13.Controls.Add(this.txtOrdemChegadaParticipante1);
            this.groupBox13.Controls.Add(this.label31);
            this.groupBox13.Controls.Add(this.label30);
            this.groupBox13.Controls.Add(this.txtDrakeIDParticipante1);
            this.groupBox13.Controls.Add(this.groupBox2);
            this.groupBox13.Controls.Add(this.label29);
            this.groupBox13.Controls.Add(this.txtOrdemSaidaParticipante1);
            this.groupBox13.Location = new System.Drawing.Point(20, 25);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(483, 649);
            this.groupBox13.TabIndex = 36;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Participante #1";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(295, 613);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(35, 13);
            this.label85.TabIndex = 72;
            this.label85.Text = "Nome";
            // 
            // txtNomeCentroCustoNecessidade1
            // 
            this.txtNomeCentroCustoNecessidade1.Location = new System.Drawing.Point(336, 609);
            this.txtNomeCentroCustoNecessidade1.Name = "txtNomeCentroCustoNecessidade1";
            this.txtNomeCentroCustoNecessidade1.Size = new System.Drawing.Size(109, 20);
            this.txtNomeCentroCustoNecessidade1.TabIndex = 71;
            this.txtNomeCentroCustoNecessidade1.Text = "BASE";
            // 
            // txtCodigoCentroCustoNecessidade1
            // 
            this.txtCodigoCentroCustoNecessidade1.Location = new System.Drawing.Point(104, 609);
            this.txtCodigoCentroCustoNecessidade1.Name = "txtCodigoCentroCustoNecessidade1";
            this.txtCodigoCentroCustoNecessidade1.Size = new System.Drawing.Size(124, 20);
            this.txtCodigoCentroCustoNecessidade1.TabIndex = 70;
            this.txtCodigoCentroCustoNecessidade1.Text = "100";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(14, 613);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(83, 13);
            this.label86.TabIndex = 69;
            this.label86.Text = "Centro de Custo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(295, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 60;
            this.label6.Text = "Nome";
            // 
            // txtNomeCentroCustoParticipante1
            // 
            this.txtNomeCentroCustoParticipante1.Location = new System.Drawing.Point(336, 180);
            this.txtNomeCentroCustoParticipante1.Name = "txtNomeCentroCustoParticipante1";
            this.txtNomeCentroCustoParticipante1.Size = new System.Drawing.Size(109, 20);
            this.txtNomeCentroCustoParticipante1.TabIndex = 59;
            this.txtNomeCentroCustoParticipante1.Text = "BASE";
            // 
            // comboBoxSexoParticipante1
            // 
            this.comboBoxSexoParticipante1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSexoParticipante1.FormattingEnabled = true;
            this.comboBoxSexoParticipante1.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.comboBoxSexoParticipante1.Location = new System.Drawing.Point(103, 95);
            this.comboBoxSexoParticipante1.Name = "comboBoxSexoParticipante1";
            this.comboBoxSexoParticipante1.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSexoParticipante1.TabIndex = 58;
            // 
            // txtEmailParticipante1
            // 
            this.txtEmailParticipante1.Location = new System.Drawing.Point(104, 208);
            this.txtEmailParticipante1.Name = "txtEmailParticipante1";
            this.txtEmailParticipante1.Size = new System.Drawing.Size(189, 20);
            this.txtEmailParticipante1.TabIndex = 57;
            // 
            // txtCodigoCentroCustoParticipante1
            // 
            this.txtCodigoCentroCustoParticipante1.Location = new System.Drawing.Point(104, 180);
            this.txtCodigoCentroCustoParticipante1.Name = "txtCodigoCentroCustoParticipante1";
            this.txtCodigoCentroCustoParticipante1.Size = new System.Drawing.Size(124, 20);
            this.txtCodigoCentroCustoParticipante1.TabIndex = 56;
            this.txtCodigoCentroCustoParticipante1.Text = "100";
            // 
            // txtTelefonesParticipante1
            // 
            this.txtTelefonesParticipante1.Location = new System.Drawing.Point(103, 148);
            this.txtTelefonesParticipante1.Name = "txtTelefonesParticipante1";
            this.txtTelefonesParticipante1.Size = new System.Drawing.Size(189, 20);
            this.txtTelefonesParticipante1.TabIndex = 55;
            this.txtTelefonesParticipante1.Text = "(22) 1234-1234 (22) 9982-0123";
            // 
            // txtPassaporteParticipante1
            // 
            this.txtPassaporteParticipante1.Location = new System.Drawing.Point(317, 122);
            this.txtPassaporteParticipante1.Name = "txtPassaporteParticipante1";
            this.txtPassaporteParticipante1.Size = new System.Drawing.Size(128, 20);
            this.txtPassaporteParticipante1.TabIndex = 54;
            // 
            // txtCPFParticipante1
            // 
            this.txtCPFParticipante1.Location = new System.Drawing.Point(104, 122);
            this.txtCPFParticipante1.Name = "txtCPFParticipante1";
            this.txtCPFParticipante1.Size = new System.Drawing.Size(128, 20);
            this.txtCPFParticipante1.TabIndex = 53;
            this.txtCPFParticipante1.Text = "111.222.333-44";
            // 
            // txtIdentidadeParticipante1
            // 
            this.txtIdentidadeParticipante1.Location = new System.Drawing.Point(317, 96);
            this.txtIdentidadeParticipante1.Name = "txtIdentidadeParticipante1";
            this.txtIdentidadeParticipante1.Size = new System.Drawing.Size(128, 20);
            this.txtIdentidadeParticipante1.TabIndex = 52;
            this.txtIdentidadeParticipante1.Text = "222213134";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(57, 210);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(35, 13);
            this.label50.TabIndex = 51;
            this.label50.Text = "E-mail";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(14, 184);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(83, 13);
            this.label49.TabIndex = 50;
            this.label49.Text = "Centro de Custo";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(42, 154);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(54, 13);
            this.label48.TabIndex = 49;
            this.label48.Text = "Telefones";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(251, 126);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(60, 13);
            this.label47.TabIndex = 48;
            this.label47.Text = "Passaporte";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(70, 125);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(23, 13);
            this.label46.TabIndex = 47;
            this.label46.Text = "Cpf";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(248, 97);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(57, 13);
            this.label45.TabIndex = 46;
            this.label45.Text = "Identidade";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(66, 103);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(31, 13);
            this.label44.TabIndex = 45;
            this.label44.Text = "Sexo";
            // 
            // txtComentarioNecessidade1
            // 
            this.txtComentarioNecessidade1.Location = new System.Drawing.Point(85, 552);
            this.txtComentarioNecessidade1.Multiline = true;
            this.txtComentarioNecessidade1.Name = "txtComentarioNecessidade1";
            this.txtComentarioNecessidade1.Size = new System.Drawing.Size(375, 35);
            this.txtComentarioNecessidade1.TabIndex = 44;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 555);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "Comentários";
            // 
            // txtNomeParticipante1
            // 
            this.txtNomeParticipante1.Location = new System.Drawing.Point(103, 71);
            this.txtNomeParticipante1.Name = "txtNomeParticipante1";
            this.txtNomeParticipante1.Size = new System.Drawing.Size(247, 20);
            this.txtNomeParticipante1.TabIndex = 41;
            this.txtNomeParticipante1.Text = "João da Silva";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(62, 75);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(35, 13);
            this.label61.TabIndex = 42;
            this.label61.Text = "Nome";
            // 
            // txtDrakeIDNecessidade1
            // 
            this.txtDrakeIDNecessidade1.Enabled = false;
            this.txtDrakeIDNecessidade1.Location = new System.Drawing.Point(399, 16);
            this.txtDrakeIDNecessidade1.Name = "txtDrakeIDNecessidade1";
            this.txtDrakeIDNecessidade1.Size = new System.Drawing.Size(61, 20);
            this.txtDrakeIDNecessidade1.TabIndex = 39;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(263, 19);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(130, 13);
            this.label51.TabIndex = 40;
            this.label51.Text = "Drake ID da Necessidade";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtIATADestino1);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.txtNumeroDestino1);
            this.groupBox5.Controls.Add(this.txtCEPDestino1);
            this.groupBox5.Controls.Add(this.label52);
            this.groupBox5.Controls.Add(this.txtLongitudeDestino1);
            this.groupBox5.Controls.Add(this.txtLatitudeDestino1);
            this.groupBox5.Controls.Add(this.label53);
            this.groupBox5.Controls.Add(this.label54);
            this.groupBox5.Controls.Add(this.txtPaisDestino1);
            this.groupBox5.Controls.Add(this.txtEstadoDestino1);
            this.groupBox5.Controls.Add(this.label55);
            this.groupBox5.Controls.Add(this.label56);
            this.groupBox5.Controls.Add(this.txtCidadeDestino1);
            this.groupBox5.Controls.Add(this.label57);
            this.groupBox5.Controls.Add(this.txtBairroDestino1);
            this.groupBox5.Controls.Add(this.label58);
            this.groupBox5.Controls.Add(this.txtComplementoDestino1);
            this.groupBox5.Controls.Add(this.label59);
            this.groupBox5.Controls.Add(this.label60);
            this.groupBox5.Controls.Add(this.txtRuaDestino1);
            this.groupBox5.Location = new System.Drawing.Point(246, 235);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(214, 308);
            this.groupBox5.TabIndex = 40;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Destino";
            // 
            // txtIATADestino1
            // 
            this.txtIATADestino1.Location = new System.Drawing.Point(50, 274);
            this.txtIATADestino1.Name = "txtIATADestino1";
            this.txtIATADestino1.Size = new System.Drawing.Size(43, 20);
            this.txtIATADestino1.TabIndex = 34;
            this.txtIATADestino1.Text = "SDU";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 277);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "IATA";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(2, 39);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 13);
            this.label17.TabIndex = 32;
            this.label17.Text = "Número";
            // 
            // txtNumeroDestino1
            // 
            this.txtNumeroDestino1.Location = new System.Drawing.Point(50, 39);
            this.txtNumeroDestino1.Name = "txtNumeroDestino1";
            this.txtNumeroDestino1.Size = new System.Drawing.Size(140, 20);
            this.txtNumeroDestino1.TabIndex = 32;
            this.txtNumeroDestino1.Text = "124";
            // 
            // txtCEPDestino1
            // 
            this.txtCEPDestino1.Location = new System.Drawing.Point(50, 247);
            this.txtCEPDestino1.Name = "txtCEPDestino1";
            this.txtCEPDestino1.Size = new System.Drawing.Size(104, 20);
            this.txtCEPDestino1.TabIndex = 31;
            this.txtCEPDestino1.Text = "27910000";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(18, 253);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(28, 13);
            this.label52.TabIndex = 26;
            this.label52.Text = "CEP";
            // 
            // txtLongitudeDestino1
            // 
            this.txtLongitudeDestino1.Location = new System.Drawing.Point(50, 221);
            this.txtLongitudeDestino1.Name = "txtLongitudeDestino1";
            this.txtLongitudeDestino1.Size = new System.Drawing.Size(140, 20);
            this.txtLongitudeDestino1.TabIndex = 25;
            this.txtLongitudeDestino1.Text = "-3242523";
            // 
            // txtLatitudeDestino1
            // 
            this.txtLatitudeDestino1.Location = new System.Drawing.Point(50, 195);
            this.txtLatitudeDestino1.Name = "txtLatitudeDestino1";
            this.txtLatitudeDestino1.Size = new System.Drawing.Size(140, 20);
            this.txtLatitudeDestino1.TabIndex = 24;
            this.txtLatitudeDestino1.Text = "54336345";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(12, 224);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 13);
            this.label53.TabIndex = 23;
            this.label53.Text = "Long.";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(21, 197);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(25, 13);
            this.label54.TabIndex = 22;
            this.label54.Text = "Lat.";
            // 
            // txtPaisDestino1
            // 
            this.txtPaisDestino1.Location = new System.Drawing.Point(50, 169);
            this.txtPaisDestino1.Name = "txtPaisDestino1";
            this.txtPaisDestino1.Size = new System.Drawing.Size(140, 20);
            this.txtPaisDestino1.TabIndex = 21;
            this.txtPaisDestino1.Text = "Brasil";
            // 
            // txtEstadoDestino1
            // 
            this.txtEstadoDestino1.Location = new System.Drawing.Point(50, 143);
            this.txtEstadoDestino1.Name = "txtEstadoDestino1";
            this.txtEstadoDestino1.Size = new System.Drawing.Size(33, 20);
            this.txtEstadoDestino1.TabIndex = 20;
            this.txtEstadoDestino1.Text = "RJ";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(17, 172);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(29, 13);
            this.label55.TabIndex = 19;
            this.label55.Text = "País";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 146);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(40, 13);
            this.label56.TabIndex = 18;
            this.label56.Text = "Estado";
            // 
            // txtCidadeDestino1
            // 
            this.txtCidadeDestino1.Location = new System.Drawing.Point(50, 117);
            this.txtCidadeDestino1.Name = "txtCidadeDestino1";
            this.txtCidadeDestino1.Size = new System.Drawing.Size(140, 20);
            this.txtCidadeDestino1.TabIndex = 17;
            this.txtCidadeDestino1.Text = "Rio de Janeiro";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(6, 120);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(40, 13);
            this.label57.TabIndex = 16;
            this.label57.Text = "Cidade";
            // 
            // txtBairroDestino1
            // 
            this.txtBairroDestino1.Location = new System.Drawing.Point(50, 91);
            this.txtBairroDestino1.Name = "txtBairroDestino1";
            this.txtBairroDestino1.Size = new System.Drawing.Size(140, 20);
            this.txtBairroDestino1.TabIndex = 15;
            this.txtBairroDestino1.Text = "Centro";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(12, 94);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(34, 13);
            this.label58.TabIndex = 14;
            this.label58.Text = "Bairro";
            // 
            // txtComplementoDestino1
            // 
            this.txtComplementoDestino1.Location = new System.Drawing.Point(50, 65);
            this.txtComplementoDestino1.Name = "txtComplementoDestino1";
            this.txtComplementoDestino1.Size = new System.Drawing.Size(140, 20);
            this.txtComplementoDestino1.TabIndex = 13;
            this.txtComplementoDestino1.Text = "S/N";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(7, 68);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(39, 13);
            this.label59.TabIndex = 12;
            this.label59.Text = "Compl.";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(19, 16);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(27, 13);
            this.label60.TabIndex = 11;
            this.label60.Text = "Rua";
            // 
            // txtRuaDestino1
            // 
            this.txtRuaDestino1.Location = new System.Drawing.Point(50, 13);
            this.txtRuaDestino1.Name = "txtRuaDestino1";
            this.txtRuaDestino1.Size = new System.Drawing.Size(140, 20);
            this.txtRuaDestino1.TabIndex = 10;
            this.txtRuaDestino1.Text = "Rua 1";
            // 
            // txtOrdemChegadaParticipante1
            // 
            this.txtOrdemChegadaParticipante1.Location = new System.Drawing.Point(251, 47);
            this.txtOrdemChegadaParticipante1.Name = "txtOrdemChegadaParticipante1";
            this.txtOrdemChegadaParticipante1.Size = new System.Drawing.Size(43, 20);
            this.txtOrdemChegadaParticipante1.TabIndex = 7;
            this.txtOrdemChegadaParticipante1.Text = "1";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(162, 51);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(83, 13);
            this.label31.TabIndex = 6;
            this.label31.Text = "Ordem chegada";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(29, 48);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(68, 13);
            this.label30.TabIndex = 5;
            this.label30.Text = "Ordem saída";
            // 
            // txtDrakeIDParticipante1
            // 
            this.txtDrakeIDParticipante1.Location = new System.Drawing.Point(103, 19);
            this.txtDrakeIDParticipante1.Name = "txtDrakeIDParticipante1";
            this.txtDrakeIDParticipante1.Size = new System.Drawing.Size(100, 20);
            this.txtDrakeIDParticipante1.TabIndex = 2;
            this.txtDrakeIDParticipante1.Text = "238";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtIATAOrigem);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.txtNumeroOrigem);
            this.groupBox2.Controls.Add(this.txtCEPOrigem);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.txtLongitudeOrigem);
            this.groupBox2.Controls.Add(this.txtLatitudeOrigem);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.txtPaisOrigem);
            this.groupBox2.Controls.Add(this.txtEstadoOrigem);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtCidadeOrigem);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtBairroOrigem);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtComplementoOrigem);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtRuaOrigem);
            this.groupBox2.Location = new System.Drawing.Point(18, 235);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(214, 308);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Origem";
            // 
            // txtIATAOrigem
            // 
            this.txtIATAOrigem.Location = new System.Drawing.Point(50, 274);
            this.txtIATAOrigem.Name = "txtIATAOrigem";
            this.txtIATAOrigem.Size = new System.Drawing.Size(43, 20);
            this.txtIATAOrigem.TabIndex = 34;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(15, 277);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(31, 13);
            this.label40.TabIndex = 33;
            this.label40.Text = "IATA";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(2, 39);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(44, 13);
            this.label38.TabIndex = 32;
            this.label38.Text = "Número";
            // 
            // txtNumeroOrigem
            // 
            this.txtNumeroOrigem.Location = new System.Drawing.Point(50, 39);
            this.txtNumeroOrigem.Name = "txtNumeroOrigem";
            this.txtNumeroOrigem.Size = new System.Drawing.Size(140, 20);
            this.txtNumeroOrigem.TabIndex = 32;
            this.txtNumeroOrigem.Text = "46";
            // 
            // txtCEPOrigem
            // 
            this.txtCEPOrigem.Location = new System.Drawing.Point(50, 247);
            this.txtCEPOrigem.Name = "txtCEPOrigem";
            this.txtCEPOrigem.Size = new System.Drawing.Size(104, 20);
            this.txtCEPOrigem.TabIndex = 31;
            this.txtCEPOrigem.Text = "27910000";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(18, 253);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 13);
            this.label36.TabIndex = 26;
            this.label36.Text = "CEP";
            // 
            // txtLongitudeOrigem
            // 
            this.txtLongitudeOrigem.Location = new System.Drawing.Point(50, 221);
            this.txtLongitudeOrigem.Name = "txtLongitudeOrigem";
            this.txtLongitudeOrigem.Size = new System.Drawing.Size(140, 20);
            this.txtLongitudeOrigem.TabIndex = 25;
            this.txtLongitudeOrigem.Text = "-123534";
            // 
            // txtLatitudeOrigem
            // 
            this.txtLatitudeOrigem.Location = new System.Drawing.Point(50, 195);
            this.txtLatitudeOrigem.Name = "txtLatitudeOrigem";
            this.txtLatitudeOrigem.Size = new System.Drawing.Size(140, 20);
            this.txtLatitudeOrigem.TabIndex = 24;
            this.txtLatitudeOrigem.Text = "123444444";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(12, 224);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(34, 13);
            this.label25.TabIndex = 23;
            this.label25.Text = "Long.";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(21, 197);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(25, 13);
            this.label24.TabIndex = 22;
            this.label24.Text = "Lat.";
            // 
            // txtPaisOrigem
            // 
            this.txtPaisOrigem.Location = new System.Drawing.Point(50, 169);
            this.txtPaisOrigem.Name = "txtPaisOrigem";
            this.txtPaisOrigem.Size = new System.Drawing.Size(140, 20);
            this.txtPaisOrigem.TabIndex = 21;
            this.txtPaisOrigem.Text = "Brasil";
            // 
            // txtEstadoOrigem
            // 
            this.txtEstadoOrigem.Location = new System.Drawing.Point(50, 143);
            this.txtEstadoOrigem.Name = "txtEstadoOrigem";
            this.txtEstadoOrigem.Size = new System.Drawing.Size(33, 20);
            this.txtEstadoOrigem.TabIndex = 20;
            this.txtEstadoOrigem.Text = "RJ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 172);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "País";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 146);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Estado";
            // 
            // txtCidadeOrigem
            // 
            this.txtCidadeOrigem.Location = new System.Drawing.Point(50, 117);
            this.txtCidadeOrigem.Name = "txtCidadeOrigem";
            this.txtCidadeOrigem.Size = new System.Drawing.Size(140, 20);
            this.txtCidadeOrigem.TabIndex = 17;
            this.txtCidadeOrigem.Text = "Macaé";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 120);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Cidade";
            // 
            // txtBairroOrigem
            // 
            this.txtBairroOrigem.Location = new System.Drawing.Point(50, 91);
            this.txtBairroOrigem.Name = "txtBairroOrigem";
            this.txtBairroOrigem.Size = new System.Drawing.Size(140, 20);
            this.txtBairroOrigem.TabIndex = 15;
            this.txtBairroOrigem.Text = "CEHAB";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 94);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Bairro";
            // 
            // txtComplementoOrigem
            // 
            this.txtComplementoOrigem.Location = new System.Drawing.Point(50, 65);
            this.txtComplementoOrigem.Name = "txtComplementoOrigem";
            this.txtComplementoOrigem.Size = new System.Drawing.Size(140, 20);
            this.txtComplementoOrigem.TabIndex = 13;
            this.txtComplementoOrigem.Text = "Casa A";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Compl.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Rua";
            // 
            // txtRuaOrigem
            // 
            this.txtRuaOrigem.Location = new System.Drawing.Point(50, 13);
            this.txtRuaOrigem.Name = "txtRuaOrigem";
            this.txtRuaOrigem.Size = new System.Drawing.Size(140, 20);
            this.txtRuaOrigem.TabIndex = 10;
            this.txtRuaOrigem.Text = "Pereira Neto";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(57, 26);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(40, 13);
            this.label29.TabIndex = 4;
            this.label29.Text = "Código";
            // 
            // txtOrdemSaidaParticipante1
            // 
            this.txtOrdemSaidaParticipante1.Location = new System.Drawing.Point(103, 45);
            this.txtOrdemSaidaParticipante1.Name = "txtOrdemSaidaParticipante1";
            this.txtOrdemSaidaParticipante1.Size = new System.Drawing.Size(43, 20);
            this.txtOrdemSaidaParticipante1.TabIndex = 3;
            this.txtOrdemSaidaParticipante1.Text = "1";
            // 
            // groupBoxParticipante2
            // 
            this.groupBoxParticipante2.Controls.Add(this.label87);
            this.groupBoxParticipante2.Controls.Add(this.label82);
            this.groupBoxParticipante2.Controls.Add(this.txtNomeCentroCustoNecessidade2);
            this.groupBoxParticipante2.Controls.Add(this.txtNomeCentroCustoParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.txtCodigoCentroCustoNecessidade2);
            this.groupBoxParticipante2.Controls.Add(this.label88);
            this.groupBoxParticipante2.Controls.Add(this.txtComentarioNecessidade2);
            this.groupBoxParticipante2.Controls.Add(this.label5);
            this.groupBoxParticipante2.Controls.Add(this.comboBoxSexoParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.txtEmailParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.txtCodigoCentroCustoParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.txtTelefonesParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.txtPassaporteParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.txtCPFParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.txtIdentidadeParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.label75);
            this.groupBoxParticipante2.Controls.Add(this.label76);
            this.groupBoxParticipante2.Controls.Add(this.label77);
            this.groupBoxParticipante2.Controls.Add(this.label78);
            this.groupBoxParticipante2.Controls.Add(this.label79);
            this.groupBoxParticipante2.Controls.Add(this.label80);
            this.groupBoxParticipante2.Controls.Add(this.label81);
            this.groupBoxParticipante2.Controls.Add(this.txtNomeParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.label62);
            this.groupBoxParticipante2.Controls.Add(this.groupBox6);
            this.groupBoxParticipante2.Controls.Add(this.txtDrakeIDNecessidade2);
            this.groupBoxParticipante2.Controls.Add(this.txtOrdemChegadaParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.label63);
            this.groupBoxParticipante2.Controls.Add(this.label32);
            this.groupBoxParticipante2.Controls.Add(this.label34);
            this.groupBoxParticipante2.Controls.Add(this.txtDrakeIDParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.label35);
            this.groupBoxParticipante2.Controls.Add(this.txtOrdemSaidaParticipante2);
            this.groupBoxParticipante2.Controls.Add(this.groupBox10);
            this.groupBoxParticipante2.Enabled = false;
            this.groupBoxParticipante2.Location = new System.Drawing.Point(524, 25);
            this.groupBoxParticipante2.Name = "groupBoxParticipante2";
            this.groupBoxParticipante2.Size = new System.Drawing.Size(464, 649);
            this.groupBoxParticipante2.TabIndex = 37;
            this.groupBoxParticipante2.TabStop = false;
            this.groupBoxParticipante2.Text = "Participante #2 (Opcional)";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(293, 616);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(35, 13);
            this.label87.TabIndex = 68;
            this.label87.Text = "Nome";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(268, 192);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(35, 13);
            this.label82.TabIndex = 75;
            this.label82.Text = "Nome";
            // 
            // txtNomeCentroCustoNecessidade2
            // 
            this.txtNomeCentroCustoNecessidade2.Location = new System.Drawing.Point(334, 612);
            this.txtNomeCentroCustoNecessidade2.Name = "txtNomeCentroCustoNecessidade2";
            this.txtNomeCentroCustoNecessidade2.Size = new System.Drawing.Size(109, 20);
            this.txtNomeCentroCustoNecessidade2.TabIndex = 67;
            this.txtNomeCentroCustoNecessidade2.Text = "BASE";
            // 
            // txtNomeCentroCustoParticipante2
            // 
            this.txtNomeCentroCustoParticipante2.Location = new System.Drawing.Point(309, 188);
            this.txtNomeCentroCustoParticipante2.Name = "txtNomeCentroCustoParticipante2";
            this.txtNomeCentroCustoParticipante2.Size = new System.Drawing.Size(109, 20);
            this.txtNomeCentroCustoParticipante2.TabIndex = 74;
            this.txtNomeCentroCustoParticipante2.Text = "OUTRO";
            // 
            // txtCodigoCentroCustoNecessidade2
            // 
            this.txtCodigoCentroCustoNecessidade2.Location = new System.Drawing.Point(102, 612);
            this.txtCodigoCentroCustoNecessidade2.Name = "txtCodigoCentroCustoNecessidade2";
            this.txtCodigoCentroCustoNecessidade2.Size = new System.Drawing.Size(124, 20);
            this.txtCodigoCentroCustoNecessidade2.TabIndex = 66;
            this.txtCodigoCentroCustoNecessidade2.Text = "100";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(12, 616);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(83, 13);
            this.label88.TabIndex = 65;
            this.label88.Text = "Centro de Custo";
            // 
            // txtComentarioNecessidade2
            // 
            this.txtComentarioNecessidade2.Location = new System.Drawing.Point(82, 552);
            this.txtComentarioNecessidade2.Multiline = true;
            this.txtComentarioNecessidade2.Name = "txtComentarioNecessidade2";
            this.txtComentarioNecessidade2.Size = new System.Drawing.Size(360, 35);
            this.txtComentarioNecessidade2.TabIndex = 46;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 555);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Comentários";
            // 
            // comboBoxSexoParticipante2
            // 
            this.comboBoxSexoParticipante2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSexoParticipante2.FormattingEnabled = true;
            this.comboBoxSexoParticipante2.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.comboBoxSexoParticipante2.Location = new System.Drawing.Point(102, 103);
            this.comboBoxSexoParticipante2.Name = "comboBoxSexoParticipante2";
            this.comboBoxSexoParticipante2.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSexoParticipante2.TabIndex = 73;
            // 
            // txtEmailParticipante2
            // 
            this.txtEmailParticipante2.Location = new System.Drawing.Point(103, 216);
            this.txtEmailParticipante2.Name = "txtEmailParticipante2";
            this.txtEmailParticipante2.Size = new System.Drawing.Size(189, 20);
            this.txtEmailParticipante2.TabIndex = 72;
            this.txtEmailParticipante2.Text = "maria@bonita.com";
            // 
            // txtCodigoCentroCustoParticipante2
            // 
            this.txtCodigoCentroCustoParticipante2.Location = new System.Drawing.Point(103, 188);
            this.txtCodigoCentroCustoParticipante2.Name = "txtCodigoCentroCustoParticipante2";
            this.txtCodigoCentroCustoParticipante2.Size = new System.Drawing.Size(124, 20);
            this.txtCodigoCentroCustoParticipante2.TabIndex = 71;
            this.txtCodigoCentroCustoParticipante2.Text = "200";
            // 
            // txtTelefonesParticipante2
            // 
            this.txtTelefonesParticipante2.Location = new System.Drawing.Point(102, 156);
            this.txtTelefonesParticipante2.Name = "txtTelefonesParticipante2";
            this.txtTelefonesParticipante2.Size = new System.Drawing.Size(189, 20);
            this.txtTelefonesParticipante2.TabIndex = 70;
            this.txtTelefonesParticipante2.Text = "(22) 1234-4554";
            // 
            // txtPassaporteParticipante2
            // 
            this.txtPassaporteParticipante2.Location = new System.Drawing.Point(314, 130);
            this.txtPassaporteParticipante2.Name = "txtPassaporteParticipante2";
            this.txtPassaporteParticipante2.Size = new System.Drawing.Size(128, 20);
            this.txtPassaporteParticipante2.TabIndex = 69;
            this.txtPassaporteParticipante2.Text = "AAA1234";
            // 
            // txtCPFParticipante2
            // 
            this.txtCPFParticipante2.Location = new System.Drawing.Point(102, 130);
            this.txtCPFParticipante2.Name = "txtCPFParticipante2";
            this.txtCPFParticipante2.Size = new System.Drawing.Size(128, 20);
            this.txtCPFParticipante2.TabIndex = 68;
            // 
            // txtIdentidadeParticipante2
            // 
            this.txtIdentidadeParticipante2.Location = new System.Drawing.Point(300, 103);
            this.txtIdentidadeParticipante2.Name = "txtIdentidadeParticipante2";
            this.txtIdentidadeParticipante2.Size = new System.Drawing.Size(128, 20);
            this.txtIdentidadeParticipante2.TabIndex = 67;
            this.txtIdentidadeParticipante2.Text = "XX12345";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(56, 218);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(35, 13);
            this.label75.TabIndex = 66;
            this.label75.Text = "E-mail";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(13, 192);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(83, 13);
            this.label76.TabIndex = 65;
            this.label76.Text = "Centro de Custo";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(41, 162);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(54, 13);
            this.label77.TabIndex = 64;
            this.label77.Text = "Telefones";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(248, 134);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(60, 13);
            this.label78.TabIndex = 63;
            this.label78.Text = "Passaporte";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(68, 133);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(23, 13);
            this.label79.TabIndex = 62;
            this.label79.Text = "Cpf";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(239, 105);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(57, 13);
            this.label80.TabIndex = 61;
            this.label80.Text = "Identidade";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(65, 111);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(31, 13);
            this.label81.TabIndex = 60;
            this.label81.Text = "Sexo";
            // 
            // txtNomeParticipante2
            // 
            this.txtNomeParticipante2.Location = new System.Drawing.Point(102, 76);
            this.txtNomeParticipante2.Name = "txtNomeParticipante2";
            this.txtNomeParticipante2.Size = new System.Drawing.Size(235, 20);
            this.txtNomeParticipante2.TabIndex = 45;
            this.txtNomeParticipante2.Text = "Maria Bonita";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(61, 80);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(35, 13);
            this.label62.TabIndex = 46;
            this.label62.Text = "Nome";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtIATADestino2);
            this.groupBox6.Controls.Add(this.label64);
            this.groupBox6.Controls.Add(this.label65);
            this.groupBox6.Controls.Add(this.txtNumeroDestino2);
            this.groupBox6.Controls.Add(this.txtCEPDestino2);
            this.groupBox6.Controls.Add(this.label66);
            this.groupBox6.Controls.Add(this.txtLongitudeDestino2);
            this.groupBox6.Controls.Add(this.txtLatitudeDestino2);
            this.groupBox6.Controls.Add(this.label67);
            this.groupBox6.Controls.Add(this.label68);
            this.groupBox6.Controls.Add(this.txtPaisDestino2);
            this.groupBox6.Controls.Add(this.txtEstadoDestino2);
            this.groupBox6.Controls.Add(this.label69);
            this.groupBox6.Controls.Add(this.label70);
            this.groupBox6.Controls.Add(this.txtCidadeDestino2);
            this.groupBox6.Controls.Add(this.label71);
            this.groupBox6.Controls.Add(this.txtBairroDestino2);
            this.groupBox6.Controls.Add(this.label72);
            this.groupBox6.Controls.Add(this.txtComplementoDestino2);
            this.groupBox6.Controls.Add(this.label73);
            this.groupBox6.Controls.Add(this.label74);
            this.groupBox6.Controls.Add(this.txtRuaDestino2);
            this.groupBox6.Location = new System.Drawing.Point(228, 242);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(214, 308);
            this.groupBox6.TabIndex = 47;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Destino";
            // 
            // txtIATADestino2
            // 
            this.txtIATADestino2.Location = new System.Drawing.Point(50, 274);
            this.txtIATADestino2.Name = "txtIATADestino2";
            this.txtIATADestino2.Size = new System.Drawing.Size(43, 20);
            this.txtIATADestino2.TabIndex = 34;
            this.txtIATADestino2.Text = "SDU";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(15, 277);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(31, 13);
            this.label64.TabIndex = 33;
            this.label64.Text = "IATA";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(2, 39);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(44, 13);
            this.label65.TabIndex = 32;
            this.label65.Text = "Número";
            // 
            // txtNumeroDestino2
            // 
            this.txtNumeroDestino2.Location = new System.Drawing.Point(50, 39);
            this.txtNumeroDestino2.Name = "txtNumeroDestino2";
            this.txtNumeroDestino2.Size = new System.Drawing.Size(140, 20);
            this.txtNumeroDestino2.TabIndex = 32;
            this.txtNumeroDestino2.Text = "1231";
            // 
            // txtCEPDestino2
            // 
            this.txtCEPDestino2.Location = new System.Drawing.Point(50, 247);
            this.txtCEPDestino2.Name = "txtCEPDestino2";
            this.txtCEPDestino2.Size = new System.Drawing.Size(104, 20);
            this.txtCEPDestino2.TabIndex = 31;
            this.txtCEPDestino2.Text = "27910000";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(18, 253);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(28, 13);
            this.label66.TabIndex = 26;
            this.label66.Text = "CEP";
            // 
            // txtLongitudeDestino2
            // 
            this.txtLongitudeDestino2.Location = new System.Drawing.Point(50, 221);
            this.txtLongitudeDestino2.Name = "txtLongitudeDestino2";
            this.txtLongitudeDestino2.Size = new System.Drawing.Size(140, 20);
            this.txtLongitudeDestino2.TabIndex = 25;
            this.txtLongitudeDestino2.Text = "-3242523";
            // 
            // txtLatitudeDestino2
            // 
            this.txtLatitudeDestino2.Location = new System.Drawing.Point(50, 195);
            this.txtLatitudeDestino2.Name = "txtLatitudeDestino2";
            this.txtLatitudeDestino2.Size = new System.Drawing.Size(140, 20);
            this.txtLatitudeDestino2.TabIndex = 24;
            this.txtLatitudeDestino2.Text = "54336345";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(12, 224);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(34, 13);
            this.label67.TabIndex = 23;
            this.label67.Text = "Long.";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(21, 197);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(25, 13);
            this.label68.TabIndex = 22;
            this.label68.Text = "Lat.";
            // 
            // txtPaisDestino2
            // 
            this.txtPaisDestino2.Location = new System.Drawing.Point(50, 169);
            this.txtPaisDestino2.Name = "txtPaisDestino2";
            this.txtPaisDestino2.Size = new System.Drawing.Size(140, 20);
            this.txtPaisDestino2.TabIndex = 21;
            this.txtPaisDestino2.Text = "Brasil";
            // 
            // txtEstadoDestino2
            // 
            this.txtEstadoDestino2.Location = new System.Drawing.Point(50, 143);
            this.txtEstadoDestino2.Name = "txtEstadoDestino2";
            this.txtEstadoDestino2.Size = new System.Drawing.Size(33, 20);
            this.txtEstadoDestino2.TabIndex = 20;
            this.txtEstadoDestino2.Text = "RJ";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(17, 172);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(29, 13);
            this.label69.TabIndex = 19;
            this.label69.Text = "País";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(6, 146);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(40, 13);
            this.label70.TabIndex = 18;
            this.label70.Text = "Estado";
            // 
            // txtCidadeDestino2
            // 
            this.txtCidadeDestino2.Location = new System.Drawing.Point(50, 117);
            this.txtCidadeDestino2.Name = "txtCidadeDestino2";
            this.txtCidadeDestino2.Size = new System.Drawing.Size(140, 20);
            this.txtCidadeDestino2.TabIndex = 17;
            this.txtCidadeDestino2.Text = "Rio de Janeiro";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(6, 120);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(40, 13);
            this.label71.TabIndex = 16;
            this.label71.Text = "Cidade";
            // 
            // txtBairroDestino2
            // 
            this.txtBairroDestino2.Location = new System.Drawing.Point(50, 91);
            this.txtBairroDestino2.Name = "txtBairroDestino2";
            this.txtBairroDestino2.Size = new System.Drawing.Size(140, 20);
            this.txtBairroDestino2.TabIndex = 15;
            this.txtBairroDestino2.Text = "Centro";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(12, 94);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(34, 13);
            this.label72.TabIndex = 14;
            this.label72.Text = "Bairro";
            // 
            // txtComplementoDestino2
            // 
            this.txtComplementoDestino2.Location = new System.Drawing.Point(50, 65);
            this.txtComplementoDestino2.Name = "txtComplementoDestino2";
            this.txtComplementoDestino2.Size = new System.Drawing.Size(140, 20);
            this.txtComplementoDestino2.TabIndex = 13;
            this.txtComplementoDestino2.Text = "S/N";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(7, 68);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(39, 13);
            this.label73.TabIndex = 12;
            this.label73.Text = "Compl.";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(19, 16);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(27, 13);
            this.label74.TabIndex = 11;
            this.label74.Text = "Rua";
            // 
            // txtRuaDestino2
            // 
            this.txtRuaDestino2.Location = new System.Drawing.Point(50, 13);
            this.txtRuaDestino2.Name = "txtRuaDestino2";
            this.txtRuaDestino2.Size = new System.Drawing.Size(140, 20);
            this.txtRuaDestino2.TabIndex = 10;
            this.txtRuaDestino2.Text = "Rua 2";
            // 
            // txtDrakeIDNecessidade2
            // 
            this.txtDrakeIDNecessidade2.Location = new System.Drawing.Point(361, 20);
            this.txtDrakeIDNecessidade2.Name = "txtDrakeIDNecessidade2";
            this.txtDrakeIDNecessidade2.Size = new System.Drawing.Size(86, 20);
            this.txtDrakeIDNecessidade2.TabIndex = 43;
            // 
            // txtOrdemChegadaParticipante2
            // 
            this.txtOrdemChegadaParticipante2.Location = new System.Drawing.Point(280, 50);
            this.txtOrdemChegadaParticipante2.Name = "txtOrdemChegadaParticipante2";
            this.txtOrdemChegadaParticipante2.Size = new System.Drawing.Size(43, 20);
            this.txtOrdemChegadaParticipante2.TabIndex = 7;
            this.txtOrdemChegadaParticipante2.Text = "2";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(225, 23);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(130, 13);
            this.label63.TabIndex = 44;
            this.label63.Text = "Drake ID da Necessidade";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(191, 54);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(83, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "Ordem chegada";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(28, 51);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(68, 13);
            this.label34.TabIndex = 5;
            this.label34.Text = "Ordem saída";
            // 
            // txtDrakeIDParticipante2
            // 
            this.txtDrakeIDParticipante2.Location = new System.Drawing.Point(102, 22);
            this.txtDrakeIDParticipante2.Name = "txtDrakeIDParticipante2";
            this.txtDrakeIDParticipante2.Size = new System.Drawing.Size(100, 20);
            this.txtDrakeIDParticipante2.TabIndex = 2;
            this.txtDrakeIDParticipante2.Text = "225";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(39, 25);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(47, 13);
            this.label35.TabIndex = 4;
            this.label35.Text = "DrakeID";
            // 
            // txtOrdemSaidaParticipante2
            // 
            this.txtOrdemSaidaParticipante2.Location = new System.Drawing.Point(102, 48);
            this.txtOrdemSaidaParticipante2.Name = "txtOrdemSaidaParticipante2";
            this.txtOrdemSaidaParticipante2.Size = new System.Drawing.Size(43, 20);
            this.txtOrdemSaidaParticipante2.TabIndex = 3;
            this.txtOrdemSaidaParticipante2.Text = "2";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.txtIATAOrigem2);
            this.groupBox10.Controls.Add(this.label41);
            this.groupBox10.Controls.Add(this.label39);
            this.groupBox10.Controls.Add(this.txtCEPOrigem2);
            this.groupBox10.Controls.Add(this.txtNumeroOrigem2);
            this.groupBox10.Controls.Add(this.label37);
            this.groupBox10.Controls.Add(this.txtLongitudeOrigem2);
            this.groupBox10.Controls.Add(this.txtPaisOrigem2);
            this.groupBox10.Controls.Add(this.txtLatitudeOrigem2);
            this.groupBox10.Controls.Add(this.label26);
            this.groupBox10.Controls.Add(this.txtEstadoOrigem2);
            this.groupBox10.Controls.Add(this.label27);
            this.groupBox10.Controls.Add(this.label18);
            this.groupBox10.Controls.Add(this.label19);
            this.groupBox10.Controls.Add(this.txtCidadeOrigem2);
            this.groupBox10.Controls.Add(this.label20);
            this.groupBox10.Controls.Add(this.txtBairroOrigem2);
            this.groupBox10.Controls.Add(this.label21);
            this.groupBox10.Controls.Add(this.txtComplementoOrigem2);
            this.groupBox10.Controls.Add(this.label22);
            this.groupBox10.Controls.Add(this.label23);
            this.groupBox10.Controls.Add(this.txtRuaOrigem2);
            this.groupBox10.Location = new System.Drawing.Point(6, 242);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(200, 308);
            this.groupBox10.TabIndex = 13;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Origem";
            // 
            // txtIATAOrigem2
            // 
            this.txtIATAOrigem2.Location = new System.Drawing.Point(54, 274);
            this.txtIATAOrigem2.Name = "txtIATAOrigem2";
            this.txtIATAOrigem2.Size = new System.Drawing.Size(43, 20);
            this.txtIATAOrigem2.TabIndex = 35;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(19, 277);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(31, 13);
            this.label41.TabIndex = 34;
            this.label41.Text = "IATA";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 39);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(44, 13);
            this.label39.TabIndex = 33;
            this.label39.Text = "Número";
            // 
            // txtCEPOrigem2
            // 
            this.txtCEPOrigem2.Location = new System.Drawing.Point(53, 247);
            this.txtCEPOrigem2.Name = "txtCEPOrigem2";
            this.txtCEPOrigem2.Size = new System.Drawing.Size(104, 20);
            this.txtCEPOrigem2.TabIndex = 30;
            this.txtCEPOrigem2.Text = "28152142";
            // 
            // txtNumeroOrigem2
            // 
            this.txtNumeroOrigem2.Location = new System.Drawing.Point(54, 39);
            this.txtNumeroOrigem2.Name = "txtNumeroOrigem2";
            this.txtNumeroOrigem2.Size = new System.Drawing.Size(140, 20);
            this.txtNumeroOrigem2.TabIndex = 34;
            this.txtNumeroOrigem2.Text = "171";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(23, 251);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 13);
            this.label37.TabIndex = 27;
            this.label37.Text = "CEP";
            // 
            // txtLongitudeOrigem2
            // 
            this.txtLongitudeOrigem2.Location = new System.Drawing.Point(53, 221);
            this.txtLongitudeOrigem2.Name = "txtLongitudeOrigem2";
            this.txtLongitudeOrigem2.Size = new System.Drawing.Size(140, 20);
            this.txtLongitudeOrigem2.TabIndex = 29;
            this.txtLongitudeOrigem2.Text = "-12412355";
            // 
            // txtPaisOrigem2
            // 
            this.txtPaisOrigem2.Location = new System.Drawing.Point(54, 169);
            this.txtPaisOrigem2.Name = "txtPaisOrigem2";
            this.txtPaisOrigem2.Size = new System.Drawing.Size(140, 20);
            this.txtPaisOrigem2.TabIndex = 21;
            this.txtPaisOrigem2.Text = "Brasil";
            // 
            // txtLatitudeOrigem2
            // 
            this.txtLatitudeOrigem2.Location = new System.Drawing.Point(53, 195);
            this.txtLatitudeOrigem2.Name = "txtLatitudeOrigem2";
            this.txtLatitudeOrigem2.Size = new System.Drawing.Size(140, 20);
            this.txtLatitudeOrigem2.TabIndex = 28;
            this.txtLatitudeOrigem2.Text = "1231444441";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(15, 224);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(34, 13);
            this.label26.TabIndex = 27;
            this.label26.Text = "Long.";
            // 
            // txtEstadoOrigem2
            // 
            this.txtEstadoOrigem2.Location = new System.Drawing.Point(54, 143);
            this.txtEstadoOrigem2.Name = "txtEstadoOrigem2";
            this.txtEstadoOrigem2.Size = new System.Drawing.Size(33, 20);
            this.txtEstadoOrigem2.TabIndex = 20;
            this.txtEstadoOrigem2.Text = "RJ";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(24, 197);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(25, 13);
            this.label27.TabIndex = 26;
            this.label27.Text = "Lat.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(21, 172);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = "País";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(10, 146);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "Estado";
            // 
            // txtCidadeOrigem2
            // 
            this.txtCidadeOrigem2.Location = new System.Drawing.Point(54, 117);
            this.txtCidadeOrigem2.Name = "txtCidadeOrigem2";
            this.txtCidadeOrigem2.Size = new System.Drawing.Size(140, 20);
            this.txtCidadeOrigem2.TabIndex = 17;
            this.txtCidadeOrigem2.Text = "Unamar";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(10, 120);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "Cidade";
            // 
            // txtBairroOrigem2
            // 
            this.txtBairroOrigem2.Location = new System.Drawing.Point(54, 91);
            this.txtBairroOrigem2.Name = "txtBairroOrigem2";
            this.txtBairroOrigem2.Size = new System.Drawing.Size(140, 20);
            this.txtBairroOrigem2.TabIndex = 15;
            this.txtBairroOrigem2.Text = "Centro";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 94);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 13);
            this.label21.TabIndex = 14;
            this.label21.Text = "Bairro";
            // 
            // txtComplementoOrigem2
            // 
            this.txtComplementoOrigem2.Location = new System.Drawing.Point(54, 65);
            this.txtComplementoOrigem2.Name = "txtComplementoOrigem2";
            this.txtComplementoOrigem2.Size = new System.Drawing.Size(140, 20);
            this.txtComplementoOrigem2.TabIndex = 13;
            this.txtComplementoOrigem2.Text = "N/A";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 68);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(39, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "Compl.";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(19, 16);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(27, 13);
            this.label23.TabIndex = 11;
            this.label23.Text = "Rua";
            // 
            // txtRuaOrigem2
            // 
            this.txtRuaOrigem2.Location = new System.Drawing.Point(53, 13);
            this.txtRuaOrigem2.Name = "txtRuaOrigem2";
            this.txtRuaOrigem2.Size = new System.Drawing.Size(140, 20);
            this.txtRuaOrigem2.TabIndex = 10;
            this.txtRuaOrigem2.Text = "Visconde de Sabugosa";
            // 
            // dtpTermino
            // 
            this.dtpTermino.Location = new System.Drawing.Point(70, 59);
            this.dtpTermino.Name = "dtpTermino";
            this.dtpTermino.Size = new System.Drawing.Size(200, 20);
            this.dtpTermino.TabIndex = 7;
            this.dtpTermino.Value = new System.DateTime(2013, 11, 2, 0, 0, 0, 0);
            // 
            // dtpInicio
            // 
            this.dtpInicio.CustomFormat = "";
            this.dtpInicio.Location = new System.Drawing.Point(70, 29);
            this.dtpInicio.Name = "dtpInicio";
            this.dtpInicio.Size = new System.Drawing.Size(200, 20);
            this.dtpInicio.TabIndex = 6;
            this.dtpInicio.Value = new System.DateTime(2013, 11, 1, 0, 0, 0, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Término";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Início";
            // 
            // btnEnviarSolicitacao
            // 
            this.btnEnviarSolicitacao.Location = new System.Drawing.Point(956, 819);
            this.btnEnviarSolicitacao.Name = "btnEnviarSolicitacao";
            this.btnEnviarSolicitacao.Size = new System.Drawing.Size(75, 23);
            this.btnEnviarSolicitacao.TabIndex = 0;
            this.btnEnviarSolicitacao.Text = "Enviar";
            this.btnEnviarSolicitacao.UseVisualStyleBackColor = true;
            this.btnEnviarSolicitacao.Click += new System.EventHandler(this.btnEnviarSolicitacao_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtChavePrivada);
            this.groupBox1.Controls.Add(this.txtCodigoFornecedor);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.txtCodigoCliente);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtDrakeId);
            this.groupBox1.Controls.Add(this.txtExternoId);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(14, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(905, 108);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados básicos";
            // 
            // txtChavePrivada
            // 
            this.txtChavePrivada.Location = new System.Drawing.Point(169, 57);
            this.txtChavePrivada.Name = "txtChavePrivada";
            this.txtChavePrivada.Size = new System.Drawing.Size(718, 20);
            this.txtChavePrivada.TabIndex = 38;
            // 
            // txtCodigoFornecedor
            // 
            this.txtCodigoFornecedor.Location = new System.Drawing.Point(788, 19);
            this.txtCodigoFornecedor.Name = "txtCodigoFornecedor";
            this.txtCodigoFornecedor.Size = new System.Drawing.Size(100, 20);
            this.txtCodigoFornecedor.TabIndex = 37;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(625, 22);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(160, 13);
            this.label42.TabIndex = 36;
            this.label42.Text = "Código Fornecedor (destinatário)";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(14, 60);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(127, 13);
            this.label33.TabIndex = 35;
            this.label33.Text = "Chave Privada do Cliente";
            // 
            // txtCodigoCliente
            // 
            this.txtCodigoCliente.Location = new System.Drawing.Point(514, 19);
            this.txtCodigoCliente.Name = "txtCodigoCliente";
            this.txtCodigoCliente.Size = new System.Drawing.Size(100, 20);
            this.txtCodigoCliente.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Drake ID";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(356, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(146, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Código do Cliente (remetente)";
            // 
            // txtDrakeId
            // 
            this.txtDrakeId.Location = new System.Drawing.Point(70, 19);
            this.txtDrakeId.Name = "txtDrakeId";
            this.txtDrakeId.Size = new System.Drawing.Size(100, 20);
            this.txtDrakeId.TabIndex = 23;
            // 
            // txtExternoId
            // 
            this.txtExternoId.Location = new System.Drawing.Point(240, 18);
            this.txtExternoId.Name = "txtExternoId";
            this.txtExternoId.Size = new System.Drawing.Size(100, 20);
            this.txtExternoId.TabIndex = 32;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(181, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Externo ID";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(343, 21);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(406, 24);
            this.label43.TabIndex = 38;
            this.label43.Text = "Este formulário é para ser usado por um cliente";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(216, 95);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(201, 13);
            this.label89.TabIndex = 65;
            this.label89.Text = "Código do atendimento que será reusado";
            // 
            // cboxReusoAtendimento
            // 
            this.cboxReusoAtendimento.AutoSize = true;
            this.cboxReusoAtendimento.Location = new System.Drawing.Point(22, 95);
            this.cboxReusoAtendimento.Name = "cboxReusoAtendimento";
            this.cboxReusoAtendimento.Size = new System.Drawing.Size(168, 17);
            this.cboxReusoAtendimento.TabIndex = 66;
            this.cboxReusoAtendimento.Text = "Solicitar reuso de atendimento";
            this.cboxReusoAtendimento.UseVisualStyleBackColor = true;
            this.cboxReusoAtendimento.CheckedChanged += new System.EventHandler(this.cboxReusoAtendimento_CheckedChanged);
            // 
            // txtCodigoAtendimentoReusado
            // 
            this.txtCodigoAtendimentoReusado.Enabled = false;
            this.txtCodigoAtendimentoReusado.Location = new System.Drawing.Point(423, 93);
            this.txtCodigoAtendimentoReusado.Name = "txtCodigoAtendimentoReusado";
            this.txtCodigoAtendimentoReusado.Size = new System.Drawing.Size(100, 20);
            this.txtCodigoAtendimentoReusado.TabIndex = 67;
            // 
            // FormSolicitarAlterarNecessidade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1078, 1043);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Name = "FormSolicitarAlterarNecessidade";
            this.Text = "Operações Realizadas por Clientes";
            this.Load += new System.EventHandler(this.FormSolicitarAlterarNecessidade_Load);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBoxParticipante2.ResumeLayout(false);
            this.groupBoxParticipante2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox txtOrdemChegadaParticipante1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtDrakeIDParticipante1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtOrdemSaidaParticipante1;
        private System.Windows.Forms.GroupBox groupBoxParticipante2;
        private System.Windows.Forms.TextBox txtOrdemChegadaParticipante2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtDrakeIDParticipante2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtOrdemSaidaParticipante2;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox txtIATAOrigem2;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtCEPOrigem2;
        private System.Windows.Forms.TextBox txtNumeroOrigem2;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtLongitudeOrigem2;
        private System.Windows.Forms.TextBox txtPaisOrigem2;
        private System.Windows.Forms.TextBox txtLatitudeOrigem2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtEstadoOrigem2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtCidadeOrigem2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtBairroOrigem2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtComplementoOrigem2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtRuaOrigem2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtIATAOrigem;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtNumeroOrigem;
        private System.Windows.Forms.TextBox txtCEPOrigem;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtLongitudeOrigem;
        private System.Windows.Forms.TextBox txtLatitudeOrigem;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtPaisOrigem;
        private System.Windows.Forms.TextBox txtEstadoOrigem;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtCidadeOrigem;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtBairroOrigem;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtComplementoOrigem;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtRuaOrigem;
        private System.Windows.Forms.DateTimePicker dtpTermino;
        private System.Windows.Forms.DateTimePicker dtpInicio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEnviarSolicitacao;
        private System.Windows.Forms.CheckBox checkboxAlteracao;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtChavePrivada;
        private System.Windows.Forms.TextBox txtCodigoFornecedor;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtCodigoCliente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDrakeId;
        private System.Windows.Forms.TextBox txtExternoId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.CheckBox checkBoxAssociada;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox txtDrakeIDNecessidade1;
        private System.Windows.Forms.TextBox txtNomeParticipante1;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtIATADestino1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtNumeroDestino1;
        private System.Windows.Forms.TextBox txtCEPDestino1;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtLongitudeDestino1;
        private System.Windows.Forms.TextBox txtLatitudeDestino1;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtPaisDestino1;
        private System.Windows.Forms.TextBox txtEstadoDestino1;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox txtCidadeDestino1;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox txtBairroDestino1;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox txtComplementoDestino1;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox txtRuaDestino1;
        private System.Windows.Forms.TextBox txtNomeParticipante2;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtIATADestino2;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txtNumeroDestino2;
        private System.Windows.Forms.TextBox txtCEPDestino2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox txtLongitudeDestino2;
        private System.Windows.Forms.TextBox txtLatitudeDestino2;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox txtPaisDestino2;
        private System.Windows.Forms.TextBox txtEstadoDestino2;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox txtCidadeDestino2;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox txtBairroDestino2;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox txtComplementoDestino2;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox txtRuaDestino2;
        private System.Windows.Forms.TextBox txtDrakeIDNecessidade2;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox txtComentarioNecessidade2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtComentarioNecessidade1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxTipo;
        private System.Windows.Forms.TextBox txtSubtipo;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtIdentidadeParticipante1;
        private System.Windows.Forms.TextBox txtEmailParticipante1;
        private System.Windows.Forms.TextBox txtCodigoCentroCustoParticipante1;
        private System.Windows.Forms.TextBox txtTelefonesParticipante1;
        private System.Windows.Forms.TextBox txtPassaporteParticipante1;
        private System.Windows.Forms.TextBox txtCPFParticipante1;
        private System.Windows.Forms.ComboBox comboBoxSexoParticipante1;
        private System.Windows.Forms.ComboBox comboBoxSexoParticipante2;
        private System.Windows.Forms.TextBox txtEmailParticipante2;
        private System.Windows.Forms.TextBox txtCodigoCentroCustoParticipante2;
        private System.Windows.Forms.TextBox txtTelefonesParticipante2;
        private System.Windows.Forms.TextBox txtPassaporteParticipante2;
        private System.Windows.Forms.TextBox txtCPFParticipante2;
        private System.Windows.Forms.TextBox txtIdentidadeParticipante2;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNomeCentroCustoParticipante1;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox txtNomeCentroCustoParticipante2;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox txtNomeCentroCustoNecessidade;
        private System.Windows.Forms.TextBox txtCodigoCentroCustoNecessidade;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox txtNomeCentroCustoNecessidade1;
        private System.Windows.Forms.TextBox txtCodigoCentroCustoNecessidade1;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox txtNomeCentroCustoNecessidade2;
        private System.Windows.Forms.TextBox txtCodigoCentroCustoNecessidade2;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox txtCodigoAtendimentoReusado;
        private System.Windows.Forms.CheckBox cboxReusoAtendimento;
    }
}