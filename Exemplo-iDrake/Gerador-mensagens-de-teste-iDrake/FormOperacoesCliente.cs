﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using iDrake.Client;
using iDrake.Client.Config;
using Gerador_mensagens_de_teste_iDrake.MensagensCliente;

namespace Gerador_mensagens_de_teste_iDrake
{
    public partial class FormOperacoesCliente : Form
    {
        public FormOperacoesCliente()
        {
            InitializeComponent();
        }

        private int GetCodigoCliente()
        {
            return int.Parse(txtCodigoCliente.Text);
        }

        private int GetCodigoFornecedor()
        {
            return int.Parse(txtCodigoFornecedor.Text);
        }

        private int GetDrakeId()
        {
            return int.Parse(txtDrakeId.Text);
        }

        private string GetExternoId()
        {
            return txtExternoId.Text;
        }

        private void btnCaixaEntrada_Click(object sender, EventArgs e)
        {
            try
            {
                var caixaSaida = new FormCaixaEntrada();

                caixaSaida.SenderId = GetCodigoCliente();
                caixaSaida.PrivateKey = txtChavePrivada.Text;

                caixaSaida.Show(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }   
        }

        private void btnSolicitarAlterarNecessidade_Click(object sender, EventArgs e)
        {
            var form = new FormSolicitarAlterarNecessidade
            {
                ConfigStrategy = GetConfig(),
                CodigoCliente = GetCodigoCliente(),
                CodigoFornecedor = GetCodigoFornecedor(),
                ChavePrivadaCliente = txtChavePrivada.Text,
                ReceberConfirmacao = cboxReceberConfirmacaoRecebimento.Checked
            };

            form.ShowDialog(this);      
        }

        private IStrategyConfiguration GetConfig()
        {
            return new ConfigStrategy(GetCodigoFornecedor(), txtChavePrivada.Text);
        }

        private void FormSolicitarAlterarNecessidade_Load(object sender, EventArgs e)
        {
        
        }

        private void btnCancelarServico_Click_1(object sender, EventArgs e)
        {
            try
            {
                var operacao = new CustomerOperation();

                var mensagem = new CancelationRequestMessage
                {
                    DrakeId = GetDrakeId(),
                    SenderId = GetCodigoCliente(),
                    RecipientId = GetCodigoFornecedor(),
                    Acknowledgment = cboxReceberConfirmacaoRecebimento.Checked
                };

                operacao.CancelationRequest(mensagem);

                operacao.Execute(new ConfigStrategy(GetCodigoCliente(), txtChavePrivada.Text));
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        private void btnAprovarFatura_Click_1(object sender, EventArgs e)
        {
            try
            {
                var operacao = new CustomerOperation();

                var mensagem = new ApproveInvoiceMessage
                {
                    DrakeId = GetDrakeId(),
                    ExternalId = GetExternoId(),
                    SenderId = GetCodigoCliente(),
                    RecipientId = GetCodigoFornecedor(),
                    Acknowledgment = cboxReceberConfirmacaoRecebimento.Checked
                };

                operacao.ApproveInvoice(mensagem);

                operacao.Execute(new ConfigStrategy(GetCodigoCliente(), txtChavePrivada.Text));
                MessageBox.Show("Enviado com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        private void btnReprovarUmaFatura_Click(object sender, EventArgs e)
        {
            var form = new FormRecusarFatura
            {
                ConfigStrategy = GetConfig(),
                CodigoCliente = GetCodigoCliente(),
                CodigoFornecedor = GetCodigoFornecedor(),
                ChavePrivadaCliente = txtChavePrivada.Text,
                ReceberConfirmacao = cboxReceberConfirmacaoRecebimento.Checked
            };

            form.ShowDialog(this);      
        }

        private void btnConfirmarRecebimento_Click(object sender, EventArgs e)
        {
            var operacao = new ReadOperation();

            var lista = new List<long>();

            lista.Add(Int32.Parse(txtCodigoMensagem.Text));

            operacao.ConfirmReadedMessages(new ConfigStrategy(GetCodigoCliente(), txtChavePrivada.Text), lista);
        }
    }
}
