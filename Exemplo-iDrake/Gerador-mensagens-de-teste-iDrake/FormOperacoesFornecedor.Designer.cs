﻿namespace Gerador_mensagens_de_teste_iDrake
{
    partial class FormOperacoesFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOperacoesFornecedor));
            this.txtCodigoCliente = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtExternoId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCancelarServico = new System.Windows.Forms.Button();
            this.txtDetalhes = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btnInformarNoShow = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnInformarUsoServico = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtReembolso = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btnConfirmarSolicitacaoCancelamentoComReembolso = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnConfirmarSolicitacaoCancelamento = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMotivo = new System.Windows.Forms.TextBox();
            this.btnRecusarSolicitacao = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cboxSomarValores = new System.Windows.Forms.CheckBox();
            this.txtCodigoAtendimento = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txtCodigoProvedor = new System.Windows.Forms.TextBox();
            this.txtAbatimentos = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.cboxReusarAtendimento = new System.Windows.Forms.CheckBox();
            this.txtCodigoAtendimentoReusado = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtTaxas = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txtComissao = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtNomeProvedor = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.txtOrdemChegadaParticipante1 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtDrakeIDParticipante1 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtOrdemSaidaParticipante1 = new System.Windows.Forms.TextBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.txtOrdemChegadaParticipante2 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtDrakeIDParticipante2 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtOrdemSaidaParticipante2 = new System.Windows.Forms.TextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.txtIATADestino = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtCEPDestino = new System.Windows.Forms.TextBox();
            this.txtNumeroDestino = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtLongitudeDestino = new System.Windows.Forms.TextBox();
            this.txtPaisDestino = new System.Windows.Forms.TextBox();
            this.txtLatitudeDestino = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtEstadoDestino = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtCidadeDestino = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtBairroDestino = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtComplementoDestino = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtRuaDestino = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtIATAOrigem = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtNumeroOrigem = new System.Windows.Forms.TextBox();
            this.txtCEPOrigem = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtLongitudeOrigem = new System.Windows.Forms.TextBox();
            this.txtLatitudeOrigem = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtPaisOrigem = new System.Windows.Forms.TextBox();
            this.txtEstadoOrigem = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCidadeOrigem = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtBairroOrigem = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtComplementoOrigem = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtRuaOrigem = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtReserva = new System.Windows.Forms.TextBox();
            this.txtPreco = new System.Windows.Forms.TextBox();
            this.dtpTermino = new System.Windows.Forms.DateTimePicker();
            this.dtpInicio = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConfirmarSolicitacao = new System.Windows.Forms.Button();
            this.txtDrakeId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtChavePrivada = new System.Windows.Forms.TextBox();
            this.txtCodigoFornecedor = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.btnCaixaEntrada = new System.Windows.Forms.Button();
            this.btnSolicitarAprovacaoParaFaturamento = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.txtCodigoMensagem = new System.Windows.Forms.TextBox();
            this.btnConfirmarRecebimento = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.txtCodigoExterno = new System.Windows.Forms.TextBox();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtCodigoCliente
            // 
            this.txtCodigoCliente.Location = new System.Drawing.Point(700, 23);
            this.txtCodigoCliente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCodigoCliente.Name = "txtCodigoCliente";
            this.txtCodigoCliente.Size = new System.Drawing.Size(132, 22);
            this.txtCodigoCliente.TabIndex = 34;
            this.txtCodigoCliente.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(489, 28);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(207, 17);
            this.label8.TabIndex = 33;
            this.label8.Text = "Código do Cliente (destinatário)";
            // 
            // txtExternoId
            // 
            this.txtExternoId.Location = new System.Drawing.Point(320, 22);
            this.txtExternoId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtExternoId.Name = "txtExternoId";
            this.txtExternoId.Size = new System.Drawing.Size(132, 22);
            this.txtExternoId.TabIndex = 32;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(241, 26);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 17);
            this.label7.TabIndex = 31;
            this.label7.Text = "Externo ID";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label6);
            this.groupBox9.Controls.Add(this.btnCancelarServico);
            this.groupBox9.Controls.Add(this.txtDetalhes);
            this.groupBox9.Location = new System.Drawing.Point(991, 203);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox9.Size = new System.Drawing.Size(425, 133);
            this.groupBox9.TabIndex = 30;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Cancelar serviço";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 20);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 17);
            this.label6.TabIndex = 18;
            this.label6.Text = "Detalhes";
            // 
            // btnCancelarServico
            // 
            this.btnCancelarServico.Location = new System.Drawing.Point(317, 97);
            this.btnCancelarServico.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancelarServico.Name = "btnCancelarServico";
            this.btnCancelarServico.Size = new System.Drawing.Size(100, 28);
            this.btnCancelarServico.TabIndex = 1;
            this.btnCancelarServico.Text = "Enviar";
            this.btnCancelarServico.UseVisualStyleBackColor = true;
            this.btnCancelarServico.Click += new System.EventHandler(this.btnCancelarServico_Click);
            // 
            // txtDetalhes
            // 
            this.txtDetalhes.Location = new System.Drawing.Point(8, 39);
            this.txtDetalhes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDetalhes.Multiline = true;
            this.txtDetalhes.Name = "txtDetalhes";
            this.txtDetalhes.Size = new System.Drawing.Size(401, 50);
            this.txtDetalhes.TabIndex = 17;
            this.txtDetalhes.Text = "Carro quebrou!";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.btnInformarNoShow);
            this.groupBox8.Location = new System.Drawing.Point(991, 415);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox8.Size = new System.Drawing.Size(425, 65);
            this.groupBox8.TabIndex = 29;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Informar NoShow";
            // 
            // btnInformarNoShow
            // 
            this.btnInformarNoShow.Location = new System.Drawing.Point(317, 28);
            this.btnInformarNoShow.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnInformarNoShow.Name = "btnInformarNoShow";
            this.btnInformarNoShow.Size = new System.Drawing.Size(100, 28);
            this.btnInformarNoShow.TabIndex = 3;
            this.btnInformarNoShow.Text = "Enviar";
            this.btnInformarNoShow.UseVisualStyleBackColor = true;
            this.btnInformarNoShow.Click += new System.EventHandler(this.btnInformarNoShow_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnInformarUsoServico);
            this.groupBox7.Location = new System.Drawing.Point(991, 343);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox7.Size = new System.Drawing.Size(425, 60);
            this.groupBox7.TabIndex = 28;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Informar uso do serviço";
            // 
            // btnInformarUsoServico
            // 
            this.btnInformarUsoServico.Location = new System.Drawing.Point(317, 23);
            this.btnInformarUsoServico.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnInformarUsoServico.Name = "btnInformarUsoServico";
            this.btnInformarUsoServico.Size = new System.Drawing.Size(100, 28);
            this.btnInformarUsoServico.TabIndex = 2;
            this.btnInformarUsoServico.Text = "Enviar";
            this.btnInformarUsoServico.UseVisualStyleBackColor = true;
            this.btnInformarUsoServico.Click += new System.EventHandler(this.btnInformarUsoServico_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtReembolso);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.btnConfirmarSolicitacaoCancelamentoComReembolso);
            this.groupBox6.Location = new System.Drawing.Point(991, 718);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox6.Size = new System.Drawing.Size(425, 87);
            this.groupBox6.TabIndex = 27;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Confirmar solicitação de cancelamento com reembolso";
            // 
            // txtReembolso
            // 
            this.txtReembolso.Location = new System.Drawing.Point(111, 28);
            this.txtReembolso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtReembolso.Name = "txtReembolso";
            this.txtReembolso.Size = new System.Drawing.Size(132, 22);
            this.txtReembolso.TabIndex = 37;
            this.txtReembolso.Text = "50,00";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(23, 28);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(79, 17);
            this.label28.TabIndex = 36;
            this.label28.Text = "Reembolso";
            // 
            // btnConfirmarSolicitacaoCancelamentoComReembolso
            // 
            this.btnConfirmarSolicitacaoCancelamentoComReembolso.Location = new System.Drawing.Point(317, 50);
            this.btnConfirmarSolicitacaoCancelamentoComReembolso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnConfirmarSolicitacaoCancelamentoComReembolso.Name = "btnConfirmarSolicitacaoCancelamentoComReembolso";
            this.btnConfirmarSolicitacaoCancelamentoComReembolso.Size = new System.Drawing.Size(100, 28);
            this.btnConfirmarSolicitacaoCancelamentoComReembolso.TabIndex = 1;
            this.btnConfirmarSolicitacaoCancelamentoComReembolso.Text = "Enviar";
            this.btnConfirmarSolicitacaoCancelamentoComReembolso.UseVisualStyleBackColor = true;
            this.btnConfirmarSolicitacaoCancelamentoComReembolso.Click += new System.EventHandler(this.btnConfirmarSolicitacaoCancelamentoComReembolso_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnConfirmarSolicitacaoCancelamento);
            this.groupBox5.Location = new System.Drawing.Point(991, 635);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Size = new System.Drawing.Size(425, 73);
            this.groupBox5.TabIndex = 26;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Confirmar solicitação de cancelamento";
            // 
            // btnConfirmarSolicitacaoCancelamento
            // 
            this.btnConfirmarSolicitacaoCancelamento.Location = new System.Drawing.Point(317, 23);
            this.btnConfirmarSolicitacaoCancelamento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnConfirmarSolicitacaoCancelamento.Name = "btnConfirmarSolicitacaoCancelamento";
            this.btnConfirmarSolicitacaoCancelamento.Size = new System.Drawing.Size(100, 28);
            this.btnConfirmarSolicitacaoCancelamento.TabIndex = 4;
            this.btnConfirmarSolicitacaoCancelamento.Text = "Enviar";
            this.btnConfirmarSolicitacaoCancelamento.UseVisualStyleBackColor = true;
            this.btnConfirmarSolicitacaoCancelamento.Click += new System.EventHandler(this.btnConfirmarSolicitacaoCancelamento_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtMotivo);
            this.groupBox3.Controls.Add(this.btnRecusarSolicitacao);
            this.groupBox3.Location = new System.Drawing.Point(991, 495);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(425, 133);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Recusar solicitação";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 20);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Motivo";
            // 
            // txtMotivo
            // 
            this.txtMotivo.Location = new System.Drawing.Point(12, 39);
            this.txtMotivo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMotivo.Multiline = true;
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.Size = new System.Drawing.Size(401, 50);
            this.txtMotivo.TabIndex = 6;
            this.txtMotivo.Text = "Não posso atender.";
            // 
            // btnRecusarSolicitacao
            // 
            this.btnRecusarSolicitacao.Location = new System.Drawing.Point(317, 97);
            this.btnRecusarSolicitacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRecusarSolicitacao.Name = "btnRecusarSolicitacao";
            this.btnRecusarSolicitacao.Size = new System.Drawing.Size(100, 28);
            this.btnRecusarSolicitacao.TabIndex = 5;
            this.btnRecusarSolicitacao.Text = "Enviar";
            this.btnRecusarSolicitacao.UseVisualStyleBackColor = true;
            this.btnRecusarSolicitacao.Click += new System.EventHandler(this.btnRecusarSolicitacao_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cboxSomarValores);
            this.groupBox4.Controls.Add(this.txtCodigoAtendimento);
            this.groupBox4.Controls.Add(this.label49);
            this.groupBox4.Controls.Add(this.txtCodigoProvedor);
            this.groupBox4.Controls.Add(this.txtAbatimentos);
            this.groupBox4.Controls.Add(this.label48);
            this.groupBox4.Controls.Add(this.cboxReusarAtendimento);
            this.groupBox4.Controls.Add(this.txtCodigoAtendimentoReusado);
            this.groupBox4.Controls.Add(this.label47);
            this.groupBox4.Controls.Add(this.txtTaxas);
            this.groupBox4.Controls.Add(this.label46);
            this.groupBox4.Controls.Add(this.txtComissao);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.txtNomeProvedor);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.groupBox12);
            this.groupBox4.Controls.Add(this.groupBox11);
            this.groupBox4.Controls.Add(this.txtReserva);
            this.groupBox4.Controls.Add(this.txtPreco);
            this.groupBox4.Controls.Add(this.dtpTermino);
            this.groupBox4.Controls.Add(this.dtpInicio);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.btnConfirmarSolicitacao);
            this.groupBox4.Location = new System.Drawing.Point(16, 203);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(967, 810);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Confirmar solicitação";
            // 
            // cboxSomarValores
            // 
            this.cboxSomarValores.AutoSize = true;
            this.cboxSomarValores.Location = new System.Drawing.Point(347, 761);
            this.cboxSomarValores.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboxSomarValores.Name = "cboxSomarValores";
            this.cboxSomarValores.Size = new System.Drawing.Size(401, 21);
            this.cboxSomarValores.TabIndex = 32;
            this.cboxSomarValores.Text = "Somar valores com os valores já informados anteriormente";
            this.cboxSomarValores.UseVisualStyleBackColor = true;
            // 
            // txtCodigoAtendimento
            // 
            this.txtCodigoAtendimento.Location = new System.Drawing.Point(181, 757);
            this.txtCodigoAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCodigoAtendimento.Name = "txtCodigoAtendimento";
            this.txtCodigoAtendimento.Size = new System.Drawing.Size(120, 22);
            this.txtCodigoAtendimento.TabIndex = 31;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(25, 761);
            this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(154, 17);
            this.label49.TabIndex = 30;
            this.label49.Text = "Código do atendimento";
            // 
            // txtCodigoProvedor
            // 
            this.txtCodigoProvedor.Location = new System.Drawing.Point(732, 71);
            this.txtCodigoProvedor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCodigoProvedor.Name = "txtCodigoProvedor";
            this.txtCodigoProvedor.Size = new System.Drawing.Size(81, 22);
            this.txtCodigoProvedor.TabIndex = 29;
            this.txtCodigoProvedor.Text = "100";
            // 
            // txtAbatimentos
            // 
            this.txtAbatimentos.Location = new System.Drawing.Point(567, 716);
            this.txtAbatimentos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAbatimentos.Name = "txtAbatimentos";
            this.txtAbatimentos.Size = new System.Drawing.Size(77, 22);
            this.txtAbatimentos.TabIndex = 28;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(475, 721);
            this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(86, 17);
            this.label48.TabIndex = 27;
            this.label48.Text = "Abatimentos";
            // 
            // cboxReusarAtendimento
            // 
            this.cboxReusarAtendimento.AutoSize = true;
            this.cboxReusarAtendimento.Location = new System.Drawing.Point(29, 719);
            this.cboxReusarAtendimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboxReusarAtendimento.Name = "cboxReusarAtendimento";
            this.cboxReusarAtendimento.Size = new System.Drawing.Size(158, 21);
            this.cboxReusarAtendimento.TabIndex = 26;
            this.cboxReusarAtendimento.Text = "Reusar atendimento";
            this.cboxReusarAtendimento.UseVisualStyleBackColor = true;
            this.cboxReusarAtendimento.CheckedChanged += new System.EventHandler(this.cboxReusarAtendimento_CheckedChanged);
            // 
            // txtCodigoAtendimentoReusado
            // 
            this.txtCodigoAtendimentoReusado.Enabled = false;
            this.txtCodigoAtendimentoReusado.Location = new System.Drawing.Point(345, 715);
            this.txtCodigoAtendimentoReusado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCodigoAtendimentoReusado.Name = "txtCodigoAtendimentoReusado";
            this.txtCodigoAtendimentoReusado.Size = new System.Drawing.Size(120, 22);
            this.txtCodigoAtendimentoReusado.TabIndex = 25;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(195, 720);
            this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(143, 17);
            this.label47.TabIndex = 24;
            this.label47.Text = "Atendimento reusado";
            // 
            // txtTaxas
            // 
            this.txtTaxas.Location = new System.Drawing.Point(749, 39);
            this.txtTaxas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTaxas.Name = "txtTaxas";
            this.txtTaxas.Size = new System.Drawing.Size(47, 22);
            this.txtTaxas.TabIndex = 23;
            this.txtTaxas.Text = "30,00";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(693, 44);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(46, 17);
            this.label46.TabIndex = 22;
            this.label46.Text = "Taxas";
            // 
            // txtComissao
            // 
            this.txtComissao.Location = new System.Drawing.Point(617, 39);
            this.txtComissao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtComissao.Name = "txtComissao";
            this.txtComissao.Size = new System.Drawing.Size(47, 22);
            this.txtComissao.TabIndex = 20;
            this.txtComissao.Text = "5,50";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(536, 43);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(69, 17);
            this.label45.TabIndex = 19;
            this.label45.Text = "Comissão";
            // 
            // txtNomeProvedor
            // 
            this.txtNomeProvedor.Location = new System.Drawing.Point(823, 73);
            this.txtNomeProvedor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNomeProvedor.Name = "txtNomeProvedor";
            this.txtNomeProvedor.Size = new System.Drawing.Size(117, 22);
            this.txtNomeProvedor.TabIndex = 18;
            this.txtNomeProvedor.Text = "GOL LINHAS AEREAS";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(660, 76);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(66, 17);
            this.label44.TabIndex = 17;
            this.label44.Text = "Provedor";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.groupBox13);
            this.groupBox12.Controls.Add(this.groupBox14);
            this.groupBox12.Location = new System.Drawing.Point(29, 527);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox12.Size = new System.Drawing.Size(728, 175);
            this.groupBox12.TabIndex = 16;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Participantes";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.txtOrdemChegadaParticipante1);
            this.groupBox13.Controls.Add(this.label31);
            this.groupBox13.Controls.Add(this.label30);
            this.groupBox13.Controls.Add(this.txtDrakeIDParticipante1);
            this.groupBox13.Controls.Add(this.label29);
            this.groupBox13.Controls.Add(this.txtOrdemSaidaParticipante1);
            this.groupBox13.Location = new System.Drawing.Point(15, 23);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox13.Size = new System.Drawing.Size(341, 132);
            this.groupBox13.TabIndex = 36;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Participante #1";
            // 
            // txtOrdemChegadaParticipante1
            // 
            this.txtOrdemChegadaParticipante1.Location = new System.Drawing.Point(137, 87);
            this.txtOrdemChegadaParticipante1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtOrdemChegadaParticipante1.Name = "txtOrdemChegadaParticipante1";
            this.txtOrdemChegadaParticipante1.Size = new System.Drawing.Size(56, 22);
            this.txtOrdemChegadaParticipante1.TabIndex = 7;
            this.txtOrdemChegadaParticipante1.Text = "1";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(19, 92);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(110, 17);
            this.label31.TabIndex = 6;
            this.label31.Text = "Ordem chegada";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(39, 59);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(89, 17);
            this.label30.TabIndex = 5;
            this.label30.Text = "Ordem saída";
            // 
            // txtDrakeIDParticipante1
            // 
            this.txtDrakeIDParticipante1.Location = new System.Drawing.Point(137, 23);
            this.txtDrakeIDParticipante1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDrakeIDParticipante1.Name = "txtDrakeIDParticipante1";
            this.txtDrakeIDParticipante1.Size = new System.Drawing.Size(132, 22);
            this.txtDrakeIDParticipante1.TabIndex = 2;
            this.txtDrakeIDParticipante1.Text = "238";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(53, 27);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(63, 17);
            this.label29.TabIndex = 4;
            this.label29.Text = "Drake ID";
            // 
            // txtOrdemSaidaParticipante1
            // 
            this.txtOrdemSaidaParticipante1.Location = new System.Drawing.Point(137, 55);
            this.txtOrdemSaidaParticipante1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtOrdemSaidaParticipante1.Name = "txtOrdemSaidaParticipante1";
            this.txtOrdemSaidaParticipante1.Size = new System.Drawing.Size(56, 22);
            this.txtOrdemSaidaParticipante1.TabIndex = 3;
            this.txtOrdemSaidaParticipante1.Text = "1";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.txtOrdemChegadaParticipante2);
            this.groupBox14.Controls.Add(this.label32);
            this.groupBox14.Controls.Add(this.label34);
            this.groupBox14.Controls.Add(this.txtDrakeIDParticipante2);
            this.groupBox14.Controls.Add(this.label35);
            this.groupBox14.Controls.Add(this.txtOrdemSaidaParticipante2);
            this.groupBox14.Location = new System.Drawing.Point(373, 23);
            this.groupBox14.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox14.Size = new System.Drawing.Size(341, 132);
            this.groupBox14.TabIndex = 37;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Participante #2 (Opcional)";
            // 
            // txtOrdemChegadaParticipante2
            // 
            this.txtOrdemChegadaParticipante2.Location = new System.Drawing.Point(136, 91);
            this.txtOrdemChegadaParticipante2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtOrdemChegadaParticipante2.Name = "txtOrdemChegadaParticipante2";
            this.txtOrdemChegadaParticipante2.Size = new System.Drawing.Size(56, 22);
            this.txtOrdemChegadaParticipante2.TabIndex = 7;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(17, 96);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(110, 17);
            this.label32.TabIndex = 6;
            this.label32.Text = "Ordem chegada";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(37, 63);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(89, 17);
            this.label34.TabIndex = 5;
            this.label34.Text = "Ordem saída";
            // 
            // txtDrakeIDParticipante2
            // 
            this.txtDrakeIDParticipante2.Location = new System.Drawing.Point(136, 27);
            this.txtDrakeIDParticipante2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDrakeIDParticipante2.Name = "txtDrakeIDParticipante2";
            this.txtDrakeIDParticipante2.Size = new System.Drawing.Size(132, 22);
            this.txtDrakeIDParticipante2.TabIndex = 2;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(52, 31);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(59, 17);
            this.label35.TabIndex = 4;
            this.label35.Text = "DrakeID";
            // 
            // txtOrdemSaidaParticipante2
            // 
            this.txtOrdemSaidaParticipante2.Location = new System.Drawing.Point(136, 59);
            this.txtOrdemSaidaParticipante2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtOrdemSaidaParticipante2.Name = "txtOrdemSaidaParticipante2";
            this.txtOrdemSaidaParticipante2.Size = new System.Drawing.Size(56, 22);
            this.txtOrdemSaidaParticipante2.TabIndex = 3;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.groupBox10);
            this.groupBox11.Controls.Add(this.groupBox2);
            this.groupBox11.Controls.Add(this.label17);
            this.groupBox11.Controls.Add(this.label10);
            this.groupBox11.Location = new System.Drawing.Point(29, 105);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox11.Size = new System.Drawing.Size(728, 415);
            this.groupBox11.TabIndex = 15;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Endereços";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.txtIATADestino);
            this.groupBox10.Controls.Add(this.label41);
            this.groupBox10.Controls.Add(this.label39);
            this.groupBox10.Controls.Add(this.txtCEPDestino);
            this.groupBox10.Controls.Add(this.txtNumeroDestino);
            this.groupBox10.Controls.Add(this.label37);
            this.groupBox10.Controls.Add(this.txtLongitudeDestino);
            this.groupBox10.Controls.Add(this.txtPaisDestino);
            this.groupBox10.Controls.Add(this.txtLatitudeDestino);
            this.groupBox10.Controls.Add(this.label26);
            this.groupBox10.Controls.Add(this.txtEstadoDestino);
            this.groupBox10.Controls.Add(this.label27);
            this.groupBox10.Controls.Add(this.label18);
            this.groupBox10.Controls.Add(this.label19);
            this.groupBox10.Controls.Add(this.txtCidadeDestino);
            this.groupBox10.Controls.Add(this.label20);
            this.groupBox10.Controls.Add(this.txtBairroDestino);
            this.groupBox10.Controls.Add(this.label21);
            this.groupBox10.Controls.Add(this.txtComplementoDestino);
            this.groupBox10.Controls.Add(this.label22);
            this.groupBox10.Controls.Add(this.label23);
            this.groupBox10.Controls.Add(this.txtRuaDestino);
            this.groupBox10.Location = new System.Drawing.Point(439, 23);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox10.Size = new System.Drawing.Size(267, 379);
            this.groupBox10.TabIndex = 13;
            this.groupBox10.TabStop = false;
            // 
            // txtIATADestino
            // 
            this.txtIATADestino.Location = new System.Drawing.Point(72, 337);
            this.txtIATADestino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtIATADestino.Name = "txtIATADestino";
            this.txtIATADestino.Size = new System.Drawing.Size(56, 22);
            this.txtIATADestino.TabIndex = 35;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(25, 341);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(38, 17);
            this.label41.TabIndex = 34;
            this.label41.Text = "IATA";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(8, 48);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(58, 17);
            this.label39.TabIndex = 33;
            this.label39.Text = "Número";
            // 
            // txtCEPDestino
            // 
            this.txtCEPDestino.Location = new System.Drawing.Point(71, 304);
            this.txtCEPDestino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCEPDestino.Name = "txtCEPDestino";
            this.txtCEPDestino.Size = new System.Drawing.Size(137, 22);
            this.txtCEPDestino.TabIndex = 30;
            this.txtCEPDestino.Text = "28152142";
            // 
            // txtNumeroDestino
            // 
            this.txtNumeroDestino.Location = new System.Drawing.Point(72, 48);
            this.txtNumeroDestino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNumeroDestino.Name = "txtNumeroDestino";
            this.txtNumeroDestino.Size = new System.Drawing.Size(185, 22);
            this.txtNumeroDestino.TabIndex = 34;
            this.txtNumeroDestino.Text = "171";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(31, 309);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(35, 17);
            this.label37.TabIndex = 27;
            this.label37.Text = "CEP";
            // 
            // txtLongitudeDestino
            // 
            this.txtLongitudeDestino.Location = new System.Drawing.Point(71, 272);
            this.txtLongitudeDestino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLongitudeDestino.Name = "txtLongitudeDestino";
            this.txtLongitudeDestino.Size = new System.Drawing.Size(185, 22);
            this.txtLongitudeDestino.TabIndex = 29;
            this.txtLongitudeDestino.Text = "-12412355";
            // 
            // txtPaisDestino
            // 
            this.txtPaisDestino.Location = new System.Drawing.Point(72, 208);
            this.txtPaisDestino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPaisDestino.Name = "txtPaisDestino";
            this.txtPaisDestino.Size = new System.Drawing.Size(185, 22);
            this.txtPaisDestino.TabIndex = 21;
            this.txtPaisDestino.Text = "Brasil";
            // 
            // txtLatitudeDestino
            // 
            this.txtLatitudeDestino.Location = new System.Drawing.Point(71, 240);
            this.txtLatitudeDestino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLatitudeDestino.Name = "txtLatitudeDestino";
            this.txtLatitudeDestino.Size = new System.Drawing.Size(185, 22);
            this.txtLatitudeDestino.TabIndex = 28;
            this.txtLatitudeDestino.Text = "1231444441";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(20, 276);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(44, 17);
            this.label26.TabIndex = 27;
            this.label26.Text = "Long.";
            // 
            // txtEstadoDestino
            // 
            this.txtEstadoDestino.Location = new System.Drawing.Point(72, 176);
            this.txtEstadoDestino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEstadoDestino.Name = "txtEstadoDestino";
            this.txtEstadoDestino.Size = new System.Drawing.Size(43, 22);
            this.txtEstadoDestino.TabIndex = 20;
            this.txtEstadoDestino.Text = "RJ";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(32, 242);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(32, 17);
            this.label27.TabIndex = 26;
            this.label27.Text = "Lat.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(28, 212);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 17);
            this.label18.TabIndex = 19;
            this.label18.Text = "País";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 180);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(52, 17);
            this.label19.TabIndex = 18;
            this.label19.Text = "Estado";
            // 
            // txtCidadeDestino
            // 
            this.txtCidadeDestino.Location = new System.Drawing.Point(72, 144);
            this.txtCidadeDestino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCidadeDestino.Name = "txtCidadeDestino";
            this.txtCidadeDestino.Size = new System.Drawing.Size(185, 22);
            this.txtCidadeDestino.TabIndex = 17;
            this.txtCidadeDestino.Text = "Unamar";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 148);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 17);
            this.label20.TabIndex = 16;
            this.label20.Text = "Cidade";
            // 
            // txtBairroDestino
            // 
            this.txtBairroDestino.Location = new System.Drawing.Point(72, 112);
            this.txtBairroDestino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBairroDestino.Name = "txtBairroDestino";
            this.txtBairroDestino.Size = new System.Drawing.Size(185, 22);
            this.txtBairroDestino.TabIndex = 15;
            this.txtBairroDestino.Text = "Centro";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(21, 116);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 17);
            this.label21.TabIndex = 14;
            this.label21.Text = "Bairro";
            // 
            // txtComplementoDestino
            // 
            this.txtComplementoDestino.Location = new System.Drawing.Point(72, 80);
            this.txtComplementoDestino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtComplementoDestino.Name = "txtComplementoDestino";
            this.txtComplementoDestino.Size = new System.Drawing.Size(185, 22);
            this.txtComplementoDestino.TabIndex = 13;
            this.txtComplementoDestino.Text = "N/A";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(15, 84);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 17);
            this.label22.TabIndex = 12;
            this.label22.Text = "Compl.";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(25, 20);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(34, 17);
            this.label23.TabIndex = 11;
            this.label23.Text = "Rua";
            // 
            // txtRuaDestino
            // 
            this.txtRuaDestino.Location = new System.Drawing.Point(71, 16);
            this.txtRuaDestino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRuaDestino.Name = "txtRuaDestino";
            this.txtRuaDestino.Size = new System.Drawing.Size(185, 22);
            this.txtRuaDestino.TabIndex = 10;
            this.txtRuaDestino.Text = "Visconde de Sabugosa";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtIATAOrigem);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.txtNumeroOrigem);
            this.groupBox2.Controls.Add(this.txtCEPOrigem);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.txtLongitudeOrigem);
            this.groupBox2.Controls.Add(this.txtLatitudeOrigem);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.txtPaisOrigem);
            this.groupBox2.Controls.Add(this.txtEstadoOrigem);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtCidadeOrigem);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtBairroOrigem);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtComplementoOrigem);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtRuaOrigem);
            this.groupBox2.Location = new System.Drawing.Point(85, 23);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(267, 379);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            // 
            // txtIATAOrigem
            // 
            this.txtIATAOrigem.Location = new System.Drawing.Point(67, 337);
            this.txtIATAOrigem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtIATAOrigem.Name = "txtIATAOrigem";
            this.txtIATAOrigem.Size = new System.Drawing.Size(56, 22);
            this.txtIATAOrigem.TabIndex = 34;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(20, 341);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(38, 17);
            this.label40.TabIndex = 33;
            this.label40.Text = "IATA";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(3, 48);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(58, 17);
            this.label38.TabIndex = 32;
            this.label38.Text = "Número";
            // 
            // txtNumeroOrigem
            // 
            this.txtNumeroOrigem.Location = new System.Drawing.Point(67, 48);
            this.txtNumeroOrigem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNumeroOrigem.Name = "txtNumeroOrigem";
            this.txtNumeroOrigem.Size = new System.Drawing.Size(185, 22);
            this.txtNumeroOrigem.TabIndex = 32;
            this.txtNumeroOrigem.Text = "46";
            // 
            // txtCEPOrigem
            // 
            this.txtCEPOrigem.Location = new System.Drawing.Point(67, 304);
            this.txtCEPOrigem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCEPOrigem.Name = "txtCEPOrigem";
            this.txtCEPOrigem.Size = new System.Drawing.Size(137, 22);
            this.txtCEPOrigem.TabIndex = 31;
            this.txtCEPOrigem.Text = "27910000";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(24, 311);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 17);
            this.label36.TabIndex = 26;
            this.label36.Text = "CEP";
            // 
            // txtLongitudeOrigem
            // 
            this.txtLongitudeOrigem.Location = new System.Drawing.Point(67, 272);
            this.txtLongitudeOrigem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLongitudeOrigem.Name = "txtLongitudeOrigem";
            this.txtLongitudeOrigem.Size = new System.Drawing.Size(185, 22);
            this.txtLongitudeOrigem.TabIndex = 25;
            this.txtLongitudeOrigem.Text = "-123534";
            // 
            // txtLatitudeOrigem
            // 
            this.txtLatitudeOrigem.Location = new System.Drawing.Point(67, 240);
            this.txtLatitudeOrigem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLatitudeOrigem.Name = "txtLatitudeOrigem";
            this.txtLatitudeOrigem.Size = new System.Drawing.Size(185, 22);
            this.txtLatitudeOrigem.TabIndex = 24;
            this.txtLatitudeOrigem.Text = "123444444";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(16, 276);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(44, 17);
            this.label25.TabIndex = 23;
            this.label25.Text = "Long.";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(28, 242);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(32, 17);
            this.label24.TabIndex = 22;
            this.label24.Text = "Lat.";
            // 
            // txtPaisOrigem
            // 
            this.txtPaisOrigem.Location = new System.Drawing.Point(67, 208);
            this.txtPaisOrigem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPaisOrigem.Name = "txtPaisOrigem";
            this.txtPaisOrigem.Size = new System.Drawing.Size(185, 22);
            this.txtPaisOrigem.TabIndex = 21;
            this.txtPaisOrigem.Text = "Brasil";
            // 
            // txtEstadoOrigem
            // 
            this.txtEstadoOrigem.Location = new System.Drawing.Point(67, 176);
            this.txtEstadoOrigem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEstadoOrigem.Name = "txtEstadoOrigem";
            this.txtEstadoOrigem.Size = new System.Drawing.Size(43, 22);
            this.txtEstadoOrigem.TabIndex = 20;
            this.txtEstadoOrigem.Text = "RJ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 212);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 17);
            this.label16.TabIndex = 19;
            this.label16.Text = "País";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 180);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 17);
            this.label15.TabIndex = 18;
            this.label15.Text = "Estado";
            // 
            // txtCidadeOrigem
            // 
            this.txtCidadeOrigem.Location = new System.Drawing.Point(67, 144);
            this.txtCidadeOrigem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCidadeOrigem.Name = "txtCidadeOrigem";
            this.txtCidadeOrigem.Size = new System.Drawing.Size(185, 22);
            this.txtCidadeOrigem.TabIndex = 17;
            this.txtCidadeOrigem.Text = "Macaé";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 148);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 17);
            this.label14.TabIndex = 16;
            this.label14.Text = "Cidade";
            // 
            // txtBairroOrigem
            // 
            this.txtBairroOrigem.Location = new System.Drawing.Point(67, 112);
            this.txtBairroOrigem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBairroOrigem.Name = "txtBairroOrigem";
            this.txtBairroOrigem.Size = new System.Drawing.Size(185, 22);
            this.txtBairroOrigem.TabIndex = 15;
            this.txtBairroOrigem.Text = "CEHAB";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 116);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 17);
            this.label13.TabIndex = 14;
            this.label13.Text = "Bairro";
            // 
            // txtComplementoOrigem
            // 
            this.txtComplementoOrigem.Location = new System.Drawing.Point(67, 80);
            this.txtComplementoOrigem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtComplementoOrigem.Name = "txtComplementoOrigem";
            this.txtComplementoOrigem.Size = new System.Drawing.Size(185, 22);
            this.txtComplementoOrigem.TabIndex = 13;
            this.txtComplementoOrigem.Text = "Casa A";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 84);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 17);
            this.label12.TabIndex = 12;
            this.label12.Text = "Compl.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 20);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 17);
            this.label11.TabIndex = 11;
            this.label11.Text = "Rua";
            // 
            // txtRuaOrigem
            // 
            this.txtRuaOrigem.Location = new System.Drawing.Point(67, 16);
            this.txtRuaOrigem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRuaOrigem.Name = "txtRuaOrigem";
            this.txtRuaOrigem.Size = new System.Drawing.Size(185, 22);
            this.txtRuaOrigem.TabIndex = 10;
            this.txtRuaOrigem.Text = "Pereira Neto";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(372, 30);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 17);
            this.label17.TabIndex = 14;
            this.label17.Text = "Destino";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 31);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 17);
            this.label10.TabIndex = 12;
            this.label10.Text = "Origem";
            // 
            // txtReserva
            // 
            this.txtReserva.Location = new System.Drawing.Point(445, 73);
            this.txtReserva.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtReserva.Name = "txtReserva";
            this.txtReserva.Size = new System.Drawing.Size(132, 22);
            this.txtReserva.TabIndex = 9;
            this.txtReserva.Text = "XX1234";
            // 
            // txtPreco
            // 
            this.txtPreco.Location = new System.Drawing.Point(447, 39);
            this.txtPreco.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPreco.Name = "txtPreco";
            this.txtPreco.Size = new System.Drawing.Size(69, 22);
            this.txtPreco.TabIndex = 8;
            this.txtPreco.Text = "230,00";
            // 
            // dtpTermino
            // 
            this.dtpTermino.Location = new System.Drawing.Point(93, 73);
            this.dtpTermino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpTermino.Name = "dtpTermino";
            this.dtpTermino.Size = new System.Drawing.Size(265, 22);
            this.dtpTermino.TabIndex = 7;
            this.dtpTermino.Value = new System.DateTime(2013, 11, 2, 0, 0, 0, 0);
            // 
            // dtpInicio
            // 
            this.dtpInicio.CustomFormat = "";
            this.dtpInicio.Location = new System.Drawing.Point(93, 36);
            this.dtpInicio.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpInicio.Name = "dtpInicio";
            this.dtpInicio.Size = new System.Drawing.Size(265, 22);
            this.dtpInicio.TabIndex = 6;
            this.dtpInicio.Value = new System.DateTime(2013, 11, 1, 0, 0, 0, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(375, 73);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 17);
            this.label9.TabIndex = 4;
            this.label9.Text = "Reserva";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(391, 39);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Preço";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Término";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Início";
            // 
            // btnConfirmarSolicitacao
            // 
            this.btnConfirmarSolicitacao.Location = new System.Drawing.Point(657, 715);
            this.btnConfirmarSolicitacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnConfirmarSolicitacao.Name = "btnConfirmarSolicitacao";
            this.btnConfirmarSolicitacao.Size = new System.Drawing.Size(100, 28);
            this.btnConfirmarSolicitacao.TabIndex = 0;
            this.btnConfirmarSolicitacao.Text = "Enviar";
            this.btnConfirmarSolicitacao.UseVisualStyleBackColor = true;
            this.btnConfirmarSolicitacao.Click += new System.EventHandler(this.btnConfirmarSolicitacao_Click);
            // 
            // txtDrakeId
            // 
            this.txtDrakeId.Location = new System.Drawing.Point(93, 23);
            this.txtDrakeId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDrakeId.Name = "txtDrakeId";
            this.txtDrakeId.Size = new System.Drawing.Size(132, 22);
            this.txtDrakeId.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 27);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 17);
            this.label3.TabIndex = 22;
            this.label3.Text = "Drake ID";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtChavePrivada);
            this.groupBox1.Controls.Add(this.txtCodigoFornecedor);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.txtCodigoCliente);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtDrakeId);
            this.groupBox1.Controls.Add(this.txtExternoId);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(16, 63);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(1207, 133);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados básicos";
            // 
            // txtChavePrivada
            // 
            this.txtChavePrivada.Location = new System.Drawing.Point(225, 70);
            this.txtChavePrivada.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtChavePrivada.Name = "txtChavePrivada";
            this.txtChavePrivada.Size = new System.Drawing.Size(956, 22);
            this.txtChavePrivada.TabIndex = 38;
            this.txtChavePrivada.Text = resources.GetString("txtChavePrivada.Text");
            // 
            // txtCodigoFornecedor
            // 
            this.txtCodigoFornecedor.Location = new System.Drawing.Point(1051, 23);
            this.txtCodigoFornecedor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCodigoFornecedor.Name = "txtCodigoFornecedor";
            this.txtCodigoFornecedor.Size = new System.Drawing.Size(132, 22);
            this.txtCodigoFornecedor.TabIndex = 37;
            this.txtCodigoFornecedor.Text = "6";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(845, 27);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(207, 17);
            this.label42.TabIndex = 36;
            this.label42.Text = "Código Fornecedor (remetente)";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(19, 74);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(197, 17);
            this.label33.TabIndex = 35;
            this.label33.Text = "Chave Privada do Fornecedor";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(308, 17);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(565, 29);
            this.label43.TabIndex = 36;
            this.label43.Text = "Este formulário é para ser usado por um fornecedor";
            // 
            // btnCaixaEntrada
            // 
            this.btnCaixaEntrada.Location = new System.Drawing.Point(991, 912);
            this.btnCaixaEntrada.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCaixaEntrada.Name = "btnCaixaEntrada";
            this.btnCaixaEntrada.Size = new System.Drawing.Size(425, 48);
            this.btnCaixaEntrada.TabIndex = 37;
            this.btnCaixaEntrada.Text = "Ver caixa de entrada";
            this.btnCaixaEntrada.UseVisualStyleBackColor = true;
            this.btnCaixaEntrada.Click += new System.EventHandler(this.btnCaixaEntrada_Click);
            // 
            // btnSolicitarAprovacaoParaFaturamento
            // 
            this.btnSolicitarAprovacaoParaFaturamento.Location = new System.Drawing.Point(16, 1020);
            this.btnSolicitarAprovacaoParaFaturamento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSolicitarAprovacaoParaFaturamento.Name = "btnSolicitarAprovacaoParaFaturamento";
            this.btnSolicitarAprovacaoParaFaturamento.Size = new System.Drawing.Size(332, 28);
            this.btnSolicitarAprovacaoParaFaturamento.TabIndex = 38;
            this.btnSolicitarAprovacaoParaFaturamento.Text = "Solicitar Aprovação para Faturamento";
            this.btnSolicitarAprovacaoParaFaturamento.UseVisualStyleBackColor = true;
            this.btnSolicitarAprovacaoParaFaturamento.Click += new System.EventHandler(this.btnSolicitarAprovacaoParaFaturamento_Click);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.txtCodigoExterno);
            this.groupBox15.Controls.Add(this.label51);
            this.groupBox15.Controls.Add(this.label50);
            this.groupBox15.Controls.Add(this.txtCodigoMensagem);
            this.groupBox15.Controls.Add(this.btnConfirmarRecebimento);
            this.groupBox15.Location = new System.Drawing.Point(991, 812);
            this.groupBox15.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox15.Size = new System.Drawing.Size(425, 92);
            this.groupBox15.TabIndex = 47;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Confirmar recebimento";
            // 
            // txtCodigoMensagem
            // 
            this.txtCodigoMensagem.Location = new System.Drawing.Point(120, 16);
            this.txtCodigoMensagem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCodigoMensagem.Name = "txtCodigoMensagem";
            this.txtCodigoMensagem.Size = new System.Drawing.Size(88, 22);
            this.txtCodigoMensagem.TabIndex = 46;
            // 
            // btnConfirmarRecebimento
            // 
            this.btnConfirmarRecebimento.Location = new System.Drawing.Point(313, 55);
            this.btnConfirmarRecebimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnConfirmarRecebimento.Name = "btnConfirmarRecebimento";
            this.btnConfirmarRecebimento.Size = new System.Drawing.Size(100, 28);
            this.btnConfirmarRecebimento.TabIndex = 45;
            this.btnConfirmarRecebimento.Text = "Enviar";
            this.btnConfirmarRecebimento.UseVisualStyleBackColor = true;
            this.btnConfirmarRecebimento.Click += new System.EventHandler(this.btnConfirmarRecebimento_Click);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(23, 16);
            this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(94, 17);
            this.label50.TabIndex = 38;
            this.label50.Text = "ID Mensagem";
            this.label50.Click += new System.EventHandler(this.label50_Click);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(9, 57);
            this.label51.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(104, 17);
            this.label51.TabIndex = 47;
            this.label51.Text = "Código Externo";
            // 
            // txtCodigoExterno
            // 
            this.txtCodigoExterno.Location = new System.Drawing.Point(121, 52);
            this.txtCodigoExterno.Margin = new System.Windows.Forms.Padding(4);
            this.txtCodigoExterno.Name = "txtCodigoExterno";
            this.txtCodigoExterno.Size = new System.Drawing.Size(88, 22);
            this.txtCodigoExterno.TabIndex = 48;
            // 
            // FormOperacoesFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1607, 1063);
            this.Controls.Add(this.groupBox15);
            this.Controls.Add(this.btnSolicitarAprovacaoParaFaturamento);
            this.Controls.Add(this.btnCaixaEntrada);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormOperacoesFornecedor";
            this.Text = "Operações Realizadas por Fornecedores";
            this.Load += new System.EventHandler(this.FormOperacoesFornecedor_Load);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCodigoCliente;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtExternoId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCancelarServico;
        private System.Windows.Forms.TextBox txtDetalhes;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btnInformarNoShow;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btnInformarUsoServico;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnConfirmarSolicitacaoCancelamentoComReembolso;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnConfirmarSolicitacaoCancelamento;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMotivo;
        private System.Windows.Forms.Button btnRecusarSolicitacao;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnConfirmarSolicitacao;
        private System.Windows.Forms.TextBox txtDrakeId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtReserva;
        private System.Windows.Forms.TextBox txtPreco;
        private System.Windows.Forms.DateTimePicker dtpTermino;
        private System.Windows.Forms.DateTimePicker dtpInicio;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtPaisOrigem;
        private System.Windows.Forms.TextBox txtEstadoOrigem;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtCidadeOrigem;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtBairroOrigem;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtComplementoOrigem;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtRuaOrigem;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox txtPaisDestino;
        private System.Windows.Forms.TextBox txtEstadoDestino;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtCidadeDestino;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtBairroDestino;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtComplementoDestino;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtRuaDestino;
        private System.Windows.Forms.TextBox txtLongitudeDestino;
        private System.Windows.Forms.TextBox txtLatitudeDestino;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtLongitudeOrigem;
        private System.Windows.Forms.TextBox txtLatitudeOrigem;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox txtOrdemChegadaParticipante1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtDrakeIDParticipante1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtOrdemSaidaParticipante1;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TextBox txtOrdemChegadaParticipante2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtDrakeIDParticipante2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtOrdemSaidaParticipante2;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox txtCEPDestino;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtCEPOrigem;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtIATADestino;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtNumeroDestino;
        private System.Windows.Forms.TextBox txtIATAOrigem;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtNumeroOrigem;
        private System.Windows.Forms.TextBox txtReembolso;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtChavePrivada;
        private System.Windows.Forms.TextBox txtCodigoFornecedor;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button btnCaixaEntrada;
        private System.Windows.Forms.Button btnSolicitarAprovacaoParaFaturamento;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.TextBox txtCodigoMensagem;
        private System.Windows.Forms.Button btnConfirmarRecebimento;
        private System.Windows.Forms.TextBox txtComissao;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtNomeProvedor;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtTaxas;
        private System.Windows.Forms.TextBox txtCodigoAtendimentoReusado;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.CheckBox cboxReusarAtendimento;
        private System.Windows.Forms.TextBox txtAbatimentos;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtCodigoProvedor;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txtCodigoAtendimento;
        private System.Windows.Forms.CheckBox cboxSomarValores;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtCodigoExterno;
        private System.Windows.Forms.Label label51;
    }
}