﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gerador_mensagens_de_teste_iDrake
{
    
    public static class SequenciaDrakeID
    {

        public static int Contagem
        {
            set
            {
                _contagem = value;
            }
        }

        private static int _contagem = 0;


        public static int Proximo()
        {
            return ++_contagem;
        }



    }
}
