﻿using System;
using System.Windows.Forms;

namespace Gerador_mensagens_de_teste_iDrake
{
    public partial class FormInicio : Form
    {
        public FormInicio()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new FormOperacoesCliente().Show(this);            
        }

        private void btnFornecedor_Click(object sender, EventArgs e)
        {
            new FormOperacoesFornecedor().Show(this);
        }
    }
}
