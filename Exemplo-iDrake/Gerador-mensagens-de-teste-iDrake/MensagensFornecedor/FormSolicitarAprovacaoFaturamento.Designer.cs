﻿using iDrake.Client.Config;

namespace Gerador_mensagens_de_teste_iDrake
{
    partial class FormSolicitarAprovacaoFaturamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
                  
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtChavePrivadaFornecedor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCodigoCliente = new System.Windows.Forms.TextBox();
            this.txtCodigoFornecedor = new System.Windows.Forms.TextBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCodigoFatura = new System.Windows.Forms.TextBox();
            this.DrakeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Desconto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorAdicional1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorAdicional2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrabalhadorValorAdicional1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomeTrabalhadorVA1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrabalhadorValorAdicional2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomeTrabalhadorVA2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ComentarioValorAdicional1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ComentarioValorAdicional2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Observacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Código do Fornecedor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Código do Cliente";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtChavePrivadaFornecedor);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtCodigoCliente);
            this.groupBox1.Controls.Add(this.txtCodigoFornecedor);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1173, 83);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Configuração";
            // 
            // txtChavePrivadaFornecedor
            // 
            this.txtChavePrivadaFornecedor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChavePrivadaFornecedor.Location = new System.Drawing.Point(372, 24);
            this.txtChavePrivadaFornecedor.Name = "txtChavePrivadaFornecedor";
            this.txtChavePrivadaFornecedor.Size = new System.Drawing.Size(795, 20);
            this.txtChavePrivadaFornecedor.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(232, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Chave Privada Fornecedor";
            // 
            // txtCodigoCliente
            // 
            this.txtCodigoCliente.Location = new System.Drawing.Point(125, 50);
            this.txtCodigoCliente.Name = "txtCodigoCliente";
            this.txtCodigoCliente.Size = new System.Drawing.Size(74, 20);
            this.txtCodigoCliente.TabIndex = 3;
            // 
            // txtCodigoFornecedor
            // 
            this.txtCodigoFornecedor.Location = new System.Drawing.Point(125, 24);
            this.txtCodigoFornecedor.Name = "txtCodigoFornecedor";
            this.txtCodigoFornecedor.Size = new System.Drawing.Size(74, 20);
            this.txtCodigoFornecedor.TabIndex = 2;
            // 
            // btnEnviar
            // 
            this.btnEnviar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnviar.Location = new System.Drawing.Point(1110, 604);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(75, 23);
            this.btnEnviar.TabIndex = 3;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DrakeID,
            this.ValorTotal,
            this.Desconto,
            this.ValorAdicional1,
            this.ValorAdicional2,
            this.TrabalhadorValorAdicional1,
            this.NomeTrabalhadorVA1,
            this.TrabalhadorValorAdicional2,
            this.NomeTrabalhadorVA2,
            this.ComentarioValorAdicional1,
            this.ComentarioValorAdicional2,
            this.Observacao});
            this.dataGridView.Location = new System.Drawing.Point(12, 134);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(1173, 464);
            this.dataGridView.TabIndex = 4;
            // 
            // btnLimpar
            // 
            this.btnLimpar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLimpar.Location = new System.Drawing.Point(12, 604);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 5;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Código da Fatura";
            // 
            // txtCodigoFatura
            // 
            this.txtCodigoFatura.Location = new System.Drawing.Point(106, 105);
            this.txtCodigoFatura.Name = "txtCodigoFatura";
            this.txtCodigoFatura.Size = new System.Drawing.Size(127, 20);
            this.txtCodigoFatura.TabIndex = 7;
            // 
            // DrakeID
            // 
            this.DrakeID.HeaderText = "Código no Drake";
            this.DrakeID.Name = "DrakeID";
            // 
            // ValorTotal
            // 
            this.ValorTotal.HeaderText = "Valor Total";
            this.ValorTotal.Name = "ValorTotal";
            // 
            // Desconto
            // 
            this.Desconto.HeaderText = "Desconto";
            this.Desconto.Name = "Desconto";
            // 
            // ValorAdicional1
            // 
            this.ValorAdicional1.HeaderText = "Valor Adicional 1";
            this.ValorAdicional1.Name = "ValorAdicional1";
            // 
            // ValorAdicional2
            // 
            this.ValorAdicional2.HeaderText = "Valor Adicional 2";
            this.ValorAdicional2.Name = "ValorAdicional2";
            // 
            // TrabalhadorValorAdicional1
            // 
            this.TrabalhadorValorAdicional1.HeaderText = "Trabalhador Valor Adicional 1";
            this.TrabalhadorValorAdicional1.Name = "TrabalhadorValorAdicional1";
            // 
            // NomeTrabalhadorVA1
            // 
            this.NomeTrabalhadorVA1.HeaderText = "Nome Trabalhador VA 1";
            this.NomeTrabalhadorVA1.Name = "NomeTrabalhadorVA1";
            // 
            // TrabalhadorValorAdicional2
            // 
            this.TrabalhadorValorAdicional2.HeaderText = "Trabalhador Valor Adicional 2";
            this.TrabalhadorValorAdicional2.Name = "TrabalhadorValorAdicional2";
            // 
            // NomeTrabalhadorVA2
            // 
            this.NomeTrabalhadorVA2.HeaderText = "Nome Trabalhador VA 1";
            this.NomeTrabalhadorVA2.Name = "NomeTrabalhadorVA2";
            // 
            // ComentarioValorAdicional1
            // 
            this.ComentarioValorAdicional1.HeaderText = "Comentario Valor Adicional 1";
            this.ComentarioValorAdicional1.Name = "ComentarioValorAdicional1";
            // 
            // ComentarioValorAdicional2
            // 
            this.ComentarioValorAdicional2.HeaderText = "Comentario Valor Adicional 2";
            this.ComentarioValorAdicional2.Name = "ComentarioValorAdicional2";
            // 
            // Observacao
            // 
            this.Observacao.HeaderText = "Observação";
            this.Observacao.Name = "Observacao";
            // 
            // FormSolicitarAprovacaoFaturamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 639);
            this.Controls.Add(this.txtCodigoFatura);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormSolicitarAprovacaoFaturamento";
            this.Text = "FORNECEDOR: Solicitar Aprovação para Faturamento";
            this.Load += new System.EventHandler(this.FormSolicitarAprovacaoFaturamento_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCodigoCliente;
        private System.Windows.Forms.TextBox txtCodigoFornecedor;
        private System.Windows.Forms.TextBox txtChavePrivadaFornecedor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCodigoFatura;
        private System.Windows.Forms.DataGridViewTextBoxColumn DrakeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Desconto;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorAdicional1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorAdicional2;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrabalhadorValorAdicional1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeTrabalhadorVA1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrabalhadorValorAdicional2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeTrabalhadorVA2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ComentarioValorAdicional1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ComentarioValorAdicional2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Observacao;
    }
}