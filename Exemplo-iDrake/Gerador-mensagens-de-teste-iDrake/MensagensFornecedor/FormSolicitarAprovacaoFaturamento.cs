﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using iDrake.Client;
using iDrake.Client.Config;

namespace Gerador_mensagens_de_teste_iDrake
{
    public partial class FormSolicitarAprovacaoFaturamento : Form
    {
        public IStrategyConfiguration ConfigStrategy { get; set; }

        public long CodigoFornecedor { get; set; }

        public string ChavePrivadaFornecedor { get; set; }

        public long CodigoCliente { get; set; }
                                                         
        public FormSolicitarAprovacaoFaturamento()
        {
            InitializeComponent();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {    
                var codigoFaturamento = txtCodigoFatura.Text;

                var mensagem = new InvoiceApprovalRequestMessage
                {
                    ExternalId = codigoFaturamento
                };

                for (int i = 0; i < dataGridView.RowCount; i++)
                {
                    try
                    {
                        var drakeID = long.Parse(dataGridView.Rows[i].Cells["DrakeID"].Value.ToString());
                        var valorTotal = decimal.Parse(dataGridView.Rows[i].Cells["ValorTotal"].Value.ToString());
                        
                     

                    
                        var desconto = decimal.Parse(dataGridView.Rows[i].Cells["Desconto"].Value.ToString());
                        var observacao = dataGridView.Rows[i].Cells["Observacao"].Value.ToString();

                        var item = new InvoiceItem
                        {
                            DrakeId = drakeID,
                            Comments = observacao,
                            Total = valorTotal,
                            Rebate = desconto
                        };

                        if (dataGridView.Rows[i].Cells["ValorAdicional1"].Value != null)
                        {
                            var valorAdicional1 =
                                decimal.Parse(dataGridView.Rows[i].Cells["ValorAdicional1"].Value.ToString());
                            var trabalhadorValorAdicional1 =
                                long.Parse(dataGridView.Rows[i].Cells["TrabalhadorValorAdicional1"].Value.ToString());
                            var nomeTrabalhadorValorAdicional1 =
                                dataGridView.Rows[i].Cells["NomeTrabalhadorVA1"].Value.ToString();
                            var comentarioValorAdicional1 =
                                dataGridView.Rows[i].Cells["ComentarioValorAdicional1"].Value.ToString();

                            item.ExtraValues.Add(new InvoiceItemExtraValue
                            {
                                Comments = comentarioValorAdicional1,
                                Value = valorAdicional1,
                                Participant =
                                    new Participant
                                    {
                                        DrakeId = trabalhadorValorAdicional1,
                                        Name = nomeTrabalhadorValorAdicional1
                                    }
                            });
                        }

                        if (dataGridView.Rows[i].Cells["ValorAdicional2"].Value != null)
                        {
                            var valorAdicional2 =
                                decimal.Parse(dataGridView.Rows[i].Cells["ValorAdicional2"].Value.ToString());
                            var trabalhadorValorAdicional2 =
                                long.Parse(dataGridView.Rows[i].Cells["TrabalhadorValorAdicional2"].Value.ToString());
                            var nomeTrabalhadorValorAdicional2 =
                                dataGridView.Rows[i].Cells["NomeTrabalhadorVA2"].Value.ToString();
                            var comentarioValorAdicional2 =
                                dataGridView.Rows[i].Cells["ComentarioValorAdicional2"].Value.ToString();

                            item.ExtraValues.Add(new InvoiceItemExtraValue
                            {
                                Comments = comentarioValorAdicional2,
                                Value = valorAdicional2,
                                Participant =
                                    new Participant
                                    {
                                        DrakeId = trabalhadorValorAdicional2,
                                        Name = nomeTrabalhadorValorAdicional2
                                    }
                            });
                        }
                        mensagem.AddItem(item);
                    }
                    catch (Exception)
                    {      
                    }
                    
                }
                                                      
                var operacao = new ProviderOperation(Convert.ToInt32(CodigoCliente));   
                operacao.InvoiceApprovalRequest(mensagem);

                operacao.Execute(ConfigStrategy);
                MessageBox.Show("Mensagem enviada!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);      

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);      
            }
        }

        private void FormSolicitarAprovacaoFaturamento_Load(object sender, EventArgs e)
        {
            txtCodigoFornecedor.Text = CodigoFornecedor.ToString();
            txtCodigoCliente.Text = CodigoCliente.ToString();
            txtChavePrivadaFornecedor.Text = ChavePrivadaFornecedor;

            CarregarGrid();
        }

        private void CarregarGrid()
        {
            var random = new Random();
            
            for (int i = 0; i < 20; i++)
            {         
                var drakeId = 1000 + i;
                var valorTotal = random.Next(0, 600);   
                var va1 = valorTotal * (decimal)0.1;
                var va2 = valorTotal * (decimal)0.2;

                var tva1 = 200;
                var tva2 = 201;
                var cva1 = "Comentário VA1";
                var cva2 = "Comentário VA2";

                var observacao = "Blablablab " + i;
                var desconto = random.Next(0, 300) * (decimal)0.1;

                dataGridView.Rows.Add(drakeId, valorTotal, desconto, va1, va2, tva1, "João", tva2, "Maria", cva1, cva2, observacao);
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            dataGridView.Rows.Clear();
        }     

    }
}
