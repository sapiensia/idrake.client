﻿using System.Collections.Generic;
using System.Windows.Forms;
using iDrake.Client;
using iDrake.Client.Stubs;
using iDrake.Client.Stubs.Enums;
using iDrake.Client.Utils;
using Newtonsoft.Json.Serialization;

namespace Gerador_mensagens_de_teste_iDrake
{
    public partial class FormCaixaEntrada : Form
    {
        public FormCaixaEntrada()
        {
            InitializeComponent();
        }

        public int SenderId { get; set; }    
        public string PrivateKey { get; set; }

        private void FormCaixaEntrada_Load(object sender, System.EventArgs e)
        {
            Carregar();
        }

        private void Carregar()
        {
            var operacao = new ReadOperation();
            var mensagens = operacao.ReadMessages(new ConfigStrategy(SenderId, PrivateKey));

            dataGridView1.Columns.Clear();

            dataGridView1.Columns.Add("Id", "Id");
            dataGridView1.Columns.Add("DrakeID", "Drake ID");
            dataGridView1.Columns.Add("ExternalId", "External ID");
            dataGridView1.Columns.Add("SenderId", "Sender ID");
            dataGridView1.Columns.Add("Type", "Type");
            dataGridView1.Columns.Add("Content", "Content");

            foreach (var mensagem in mensagens)
            {
                var content = JsonHelper.JsonSerializer(mensagem);

                object[] linha = 
                {
                    mensagem.Id.ToString(),
                    mensagem.DrakeId.ToString(),
                    mensagem.ExternalId,
                    mensagem.SenderId.ToString(),
                    mensagem.Type.ToString(),
                    content
                };

                dataGridView1.Rows.Add(linha);
            }          

        }

        private void btnAtualizar_Click(object sender, System.EventArgs e)
        {
            Carregar();
        }
    }
}
