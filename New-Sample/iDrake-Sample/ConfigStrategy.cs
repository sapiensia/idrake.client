﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iDrake.Client.Config;

namespace iDrake_Sample
{
    public class ConfigStrategy : IStrategyConfiguration
    {

        private readonly int _senderId;
        private readonly string _privateKey;
        private readonly string _integrationUrl = "https://i.drake.bz/MessageService";


        public ConfigStrategy(int senderId, string privateKey)
        {
            _senderId = senderId;
            _privateKey = privateKey;
        }
        public int? GetSenderId()
        {
            return _senderId;
        }

        public string GetPrivateKey()
        {
            return _privateKey;
        }

        public string GetIntegrationServiceUrl()
        {
            return _integrationUrl;
        }

        public bool IsInvalidSslCertificateAccepted()
        {
            return true;
        }

    }
}
