﻿namespace iDrake_Sample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCarregarCaixaMensagens = new System.Windows.Forms.Button();
            this.gridMensagens = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.txtChavePrivada = new System.Windows.Forms.TextBox();
            this.btnInformarLeitura = new System.Windows.Forms.Button();
            this.btnConfirmarPedido = new System.Windows.Forms.Button();
            this.btnRecusarPedido = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCancelarComTaxa = new System.Windows.Forms.Button();
            this.btnCancelarSemCustos = new System.Windows.Forms.Button();
            this.btnCancelarComReembolso = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridMensagens)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCarregarCaixaMensagens
            // 
            this.btnCarregarCaixaMensagens.Location = new System.Drawing.Point(17, 36);
            this.btnCarregarCaixaMensagens.Name = "btnCarregarCaixaMensagens";
            this.btnCarregarCaixaMensagens.Size = new System.Drawing.Size(274, 38);
            this.btnCarregarCaixaMensagens.TabIndex = 1;
            this.btnCarregarCaixaMensagens.Text = "Carregar caixa de mensagens";
            this.btnCarregarCaixaMensagens.UseVisualStyleBackColor = true;
            this.btnCarregarCaixaMensagens.Click += new System.EventHandler(this.btnCarregarCaixaMensagens_Click);
            // 
            // gridMensagens
            // 
            this.gridMensagens.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMensagens.Location = new System.Drawing.Point(13, 25);
            this.gridMensagens.MultiSelect = false;
            this.gridMensagens.Name = "gridMensagens";
            this.gridMensagens.ReadOnly = true;
            this.gridMensagens.RowTemplate.Height = 28;
            this.gridMensagens.Size = new System.Drawing.Size(792, 325);
            this.gridMensagens.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Código";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(229, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Chave Privada";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(71, 42);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(69, 26);
            this.txtCodigo.TabIndex = 5;
            // 
            // txtChavePrivada
            // 
            this.txtChavePrivada.Location = new System.Drawing.Point(345, 39);
            this.txtChavePrivada.Name = "txtChavePrivada";
            this.txtChavePrivada.Size = new System.Drawing.Size(786, 26);
            this.txtChavePrivada.TabIndex = 6;
            // 
            // btnInformarLeitura
            // 
            this.btnInformarLeitura.Location = new System.Drawing.Point(17, 82);
            this.btnInformarLeitura.Name = "btnInformarLeitura";
            this.btnInformarLeitura.Size = new System.Drawing.Size(274, 38);
            this.btnInformarLeitura.TabIndex = 7;
            this.btnInformarLeitura.Text = "Informar leitura";
            this.btnInformarLeitura.UseVisualStyleBackColor = true;
            this.btnInformarLeitura.Click += new System.EventHandler(this.btnInformarLeitura_Click);
            // 
            // btnConfirmarPedido
            // 
            this.btnConfirmarPedido.Location = new System.Drawing.Point(17, 126);
            this.btnConfirmarPedido.Name = "btnConfirmarPedido";
            this.btnConfirmarPedido.Size = new System.Drawing.Size(274, 40);
            this.btnConfirmarPedido.TabIndex = 8;
            this.btnConfirmarPedido.Text = "Confirmar pedido";
            this.btnConfirmarPedido.UseVisualStyleBackColor = true;
            this.btnConfirmarPedido.Click += new System.EventHandler(this.btnConfirmarPedido_Click);
            // 
            // btnRecusarPedido
            // 
            this.btnRecusarPedido.Location = new System.Drawing.Point(17, 172);
            this.btnRecusarPedido.Name = "btnRecusarPedido";
            this.btnRecusarPedido.Size = new System.Drawing.Size(274, 40);
            this.btnRecusarPedido.TabIndex = 9;
            this.btnRecusarPedido.Text = "Recusar pedido";
            this.btnRecusarPedido.UseVisualStyleBackColor = true;
            this.btnRecusarPedido.Click += new System.EventHandler(this.btnRecusarPedido_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCancelarComTaxa);
            this.groupBox1.Controls.Add(this.btnCancelarSemCustos);
            this.groupBox1.Controls.Add(this.btnCancelarComReembolso);
            this.groupBox1.Controls.Add(this.btnInformarLeitura);
            this.groupBox1.Controls.Add(this.btnRecusarPedido);
            this.groupBox1.Controls.Add(this.btnConfirmarPedido);
            this.groupBox1.Controls.Add(this.btnCarregarCaixaMensagens);
            this.groupBox1.Location = new System.Drawing.Point(873, 136);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 363);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Operações";
            // 
            // btnCancelarComTaxa
            // 
            this.btnCancelarComTaxa.Location = new System.Drawing.Point(17, 310);
            this.btnCancelarComTaxa.Name = "btnCancelarComTaxa";
            this.btnCancelarComTaxa.Size = new System.Drawing.Size(274, 40);
            this.btnCancelarComTaxa.TabIndex = 14;
            this.btnCancelarComTaxa.Text = "Cancelar com taxa";
            this.btnCancelarComTaxa.UseVisualStyleBackColor = true;
            this.btnCancelarComTaxa.Click += new System.EventHandler(this.btnCancelarComTaxa_Click);
            // 
            // btnCancelarSemCustos
            // 
            this.btnCancelarSemCustos.Location = new System.Drawing.Point(17, 264);
            this.btnCancelarSemCustos.Name = "btnCancelarSemCustos";
            this.btnCancelarSemCustos.Size = new System.Drawing.Size(274, 40);
            this.btnCancelarSemCustos.TabIndex = 13;
            this.btnCancelarSemCustos.Text = "Cancelar sem custos";
            this.btnCancelarSemCustos.UseVisualStyleBackColor = true;
            this.btnCancelarSemCustos.Click += new System.EventHandler(this.btnCancelarSemCustos_Click);
            // 
            // btnCancelarComReembolso
            // 
            this.btnCancelarComReembolso.Location = new System.Drawing.Point(17, 218);
            this.btnCancelarComReembolso.Name = "btnCancelarComReembolso";
            this.btnCancelarComReembolso.Size = new System.Drawing.Size(274, 40);
            this.btnCancelarComReembolso.TabIndex = 10;
            this.btnCancelarComReembolso.Text = "Cancelar com reembolso";
            this.btnCancelarComReembolso.UseVisualStyleBackColor = true;
            this.btnCancelarComReembolso.Click += new System.EventHandler(this.btnCancelarComReembolso_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtCodigo);
            this.groupBox2.Controls.Add(this.txtChavePrivada);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(28, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1150, 100);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados da conta";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.gridMensagens);
            this.groupBox3.Location = new System.Drawing.Point(28, 136);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(827, 363);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Caixa de mensagens";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 516);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Sample do Fornecedor";
            ((System.ComponentModel.ISupportInitialize)(this.gridMensagens)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCarregarCaixaMensagens;
        private System.Windows.Forms.DataGridView gridMensagens;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.TextBox txtChavePrivada;
        private System.Windows.Forms.Button btnInformarLeitura;
        private System.Windows.Forms.Button btnConfirmarPedido;
        private System.Windows.Forms.Button btnRecusarPedido;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnCancelarComTaxa;
        private System.Windows.Forms.Button btnCancelarSemCustos;
        private System.Windows.Forms.Button btnCancelarComReembolso;
    }
}

