﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iDrake.Client;
using iDrake.Client.Config;
using iDrake.Client.Utils;
using Newtonsoft.Json.Serialization;

namespace iDrake_Sample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private IStrategyConfiguration GetConfig()
        {
            return new ConfigStrategy(int.Parse(txtCodigo.Text), txtChavePrivada.Text);
        }
        
        private void btnCarregarCaixaMensagens_Click(object sender, EventArgs e)
        {
            // O código abaixo obtem todas as mensagens presentes na caixa de mensagens do fornecedor.
            var operacao = new ReadOperation();
            var mensagens = operacao.ReadMessages(GetConfig());

            gridMensagens.Columns.Clear();

            gridMensagens.Columns.Add("Id", "Código da Mensagem");
            gridMensagens.Columns.Add("DrakeID", "Drake ID");
            gridMensagens.Columns.Add("SenderId", "ID do Cliente");
            gridMensagens.Columns.Add("Type", "Tipo");
            gridMensagens.Columns.Add("Content", "Conteúdo");

            foreach (var mensagem in mensagens)
            {
                var content = JsonHelper.JsonSerializer(mensagem, true, new DefaultContractResolver());

                object[] linha = 
                {
                    mensagem.Id.ToString(),
                    mensagem.DrakeId.ToString(),
                    mensagem.SenderId.ToString(),
                    mensagem.Type.ToString(),
                    content
                };

                gridMensagens.Rows.Add(linha);
            }          
        }

        private void btnInformarLeitura_Click(object sender, EventArgs e)
        {
            // O código abaixo confirma a leitura das mensagens selecionadas no GRID.
            var mensagensLidas = new List<long>();
            
            for (var i = 0; i < gridMensagens.SelectedRows.Count; i++)
            {
                var mensagemId = gridMensagens.SelectedRows[i].Cells[0].Value.ToString();

                mensagensLidas.Add(int.Parse(mensagemId));
            }

            var operacao = new ReadOperation();
            operacao.ConfirmReadedMessages(GetConfig(), mensagensLidas);
        }

        private void btnConfirmarPedido_Click(object sender, EventArgs e)
        {
            // O código abaixo confirma a leitura das mensagens selecionadas no GRID.
            var clienteId = int.Parse(gridMensagens.SelectedRows[0].Cells[2].Value.ToString());
            var drakeId = long.Parse(gridMensagens.SelectedRows[0].Cells[1].Value.ToString());

            var operacao = new ProviderOperation();

            operacao.ConfirmService(new ConfirmServiceMessage
            {
                RecipientId = clienteId,
                DrakeId = drakeId,
                Acknowledgment = false,
                Commission = 10,
                Value = 100,
                Fees = 15,
                ExternalId = "XPTO", // Código de controle do fornecedor
                Details = "Detalhes...",
                ReservationNumber = "RN-1234",
                SenderUserName = "Usuário do fornecedor",
                Provider = new Provider {Id = "G1", Name = "GOL LINHAS AÉREAS"},
                Origin = new Address
                {
                    Name = "Aeroporto SDU",
                    //...
                },
                Destination = new Address
                {
                    Name = "Aeroporto VIX",
                    //...
                },
                Start = new DateTime(2015, 1, 1, 10, 20, 00),
                End = new DateTime(2015, 1, 1, 11, 10, 00)
            });

            operacao.Execute(GetConfig());
        }

        private void btnRecusarPedido_Click(object sender, EventArgs e)
        {
            var clienteId = int.Parse(gridMensagens.SelectedRows[0].Cells[2].Value.ToString());
            var drakeId = long.Parse(gridMensagens.SelectedRows[0].Cells[1].Value.ToString());

            var operacao = new ProviderOperation();

            operacao.RefuseRequest(new RefuseRequestMessage
            {
                RecipientId = clienteId,
                DrakeId = drakeId,
                Details = "Não temos mais vagas no voo."
            });

            operacao.Execute(GetConfig());
        }

        private void btnCancelarComReembolso_Click(object sender, EventArgs e)
        {
            var clienteId = int.Parse(gridMensagens.SelectedRows[0].Cells[2].Value.ToString());
            var drakeId = long.Parse(gridMensagens.SelectedRows[0].Cells[1].Value.ToString());

            var operacao = new ProviderOperation();

            operacao.CancelConfirmationWithRefund(new CancelationConfirmationWithRefundMessage
            {
                RecipientId = clienteId,
                DrakeId = drakeId,
                RefundValue = 80
            });

            operacao.Execute(GetConfig());
        }

        private void btnCancelarSemCustos_Click(object sender, EventArgs e)
        {
            var clienteId = int.Parse(gridMensagens.SelectedRows[0].Cells[2].Value.ToString());
            var drakeId = long.Parse(gridMensagens.SelectedRows[0].Cells[1].Value.ToString());

            var operacao = new ProviderOperation();

            operacao.CancelConfirmation(new CancelationConfirmationMessage
            {
                RecipientId = clienteId,
                DrakeId = drakeId
            });

            operacao.Execute(GetConfig());
        }

        private void btnCancelarComTaxa_Click(object sender, EventArgs e)
        {
            var clienteId = int.Parse(gridMensagens.SelectedRows[0].Cells[2].Value.ToString());
            var drakeId = long.Parse(gridMensagens.SelectedRows[0].Cells[1].Value.ToString());

            var operacao = new ProviderOperation();

            operacao.CancelConfirmationWithFee(new CancelationConfirmationWithFeeMessage
            {
                RecipientId = clienteId,
                DrakeId = drakeId,
                FeeValue = 20
            });

            operacao.Execute(GetConfig());
        }
    }
}
