﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace iDrake.Client.Test.UnitTest.Steps
{

    [Binding]
    public class BaseSteps
    {

        private const string CurrentExceptionKey = "CURRENT_EXCEPTION";
        private const string CurrentResultKey = "CURRENT_RESULT";

        public void AddCurrent(object current)
        {
            ScenarioContext.Current.Add(current.GetType().FullName, current);
        }

        public T GetCurrent<T>()
        {
            if (ScenarioContext.Current.ContainsKey(typeof (T).FullName) == false)
            {
                throw new ArgumentException("Não existe objeto no contexto do tipo '"+ typeof(T).FullName+"'.");
            }

            return ScenarioContext.Current.Get<T>();
        }

        public void AddCurrentException(Exception e)
        {
            ScenarioContext.Current.Add(CurrentExceptionKey, e);
        }

        public Exception GetCurrentException()
        {
            if (ScenarioContext.Current.ContainsKey(CurrentExceptionKey) == false)
            {
                return null;
            }

            return ScenarioContext.Current.Get<Exception>(CurrentExceptionKey);
        }

        public void AddCurrentResult(object currentResult)
        {
            ScenarioContext.Current.Add(CurrentResultKey, currentResult);
        }
        
        public T GetCurrentResult<T>()
        {
            if (ScenarioContext.Current.ContainsKey(CurrentResultKey) == false)
            {
                return default(T);
            }

            return ScenarioContext.Current.Get<T>(CurrentResultKey);
        }

        #region Então

        [Then(@"devo receber o erro '(.*)'")]
        public void EntaoDevoReceberOErro(string erro)
        {
            GetCurrentException().Should().NotBeNull("Era esperado um erro.");
            GetCurrentException().Message.Should().Be(erro, "Mensagem de erro é diferente do que era esperada.");
        }

        [Then(@"não devo receber erro")]
        public void EntaoNaoDevoReceberErro()
        {
            GetCurrentException().Should().BeNull("Não Era esperado um erro.");
        }

        [Then(@"devo receber os seguintes erros")]
        public void EntaoDevoReceberOsSeguintesErros(Table erros)
        {
            var errosOcorridos = GetCurrentResult<List<string>>();

            errosOcorridos.Should().NotBeNull("Erros ocorridos não deveria ser nulo.");

            foreach (var erroEsperadoLinha in erros.Rows)
            {
                var erroEsperado = erroEsperadoLinha["ERRO"];

                errosOcorridos.Should().Contain(erroEsperado, "Deveria ter retornado o erro esperado '{0}'.", erroEsperado);
            }

            errosOcorridos.Count.Should().Be(erros.RowCount, "Quantidade de erros ocorridos é diferente da quantidade de erros gerados.");
        }

        #endregion

    }

}
