﻿using System;
using TechTalk.SpecFlow;

namespace iDrake.Client.Test.UnitTest.Steps
{

    [Binding]
    [Scope(Feature = "Validação de mensagens")]
    public class ValidacaoDeMensagensSteps : BaseSteps
    {

        #region Dado

        [Given(@"que tenho uma mensagem de confirmação de serviço com endereço de origem nulo")]
        public void DadoQueTenhoUmaMensagemDeConfirmacaoDeServicoComEnderecoDeOrigemNulo()
        {
            var mensagem = new ConfirmServiceMessage
            {
                Origin = null,
                Destination = new Address()
            };

            AddCurrent(mensagem);
        }
        
        [Given(@"que tenho uma mensagem de confirmação de serviço com endereço de destino nulo")]
        public void DadoQueTenhoUmaMensagemDeConfirmacaoDeServicoComEnderecoDeDestinoNulo()
        {
            var mensagem = new ConfirmServiceMessage
            {
                Origin = new Address(),
                Destination = null
            };

            AddCurrent(mensagem);
        }
        
        [Given(@"que tenho uma mensagem de confirmação de serviço com endereço de origem e destino")]
        public void DadoQueTenhoUmaMensagemDeConfirmacaoDeServicoComEnderecoDeOrigemEDestino()
        {
            var mensagem = new ConfirmServiceMessage
            {
                Origin = new Address(),
                Destination = new Address()
            };

            AddCurrent(mensagem);
        }

        #endregion

        #region Quando

        [When(@"tento validar a mensagem")]
        public void QuandoTentoValidarAMensagem()
        {
            try
            {
                GetCurrent<ConfirmServiceMessage>().Validate();
            }
            catch (Exception e)
            {
                AddCurrentException(e);
            }
        }

        #endregion

    }

}
