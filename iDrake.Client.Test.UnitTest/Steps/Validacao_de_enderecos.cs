﻿using iDrake.Client.Internal;
using TechTalk.SpecFlow;

namespace iDrake.Client.Test.UnitTest.Steps
{

    [Binding]
    [Scope(Feature = "Validação de endereços")]
    public class ValidacaoDeEnderecosSteps : BaseSteps
    {

        #region Dado

        [Given(@"que tenho um endereço")]
        public void DadoQueTenhoUmEndereco()
        {
            var endereco = new Address();
            AddCurrent(endereco);
        }
        
        [Given(@"que a rua tem mais de (.*) caracteres")]
        public void DadoQueARuaTemMaisDeCaracteres(int tamanho)
        {
            GetCurrent<Address>().Street = TestUtil.GetStringGreaterThan(tamanho);
        }
        
        [Given(@"que o número tem mais de (.*) caracteres")]
        public void DadoQueONumeroTemMaisDeCaracteres(int tamanho)
        {
            GetCurrent<Address>().Number = TestUtil.GetStringGreaterThan(tamanho);
        }
        
        [Given(@"que o complemento tem mais de (.*) caracteres")]
        public void DadoQueOComplementoTemMaisDeCaracteres(int tamanho)
        {
            GetCurrent<Address>().Complement = TestUtil.GetStringGreaterThan(tamanho);
        }

        [Given(@"que o bairro tem mais de (.*) caracteres")]
        public void DadoQueOBairroTemMaisDeCaracteres(int tamanho)
        {
            GetCurrent<Address>().District = TestUtil.GetStringGreaterThan(tamanho);
        }

        [Given(@"que a cidade tem mais de (.*) caracteres")]
        public void DadoQueACidadeTemMaisDeCaracteres(int tamanho)
        {
            GetCurrent<Address>().City = TestUtil.GetStringGreaterThan(tamanho);
        }

        [Given(@"que o estado tem mais de (.*) caracteres")]
        public void DadoQueOEstadoTemMaisDeCaracteres(int tamanho)
        {
            GetCurrent<Address>().State = TestUtil.GetStringGreaterThan(tamanho);
        }

        [Given(@"que o país tem mais de (.*) caracteres")]
        public void DadoQueOPaisTemMaisDeCaracteres(int tamanho)
        {
            GetCurrent<Address>().Country = TestUtil.GetStringGreaterThan(tamanho);
        }

        [Given(@"que o CEP tem mais de (.*) caracteres")]
        public void DadoQueOcepTemMaisDeCaracteres(int tamanho)
        {
            GetCurrent<Address>().Zip = TestUtil.GetStringGreaterThan(tamanho);
        }

        [Given(@"que a IATA tem mais de (.*) caracteres")]
        public void DadoQueAiataTemMaisDeCaracteres(int tamanho)
        {
            GetCurrent<Address>().IATA = TestUtil.GetStringGreaterThan(tamanho);
        }

        #endregion 

        #region Quando

        [When(@"tento validar o endereço")]
        public void QuandoTentoValidarOEndereco()
        {
            var listaErros = Validator.Validate(GetCurrent<Address>());
            AddCurrentResult(listaErros);
        }

        #endregion

    }

}
