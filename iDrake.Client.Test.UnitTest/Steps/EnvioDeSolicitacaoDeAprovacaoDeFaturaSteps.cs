﻿using System.Linq;
using AutoMapper;
using FluentAssertions;
using iDrake.Client.Internal;
using iDrake.Client.Stubs;
using TechTalk.SpecFlow;

namespace iDrake.Client.Test.UnitTest.Steps
{

    [Binding]
    public class EnvioDeSolicitacaoDeAprovacaoDeFaturaSteps : BaseSteps
    {

        #region Dado

        [Given(@"que o AutoMapper está configurado")]
        public void DadoQueOAutoMapperEstaConfigurado()
        {
            AutoMapperConfiguration.Configure();
        }

        [Given(@"que tenho uma mensagem de solicitação de aprovação de fatura")]
        public void DadoQueTenhoUmaMensagemDeSolicitacaoDeAprovacaoDeFatura()
        {
            var mensagem = new InvoiceApprovalRequestMessage
            {
                ExternalId = "FAT-1000"
            };

            AddCurrent(mensagem);
        }

        [Given(@"que a mensagem de solicitação de aprovação de fatura tenha um centro de custo definido")]
        public void DadoQueAMensagemDeSolicitacaoDeAprovacaoDeFaturaTenhaUmCentroDeCustoDefinido()
        {
           GetCurrent<InvoiceApprovalRequestMessage>().SetCostCenter("BR", "BRAZIL");
        }

        #endregion

        #region Quando

        [When(@"solicito para gerar a mensagem equivalente no serviço")]
        public void QuandoSolicitoParaGerarAMensagemEquivalenteNoServico()
        {
            var mensagemCliente = GetCurrent<InvoiceApprovalRequestMessage>();

            AddCurrent(mensagemCliente.GetEquivalentServiceMessage());
        }

        #endregion

        #region Então

        [Then(@"a mensagem equivalente no serviço deverá possuir as seguintes características")]
        public void EntaoAMensagemEquivalenteNoServicoDeveraPossuirAsSeguintesCaracteristicas(Table table)
        {
            var mensagemServidor = GetCurrent<ServiceMessage>();

            var row = table.Rows[0];

            var externalIdEsperado = row["ExternalId"];
            var costCenterCodeEsperado = row["Invoice.Details.COST_CENTER_CODE"];
            var costCenterNameEsperado = row["Invoice.Details.COST_CENTER_NAME"];

            mensagemServidor.ExternalId.Should().Be(externalIdEsperado);
            
            mensagemServidor.Invoice.Details.ToList()
                .First(x => x.Key == "COST_CENTER_CODE").Value
                .Should()
                .Be(costCenterCodeEsperado);

            mensagemServidor.Invoice.Details.ToList()
                .First(x => x.Key == "COST_CENTER_NAME").Value
                .Should()
                .Be(costCenterNameEsperado);
        }

        #endregion

    }

}