﻿using System;
using FluentAssertions;
using iDrake.Client.Internal;
using NUnit.Framework;

namespace iDrake.Client.Test.UnitTest
{
    public class ConvertMessagesTest 
    {
        [SetUp]
        public void ConfirmCancelationWithRefundTest_SetUp()
        {
            AutoMapperConfiguration.Configure();
        }
        
        [Test]
        public void conversao_CancelationConfirmationWithRefundMessage()
        {
            // Given
            var message = new CancelationConfirmationWithRefundMessage();
            message.RefundValue = 123;
            
            // When
            var clientMessage = (CancelationConfirmationWithRefundMessage)message.GetEquivalentServiceMessage().GetEquivalentClientMessage();
            
            // Then
            clientMessage.RefundValue.Should().Be(123);
        }
        
        [Test]
        public void conversao_CancelationConfirmationWithFeeMessage()
        {
            // Given
            var message = new CancelationConfirmationWithFeeMessage();
            message.FeeValue = 123;
            
            // When
            var clientMessage = (CancelationConfirmationWithFeeMessage)message.GetEquivalentServiceMessage().GetEquivalentClientMessage();
            
            // Then
            clientMessage.FeeValue.Should().Be(123);
        }

        [Test]
        public void conversao_CancelServiceMessage()
        {
            // Given
            var message = new CancelServiceMessage();
            message.Details = "Teste";
            
            // When
            var clientMessage = (CancelServiceMessage)message.GetEquivalentServiceMessage().GetEquivalentClientMessage();
            
            // Then
            clientMessage.Details.Should().Be("Teste");
        }

        [Test]
        public void conversao_ConfirmProcessingMessage()
        {
            // Given
            var message = new ConfirmProcessingMessage();
            message.ReceivedMessageId = 100;
            message.Sent = DateTime.Today;
            
            // When
            var clientMessage = (ConfirmProcessingMessage)message.GetEquivalentServiceMessage().GetEquivalentClientMessage();
            
            // Then
            clientMessage.ReceivedMessageId.Should().Be(100);
            clientMessage.Sent.Should().Be( DateTime.Today);
        }
    }
}