﻿Funcionalidade: Validação de endereços

Cenário: Endereço com rua com mais de 255 caracteres
	Dado que tenho um endereço
	E que a rua tem mais de 255 caracteres
	Quando tento validar o endereço
	Então devo receber os seguintes erros
	| ERRO                                   |
	| Rua deve ter no máximo 255 caracteres. |

Cenário: Endereço com número com mais de 25 caracteres
	Dado que tenho um endereço
	E que o número tem mais de 25 caracteres
	Quando tento validar o endereço
	Então devo receber os seguintes erros
	| ERRO                                     |
	| Número deve ter no máximo 25 caracteres. |

Cenário: Endereço com complemento com mais de 50 caracteres
	Dado que tenho um endereço
	E que o complemento tem mais de 50 caracteres
	Quando tento validar o endereço
	Então devo receber os seguintes erros
	| ERRO                                          |
	| Complemento deve ter no máximo 50 caracteres. |

Cenário: Endereço com bairro com mais de 100 caracteres
	Dado que tenho um endereço
	E que o bairro tem mais de 100 caracteres
	Quando tento validar o endereço
	Então devo receber os seguintes erros
	| ERRO                                      |
	| Bairro deve ter no máximo 100 caracteres. |

Cenário: Endereço com cidade com mais de 100 caracteres
	Dado que tenho um endereço
	E que a cidade tem mais de 100 caracteres
	Quando tento validar o endereço
	Então devo receber os seguintes erros
	| ERRO                                      |
	| Cidade deve ter no máximo 100 caracteres. |

Cenário: Endereço com estado com mais de 2 caracteres
	Dado que tenho um endereço
	E que o estado tem mais de 2 caracteres
	Quando tento validar o endereço
	Então devo receber os seguintes erros
	| ERRO                                    |
	| Estado deve ter no máximo 2 caracteres. |
	
Cenário: Endereço com país com mais de 40 caracteres
	Dado que tenho um endereço
	E que o país tem mais de 40 caracteres
	Quando tento validar o endereço
	Então devo receber os seguintes erros
	| ERRO                                   |
	| País deve ter no máximo 40 caracteres. |

Cenário: Endereço com CEP com mais de 8 caracteres
	Dado que tenho um endereço
	E que o CEP tem mais de 8 caracteres
	Quando tento validar o endereço
	Então devo receber os seguintes erros
	| ERRO                                 |
	| CEP deve ter no máximo 8 caracteres. |

Cenário: Endereço com IATA com mais de 4 caracteres
	Dado que tenho um endereço
	E que a IATA tem mais de 4 caracteres
	Quando tento validar o endereço
	Então devo receber os seguintes erros
	| ERRO                                         |
	| Código IATA deve ter no máximo 4 caracteres. |