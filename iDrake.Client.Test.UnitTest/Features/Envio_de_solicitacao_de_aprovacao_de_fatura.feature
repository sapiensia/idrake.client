﻿Funcionalidade: Envio de solicitação de aprovação de fatura

Contexto: 
Dado que o AutoMapper está configurado

Cenário: Envio de solicitação de aprovação de fatura com centro de custo
Dado que tenho uma mensagem de solicitação de aprovação de fatura
E que a mensagem de solicitação de aprovação de fatura tenha um centro de custo definido
Quando solicito para gerar a mensagem equivalente no serviço
Então a mensagem equivalente no serviço deverá possuir as seguintes características
| ExternalId | Invoice.Details.COST_CENTER_CODE | Invoice.Details.COST_CENTER_NAME |
| FAT-1000   | BR                               | BRAZIL                           |