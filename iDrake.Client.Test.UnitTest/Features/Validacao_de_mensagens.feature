﻿Funcionalidade: Validação de mensagens

Cenário: Validar mensagem de confirmação de serviço com endereço de origem nulo
	Dado que tenho uma mensagem de confirmação de serviço com endereço de origem nulo
	Quando tento validar a mensagem
	Então não devo receber erro

Cenário: Validar mensagem de confirmação de serviço com endereço de destino nulo
	Dado que tenho uma mensagem de confirmação de serviço com endereço de destino nulo
	Quando tento validar a mensagem
	Então não devo receber erro

Cenário: Validar mensagem de confirmação de serviço com endereço de origem e destino
	Dado que tenho uma mensagem de confirmação de serviço com endereço de origem e destino
	Quando tento validar a mensagem
	Então não devo receber erro
